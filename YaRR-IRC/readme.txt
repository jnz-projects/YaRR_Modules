*****************************************************************
* Custom	Scripts.dll For C&C Renegade			*
* Author:	Jonathan Wilson					*
* Website:	http://www.sourceforge.net/projects/rentools	*
* Email:	jfwfreo@tpgi.com.au				*
* ICQ:		77572965					*
* AIM:		jonwil2002					*
* Yahoo:	jonwil2002					*
* MSN:		JonathanWilson623@hotmail.com			*
*****************************************************************

The Renegade Scripts.dll is Copyright 2002-2006
Jonathan Wilson.
Daniel King
Datalore
Vloktboky
WhiteDragon(MDB)
Joshua "NeoSaber" Kraft
Olaf Van Der Spek
E!
TheKGBspy
Tom "Dante" Anderson
NameHunter
Spirit
Mark "Shawk" Sararu
TimeFX

Apologies if I missed anyone out or if I got anyones copyright wrong.
If you think that your name should be on a source file that it is not currently on (or not on a file that it is currently on), please let me know and
I will fix it.

The Renegade scripts.dll is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2, or (at your option) any later version. See the file COPYING for more details.
In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code
covered by this licence. Only the source code to the module(s) containing the licenced code has to be released.

Basicly, if you are distributing the scripts.dll without changes, all you have to do is link back to http://www.sourceforge.net/projects/rentools/
If you are distributing a modified version of the scripts.dll code, you have to include the source code to all files contained in the same .dll or .so
as any files containg scripts.dll code.
Any new files you wish to add should contain an appropriate GPL header (similar to what is in the existing files)

Also, a note to me that you have made new scripts, while not essential, would be nice :)

This DLL has been tested with the FDS and works so far, if there's any issues let me know. As of 1.5, it should work with both the RH7 and RH8 versions of the Linux FDS (thanks datalore).
It has been tested with Load + Save and works just fine.
I really need to get as many people as possible testing this dll, using it in maps and so on.
If you have any ideas for new scripts please contact me, Jonathan Wilson.
Also contact me if you have any questions, want to report bugs and so on.
Should you experience a crash in renegade with my dll installed, look for a file called critical.txt
Should critical.txt exist (it will say "Critical Scripts.dll error" in it) let me know (I need to know what the parametereter specified in the critical.txt line is)
Also, when reporting crashes, crashdump.txt (new in scripts.dll/bhs.dll 2.2) contains important details of the crash so hang on to it.
As of 3.2, crashdumps are now numbered like screenshots.
If you get the "bhs.dll not found" or "Scripts2.dll not found" message, look for dllload.txt, that will contain error info I can use to diagnose why bhs.dll/scripts2.dll isn't loading.
I do not provide support for any of the Black-Cell mods (Dragonade, SSAOW, SSCTF etc) so do not ask me about them (e.g. "how did Black-Cell do xxx" or "how can I get xxx Black-Cell mod to work").
If you want support for Black-Cell mods, talk to Black-Cell.
The official scripts.dll/bhs.dll shouldn't be used with anything created by Silent_Kane.
Also, you should not mix and match bhs.dll and scripts.dll versions.
Using a bhs.dll version with any scripts.dll other than one based off the matching version is not supported and can lead to nasty side-effects.
In particular, taking old scripts.dll code (e.g. with custom mods) and adding the new bhs.dll to it to get the new effects won't work.

*****************************************************************

Thanks to all the people who have coded scripts in the custom scripts.dll and helped with the C++ stuff.
Thanks to Datalore for his work on the LFDS stuff (and to Mac for testing it and stuff)
Thanks to Mac for getting me Linux FDS boxes to test the scripts.dll on.
Thanks to TheKGBSpy, NeoSaber, vloktboky03, v00d00 and others (who I forget) who have made discoveries about the engine and the scripts.dll.
Thanks to the members of the reborn team (including TheKGBSpy) for making me a part of the team and for giving me the ideas and support needed to get all the Reborn scripts done.
Thanks to the members of the APB team (including NeoSaber and Chronojam) for making me a part of the team and for giving me the ideas and support needed to get all the APB scripts done.
And thanks to all the people who have tested, provided ideas and so on (far too many to list).
Also, a big thanks to Greg Hjelstrom & all the people at Westwood Studios for being so helpful in answering all those questions about the game engine over on the message boards and also for writing such a great game in the first place.

No thanks to the PHB managers at EA forcing me to do all the 
reverse engineering on this myself instead of being able to use the real scripts.dll from westwood.

*****************************************************************

Below is a quick guide to using the custom Scripts.dll from Jonathan Wilson.

Installation:
Go to your *\Westwood\Renegade\ Directory, and rename the scripts.dll to scripts2.dll
Unzip Scripts.dll, bhs.dll, d3d8.dll and shaders.dll into your *\Westwood\Renegade\ Directory.

Now the scripts will work normally in game, these are just EXPANDED scripts.
Use:
Startup Commando Level Editor (Level Edit), Create a new Mod.
After the folders have been created (Level Edit loads up), go ahead and close it.
Now, create a new directory inside your new mod folder called "Scripts"
Next, copy Scripts.dll AND Scripts2.dll into this new directory.
Your new scripts will now be available in game.

New Scripts:
You will add these new scripts exactly like you did before.
You may want to bookmark this site (http://www.celticrover.com/tig/hex.asp) for the hex to decimal conversions
If you need to know the string IDs, download strdump from the same place you got this file and run it on your strings.tdb, it's a console application and will print details of the strings to the console screen.

*****************************************************************

JFW_Spawn_Object_Death (This script will create an object upon death of a unit, does not work on player spawners)
	Drop_Object (Preset STRING Name) example POW_Flamethrower_Player
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died.

JFW_Poke_Send_Custom (This script will send a message to an object when it's poked. It also uses script commands to make the little green arrows appear when its at state 0 or 1.)
	The script triggers on startup, sent a message and ActionKey (aka poke). On startup, it sets the state to 0, unless the Activate_Message is 0 then it sets it to 1.
	When it's sent the message that matches the Activate_Message parameter, it sets the state to the message parameter.
	When the action key is pressed, if the state is 0, it will play a sound (something like "access denied" or something).
	If the state is 1, it will send the message and parameter specified to the object specified. If the state is 2, it will do nothing.
	Sound_Name (the sound to play when it triggers if the state is 0, it will actually play now since I figured it out, must be 3D)
	Activate_Message (The number of a message that, when sent to this object, changes the state)
	Object_Id (The object ID to send the message to when it's poked)
	Message (The message to send when it's poked)
	Param (The parameter to pass when it's poked)
	Delay (how long to wait before sending the custom)

JFW_Custom_Destroy_Object (This script will destroy an object when a message is sent to it)
	Message (The message to check for)
	ID (The ID of the object to destroy)

JFW_Custom_Toggle_Radar (This script will toggle the radar when it recieves the message specified)
	Message (The message to check for, send a parameter of 1 to disable radar and 0 to enable radar)

JFW_Attatch_Script (this script will attach another script to an object, depending on the parameters passed in)
	Trigger (what event to trigger the script on, 1 = zone entry, 2 = object death, 3 = custom, 4 = poke)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any, doesn't apply for trigger type of 2 or 3)
	Script_Name (what script to attach)
	Script_Params (parameters to pass to that script, it's basically a list like you see in leveledit next to the script name)
	Delim (since the string for Script_Params can't contain any commas, this field specifies what char is to be used instead, the script replaces it with a comma before attaching)
	Destroy (this specifies whether to destroy the object after the script is attached, applies for trigger type 1 and 4, 1 = destroy else it doesn't destroy)
	Custom (the message to test for when there is a trigger type of 3)
This script is depreciated, use JFW_Attach_Script_Custom instead.

JFW_Reflect_Custom (this script will send any message it recieves back to whatever object sent it)
	It takes no parameters

JFW_Add_Objective (this script adds an objective, make sure it's called before the other objective scripts)
	Objective_Num (This is an identifier for the objective, generally it would be a good idea to keep them in order,and start with 1000)
	Objective_Type (Type of objective, takes three different numbers; 1=Primary Objective, 2=Secondary, 3=Hidden)
	Title_ID (Title ID of the objective, this is a numeric value that links to a string in Stings.tdb, you can use 4607 for testing)
	Unknown (this is an unknown value, could be another string ID)
	Sound_Name (I think it's a sound, can't be sure. don't know if it's 2D or 3D or when it's played.)
	Description_ID (Another String For the Objective, but this is the one that is used to display the objective to the player, like Objective Title ID, you can test with 4607, it's kind of like a description of the objective)
	Trigger (see below for note about trigger and usage)

JFW_Remove_Objective (this script removes an objective)
	Objective_Num (the objective number to remove)

JFW_Set_Objective_Status (this script sets the status of an objective)
	Objective_Num (The number of the objective you would like to set the status of)
	Status (the new status,1=Success, 2=Failed)

JFW_Change_Objective_Type (this script changes the type of an objective)
	Objective_Num (The number of the objective you would like to change the type of)
	Objective_Type (Type of objective, takes three different numbers; 1=Primary Objective, 2=Secondary, 3=Hidden)

JFW_Set_Objective_Radar_Blip (this sets the "star" icon on the radar for the objective to a vector)
	Objective_Num (The number of the objective you would like to set the radar blip for)
	Position (This is an x,y,z indicating the location of the objective)

JFW_Set_Objective_Radar_Blip_Object (this sets the "star" icon on the radar for the objective to an object)
	Objective_Num (The number of the objective you would like to set the radar blip for)
	Object (The numeric ID of the object as seen in leveledit)

JFW_Set_Objective_HUD_Info (This shows the little "pog" picture for the objective)
	Objective_Num (The number of the objective you would like to show the pog for)
	Priority (the priority of the objective, not sure quite how this works)
	Pog_File_Name (This is the *.tga filename of your objective pogs, you can search Always.dat with XCC and enter in the search string *pog* to find some to use, you could use "POG_M05_1_01.tga" for test)
	Pog_Title_Id (This is the test that will show up on the POG in the game (i.e. ACCESS, DESTROY), you can find these in String.tdb, you can use 7372(which is ACCESS) for testing)

JFW_Set_Objective_HUD_Info_Position (This shows the little "pog" picture for the objective and enables the "range and direction finder" arrow)
	Objective_Num (The number of the objective you would like to show the pog for)
	Priority (the priority of the objective, not sure quite how this works)
	Pog_File_Name (This is the *.tga filename of your objective pogs, you can search Always.dat with XCC and enter in the search string *pog* to find some to use, you could use "POG_M05_1_01.tga" for test)
	Pog_Title_Id (This is the test that will show up on the POG in the game (i.e. ACCESS, DESTROY), you can find these in String.tdb, you can use 7372(which is ACCESS) for testing)
	Position (This is an x,y,z indicating the location of the objective)

JFW_Set_Num_Tertary_Objectives (This specifies the number of tertary aka hidden objectives, don't know why it's there, it just is)
	Count (count of tertary objectives)

JFW_Mission_Complete (This completes the mission)
	Won (0 = mission failed, 1 = mission suceeded)

Note about trigger parameter for JFW_Add_Objective. Because of how renegade works, you have to make sure that the Set_Objective_Radar_Blip and Set_Objective_HUD_Info scripts get executed after the Add_Objective script.
Unless you have an object that you want to attach both a HUD script and/or Radar script plus the Add_Objectve script to, you don't do anything special.
But, if you are attaching a HUD script or a Radar script to the same object as the Add_Objective script, you need to do special stuff.
What you do is you setup add_objective in the usual way. You set the "trigger" parameter to a message number.
Then, you add the HUD or Radar script, set the type to 3 and the typeval to the same thing as you set the "trigger" for the add_objective script
Then, you set the other parameters of the HUD or Radar script as usual.

All the objective scripts listed above (plus the mission complete) take, in addition to the parameters mentioned for the individual script, 2 parameters:
	Type (what event to trigger the script on, 0 = startup, 1 = zone entry, 2 = object death, 3 = custom, 4 = poke, 5 = timer expired)
	TypeVal (what the script does with this depends on the type)
	If the type is 0, 2, 4 then the TypeVal is ignored. 
	If it's 1, the TypeVal specifies what player type to activate on, 0 = Nod, 1 = GDI, 2 = any
	If it's 3, the TypeVal specifies what message to trigger on
	If it's 5, the TypeVal specifies what value of the "timer" parameter to trigger on.
	All the scripts also take a parameter called Timer_Custom. Because of the way timers work (they work on a script by script basis, they aren't global), you need to set this to the same as the value for JFW_Start_Timer in order to make timers work.
	If type is not 5, it doesn't matter what this is set to.

JFW_Start_Timer (this script starts a timer)
	Type (what event to trigger the script on, 0 = startup, 1 = zone entry, 2 = object death, 3 = custom, 4 = poke)
	TypeVal (what the script does with this depends on the type, it's the same as the objective scripts above)
	Timer_Object (what object to set the timer on, numeric object ID)
	Timer_Number (this is the number for the timer, it's what you use for the "typeval" later on)
	Timer_Time (how long to set the timer for)
	Timer_Repeat (set to 1 to make the timer repeat indefinatly, set to 0 to make it only execute once)
	Timer_Custom. (This one must match with the Timer_Custom value for one or more scripts attached to the Timer_Object)

JFW_Preset_Buy (this script is supposed to act as a "purchase point" for e.g. a helipad, it triggers on zone entry)
	Preset_Name (the name of the preset to create)
	Cost (the cost)
	Location (the location to spawn it at)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Note that if the player doesn't have enough cash, no purchase will take place

JFW_Death_Send_Custom (this script sends a message when whatever it's attached to dies, use with JFW_Custom_Destroy_Object to destroy 1 object when another object dies)
	ID (the ID of the object to send the message to)
	Message (the message to send)
	Param (the parameter to pass)

JFW_Pokeable_Item (This script will turn on the 3 green poke arrows for an object)

JFW_Death_Play_Cinematic (This script will fire off a cinematic when the object it's attached to dies)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Facing (what direction to fire it off in)

JFW_Zone_Play_Cinematic (This script will fire off a cinematic when the zone it's attached to is entered)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Facing (what direction to fire it off in)

JFW_Blow_Up_On_Enter (This script will create an explosion at the location of the object that entered the zone)
	Explosion (the explosion to create)

JFW_Apply_Damage_On_Enter (This script will apply damage to the object that entered the zone)
	Warhead (the warhead to use)
	Damage (how much damage to do)

JFW_Team_DM_Zone (This script is the zone script for the team deathmatch. Place enough of these zones such that every startup spawner is contained entirely within a zone)
	Player_Type (the player type to trigger on, 0 = Nod, 1 = GDI)
	Controller_ID (the ID of the team DM controller for the team)
	Deactivate_Message (the message that is used to make sure that a player can only have one copy of the attached script active at once)
	Death_Message (the message that is sent to the controller to record a death for that team)
	
JFW_Team_DM_Attatched (This script gets attached to the player object when it walks out of the zone after it spawns, don't use this script directly)
	Controller_ID (the ID of the team DM controller for the team)
	Deactivate_Message (the message that is used to make sure that a player can only have one copy of the attached script active at once)
	Death_Message (the message that is sent to the controller to record a death for that team)

JFW_Team_DM_Controller (This script should be attached to a DavesArrow or something, one for each team)
	Deaths_To_Loose (how many deaths must happen before the game is over)
	Death_Message (the message that is sent to the controller to record a death for that team)
	Building_To_Destroy1 (the first building to destroy when the game is over)
	Building_To_Destroy2 (the second building to destroy when the game is over)
	Building_To_Destroy3 (the third uilding to destroy when the game is over)
	Building_To_Destroy4 (the forth building to destroy when the game is over)
	Building_To_Destroy5 (the fifth building to destroy when the game is over)
	Note that PCTs and Team DM don't mix, don't use them. No vehicles either.

JFW_Timer_Destroy_Building (destroys a building when the timer expires)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Custom_Destroy_Building (destroys a building when it recieves a message)
	Message (the message to look for)
	ID (the ID of the object to destroy)

JFW_Zone_PCT (triggers a PCT on zone entry)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = trigger GDI terminal for GDI players, Nod terminal for Nod players)
	Thanks to bhs.dll, this script now works.

JFW_2D_Sound_Timer (plays a 2D sound when the timer expires)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (whether to repeat this or to only send it once, 1 = repeat, 0 = not repeat)
	Sound (the sound preset to use, must be 2D)

JFW_3D_Sound_Zone (plays a 3D sound on entry to the zone)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (the sound preset to use, must be 3D)

JFW_Timer_Play_Cinematic (plays a cinematic when a timer runs out)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (whether to repeat this or to only play it once, 1 = repeat, 0 = not repeat)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Facing (what direction to fire it off in)

JFW_User_Settable_Parameters (reads parameters for a script from a file, store them like you would see them in LevelEdit script list window)
	File_Name (what filename to read the parameters from, it will read from the path where game.exe is, if you put say data\params.txt it will read params.txt from renegade data folder)
	Script_Name (the script name to attach)

JFW_Nod_Turret (this script is a clone of M00_Nod_Turret)
	no parameters

JFW_Base_Defence (this script is a clone of M00_Base_Defence)
	MinAttackDistance (minumum attack distance)
	MaxAttackDistance (maximum attack distance)
	AttackTimer (time between attacks)
Note that this script has had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_User_Controllable_Base_Defence (this script is a clone of GTH_User_Controllable_Base_Defence)
	MinAttackDistance (minumum attack distance)
	MaxAttackDistance (maximum attack distance)
	AttackTimer (time between attacks)
Note that this script has had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Bounce_Zone_Entry (this script adds a fixed value to the Z position of anything that enters the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Amount (what to add to the z position each time)

JFW_Grant_Key_Zone_Entry (this grants a key on zone entry)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Key (what key number to grant)

JFW_Custom_Play_Cinematic (this script plays a cinematic when it recieves a custom)
	Message (what message will start the cinematic playing)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Facing (what direction to fire it off in)

JFW_Debug_Text_File (this script logs all events that happen to an object to a text file)
	Log_File (the filename to use, only one copy of the script can reference a given log file)
	Description (a description of the object to place in the logs)
	Note that because of how renegade works, it probobly won't log ActionComplete or TimerExpired events. Anything else should be logged though.

JFW_Timer_Custom (sends a custom when a timer expires)
	Time (the time to go for)
	TimerNum (the timer number to use)
	Repeat (whether to repeat this or to only send it once, 1 = repeat, 0 = not repeat)
	ID (what ID to send the message to)
	Message (what message to send)
	Param (what parameter to send)

JFW_Blow_Up_On_Death (this script creates an explosion when whatever it's attached to dies, usefull for e.g. a Demo Truck)
	Explosion (which explosion to create)
Thanks to bhs.dll, the explosion will now work even if the client doesn't have BHS.DLL installed
And no damage is done to buildings, use JFW_Death_Send_Custom (or some relation of it like JFW_Death_Send_Custom_Driver_ID in you want to have points be given for the driver of the vehicle) and JFW_Building_Damage or something for that bit.
And to make the driver die, use RA_DriverDeath

JFW_Zone_Send_Custom_Enable (sends a custom when a zone is entered or exited but only if it's enabled)
	ID (the id to send the messages to)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Enable_Message (what message will turn the zone on)
	Disable_Message (what message will turn the zone off)

JFW_Zone_Send_Custom_Preset (sends a custom when a zone is entered or exited but only if the object that entered it is a specific preset)
	ID (the id to send the messages to)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Preset_Name (what preset name to trigger on)

JFW_2D_Sound_Timer_Random (plays a sound at random intervals)
	Time_Min (minumum time for how long the timer should run for)
	Time_Max (maximum time for how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (whether to repeat this or to only send it once, 1 = repeat, 0 = not repeat)
	Sound (the sound preset to use, must be 2D)

JFW_3D_Sound_Timer_Random (plays a sound at random intervals)
	Time_Min (minumum time for how long the timer should run for)
	Time_Max (maximum time for how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (whether to repeat this or to only send it once, 1 = repeat, 0 = not repeat)
	Sound (the sound preset to use, must be 3D)
	Position (the position to play the sound at)

JFW_2D_Sound_Timer_Health (plays a 2D sound when the timer expires but only if the health of the object it's attached to is between certain values, great for "building is damaged" for example)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (whether to repeat this or to only send it once, 1 = repeat, 0 = not repeat)
	Sound (the sound preset to use, must be 2D)
	Min_Health (minumum health can be for the sound to play)
	Max_Health (maximum health can be for the sound to play)

JFW_Aircraft_Fuel (makes a vecicle only usable for a limited time, time starts when you enter the vecicle for the first time)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Explosion (what explosion to create when the time runs out)
	Refuel_Message (what message will reset the timer, perhans sent by a zone somewhere or something)

JFW_Power_Off (powers off a building or powers it on, only base defence structures actually stop working)
	Message_Off (what message will turn the building off)
	Message_On (what message will turn the building on)

JFW_Assault_Powerplant_Controller (script for assault map)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Power_Off (what message will turn the buildings off)
	Power_On (what message will turn the buildings on)
	Building1-Building15 (the buildings to turn off/on)
	On startup, this script sets an internal flag to false then sends Power_Off to all 15 building controllers. It also starts a timer.
	On timer expired, if the flag is false it will send Power_On to all 15 buildings.
	On zone entry, the flag is set to true.
	On zone exit, the flag is set to false.

JFW_Nod_Obelisk_CnC (this script is a clone of M00_Nod_Obelisk_CnC)
	Controller_ID (this is for if u have more than one obelisk)
This version will get used instead of the Westwood version. It contains a fix to prevent the "weapon" object from being damaged.
However, as I don't have a way for the weapon object to say "I have been killed", it won't prevent things if the weapon object is totally destroyed in one hit.

JFW_Obelisk_Weapon_CnC (this script is a clone of M00_Obelisk_Weapon_CnC)
	no parameters

JFW_Advanced_Guard_Tower (this script is a clone of M00_Advanced_Guard_Tower)
	no parameters
This version will get used instead of the Westwood version. It contains a fix to prevent the "weapon" object/guns from being damaged.
However, as I don't have a way for the weapon object to say "I have been killed", it won't prevent things if the weapon object/guns are totally destroyed in one hit.

JFW_Advanced_Guard_Tower_Missile (this script is a clone of M00_Advanced_Guard_Tower_Missile)
	no parameters

JFW_Advanced_Guard_Tower_Gun (this script is a clone of M00_Advanced_Guard_Tower_Gun)
	no parameters

JFW_Building_Gun (generic script for doing base defences)
	Disable_Custom (what custom to use internally to disable the building when the power goes out or when the building is destroyed)
	Positon (what offset from the building controller to spawn the weapon at)
	Weapon (what "vehicle" to use for the weapon, e.g. GDI_AGT is for the AGT missile)
	Visible (if the "vehicle" should be visible (like the AGT guns) or invisible (like the AGT missile), 0 = invisible, 1 = visible)
	Min_Range (if the thing it's going to shoot at is closer than this range not counting any differences in Z position, it won't be shot at)
	Max_Range (if the thing it's going to shoot at is further away than this range, it won't be shot at)

JFW_Building_Gun_Weapon (weapon script for JFW_Building_Gun, don't use this directly)
	Disable_Custom (what custom to use internally to disable the building when the power goes out or when the building is destroyed)
	Visible (if the "vehicle" should be visible (like the AGT guns) or invisible (like the AGT missile), 0 = invisible, 1 = visible)
	Min_Range (if the thing it's going to shoot at is closer than this range not counting any differences in Z position, it won't be shot at)
	Max_Range (if the thing it's going to shoot at is further away than this range, it won't be shot at)
Tips when making this:
Make sure that the team of the gun is different from the team of the thing it's shooting at, otherwise it won't work.
Also, the JFW_Building_Gun script is very sensitive to the sight range of the "gun" vehicle, make sure that the sight
range of the gun vehicle and the range of the weapon preset are all BIGGER than the max range set in the script.
If you still get problems after fiddling with all the settings, let me know and I can investigate further. it's not
the script though, I am now 100% sure of that.
Note that this script has had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Guard_Duty_Helper (helper script for JFW_Guard_Duty, don't attach this to anything)
	ID (the ID of the main vehicle object)
	Helper_Msg (what message to use for the helper communications)

JFW_Guard_Duty (script for vehicle AI)
	Helper_Obj (preset name of object should be used for the special helper object, see below)
	Helper_Bone (what bone to attach the helper object to, muzzle is good)
	Range (the range to use for the attack, should be the same as whatever range the vehicles weapon has)
	Dummy_Object (a dummy object to use when it's not actually attacking something, it won't ever get shot at, just use anything u like)
	Helper_Msg (what message to use for the helper communications)
	Waypathid (the waypath to follow)
	Attack_Time (how many seconds to attack for before stopping the attack)
	Wait_Time (how many seconds to wait for after stopping the attack before it can attack again)

How to use JFW_Guard_Duty:
1.make a new vehicle preset (anywhere in the preset tree will do).
It needs the following settings:
Physics Model of DecorationPhys.
Model name of always\weapons\invisible\v_invs_turret.w3d (since that's invisible & won't get in the way)
No health or armour
Player Type being the same as the AI vehicle.
NotTargetable should be checked
RadarBlipType should be none
no Animation
no Killed Explosion
no Orator type
DefaultHybernationEnable, AllowInnateConversations & UseCreationEffect should be unchecked
weapon should be set to none
sight settings should be set to whatever you want for the vehicle (the ones on the vehicle itself will have no effect)
IsStealthUnit should be unchecked (even if the vehicle you are sticking it on is stealth)
TypeName, FireAnim and Profile should be empty
Aim2d should be off
Type should match whatever the thing you are attaching it to is.
SightDownMuzzle and OccupantsVisible should both be unchecked
NumSeats should be 0
All string ids should be 0
No transitions or scripts should be attached to this object.
Note down its preset name, you will need it later.
Now place the waypath you want the AI vehicle to follow.
Place the AI vehicle next to the start of this Waypath.
Circular waypaths work, don't know what will happen with ones that don't start & end near the same place.
Then attach JFW_Guard_Duty to the object.
Set Helper_Obj to the preset name of the helper preset you made earlier
Set Helper_Bone to the name of a bone on the vehicle. This should be "visible" from as many different angles as possible.
On the Medium Tank for example, the muzzle bone seemed to be the best.
Set Range to the same thing as the range of the vehicle's weapon.
Set Dummy_Object to any object you like, it won't get fired at, it just has to be there to make the logic work.
Set Helper_Msg to anything you like that doesn't conflict with other messages being passed about, this is what gets used to pass messages between the helper object and the main object.
Set Waypathid to the ID of the waypath to follow
Fiddle with the values of attack_time and wait_time to see what values get the best results.

The vehicle itself should have DefaultHibernationEnable cleared.
Setting other settings for the vehicle helps also (depending on the vehicle).
Just play around with the settings and it should work.

JFW_Hunt_Attack (script for vehicles/whatever that sits there until something comes within their sight range then it attacks them)
	Attack_Range (what range to use when attacking)

JFW_Hunt_Blow_Up (script for vehicles/whatever that sits there until something comes within their sight range then it chases them until it reaches them or they disappear. When it gets to them, it explodes)
	Speed (how fast to chase them)
	Distance (how close to get to the object before blowing up)
	Explosion (what explosion to use)

NB, for the 3 Bot scripts, make sure that you use a vehicle that has number of seats = 0 otherwise the players can get in which is bad.
Also, for JFW_Hunt_Attack and JFW_Hunt_Blow_Up, the sight range of the vehicle they are attached to dictates how far away they will spot something to move to.
And, make sure that the thing you use can move (else the scripts won't work :)

JFW_Follow_Waypath (script to make a unit follow a waypath on creation)
	Waypathid (the waypath to follow)
	Speed (how fast to follow it)

JFW_Give_Money (gives money to whatever object sent the custom when a custom is triggered)
	Message (which message to trigger on)
	Entire_Team (give to entire team or just the player, 1 = entire team, 0 = player only)
	Amount (how much to give, positive means add money, negative means take it away)
	Use this script for example with a custom of 1000000025 to trigger when collected

JFW_2D_Sound_Custom (plays a 2d sound when a custom is recieved)
	Message (which message to trigger on)
	Preset (which sound to play)
Just attach multiple copies of this to trigger multiple sounds

JFW_Random_Custom (fires off a custom at a random interval)
	Message (which message to send)
	ID (what to send it to)
	TimerNum (what number to use for the timer)
	Time_Min (minumum time for how long the timer should run for)
	Time_Max (maximum time for how long the timer should run for)
	Repeat (0 = fire off only once, 1 = fire off repeately)

JFW_Resource_Field (when entered by a resource collecter, a timer is started and a flag is cleared. If the resource collecter exits before the timer goes off, the flag is set. When the timer goes off, if the flag is clear, it will send a cash amount to the resource collecter)
	Cash (how much cash this field is worth)
	Time (how long the timer should run for before it grants the cash, i.e. the "resource collection time")
	TimerNum (what number to use for the timer)
	Collected_Preset (this preset will be created when the timer trips. Attach a "destroy this after a short time" script to it)
	Collected_Preset_Distance (how far in units from the harvester the Harvested_Preset should appear. It will be created in a straight line from the direction the harvester is facing)
	Collected_Message (what message to send to the resource collecter to record the cash amount)
	Nod_Preset (the preset name of the Nod Resource Collector)
	GDI_Preset (the preset name of the GDI Resource Collector)
	StartHarvest (the message to send to the harvester when harvesting starts)
	StopHarvest (the message to send to the harvester when harvesting stops)
Use the last 2 with e.g. JFW_Enable_Disable_Animation_On_Custom to make the harvester animate while it's harvesting
n.b., if there is already a resource collector in the zone, any zone entry events by other resource collectors will be ignored (so if the original collector leaves, the new collector then has to leave and re-enter to trigger the start of collection)

JFW_Resource_Collector (this should go on the resource collecter preset to make it work)
	Collected_Mesasge (what message will be sent to record the cash amount)
	Refinery_Message (what message will be sent by the refinery to identify that the refinery has been entered, when this is recieved, it will give the current cash total to the player)
	Cash_Limit (do not let the total cash amount get any larger than this, 0 = no limit)
	Cash_Type (how to record cash, 0 = when someone gets in, reset cash figure, 1 = when someone other than the person who was last in gets in, reset the cash amount, 2 = never reset the cash amount, 3 = same as 2 except it will reset the cash amount every time a "harvested" message is sent)
	Entire_Team (give to entire team or only to player, 0 = player, 1 = team
The cash amount is of course always reset when the refinery is triggered and the cash is delivered to the player.

JFW_Resource_Collector_2 (updated version of JFW_Resource_Collector)
	Collected_Mesasge (what message will be sent to record the cash amount)
	Refinery_Message (what message will be sent by the refinery to identify that the refinery has been entered, when this is recieved, it will give the current cash total to the player)
	Cash_Limit (do not let the total cash amount get any larger than this, 0 = no limit)
	Cash_Type (how to record cash, 0 = when someone gets in, reset cash figure, 1 = when someone other than the person who was last in gets in, reset the cash amount, 2 = never reset the cash amount, 3 = same as 2 except it will reset the cash amount every time a "harvested" message is sent)
	Entire_Team (give to entire team or only to player, 0 = player, 1 = team
The cash amount is of course always reset when the refinery is triggered and the cash is delivered to the player.

JFW_Resource_Refinery (this handles the refinery, when it's entered, it sends a message to the resource collecter which then gives the correct cash amount to the player)
	Refinery_Message (what message to send to the resource collecter to identify that the refinery has been entered)
Make sure you make the zone big enough for these 3 scripts. Use JFW_Debug_Text_File to identify if the zone entry trigger is triggering or not.
Also, make sure that you set the script zones for the field and refinery to "check stars only" = off (otherwise, it won't trigger on the vehicle, only on its occupant. 
A good way to make sure that it's triggering on the vehicle is to put a vehicle into your map at startup and use JFW_Debug_Text_File on the zone. If it's working, you 
should see the ID of the vehicle you put at startup in the log file, in addition to the ID of the player.
BTW, you can also use a specific infantry unit as a valid preset name and it will still work as long as the infantry preset has the JFW_Resource_Collector attached and as long as the refinery and field are set up right

JFW_Custom_Damage_Object (this takes health from an object when it's sent a message)
	ID (what object to damage)
	Message (what message to trigger on)
	Damage (how much damage to do)
note that if you damage something where <damage> is >= <number of hit points the object currently has> it will disappear (this is to prevent unusable, unkillable, unremoveable "dead" objects littering the field)

JFW_Blow_Up_On_Custom (This script will create an explosion at the location of the object that sent the message when a message is sent)
	Explosion (the explosion to create)
	Message (the message to trigger on)
Use 1000000025 to make an exploding powerup

JFW_Blow_Up_Obj_On_Custom (This script will create an explosion at the location of the object that the script is attached to when a message is sent)
	Explosion (the explosion to create)
	Message (the message to trigger on)
Use 1000000029 to make it blow up when someone exits and 1000000028 to make it blow up when someone enters it

JFW_Delay_Custom (This script will introduce a delay into a message that is sent to it)
	Message (the message to delay)
	Delay (the time to delay)
	ID (the ID of the thing to send it on to)

JFW_Startup_Custom (This script will send a custom when it starts)
	Message (the message to send)
	ID (the ID of the object to send it to)

JFW_Clear_Weapons (This script will remove the weapons from any object that enters the zone. If the object gets more weapons and re-enters the zone, they will disappear)
	(no parameters)
This will work for vehicles too, rendering the vehicles weaponless.

JFW_Zone_Send_Custom_Enable_Enter (sends a custom when a zone is entered or exited but only if it's enabled)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Enable_Message (what message will turn the zone on)
	Disable_Message (what message will turn the zone off)

JFW_Zone_Send_Custom_Preset_Enter (sends a custom when a zone is entered or exited but only if the object that entered it is a specific preset)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Preset_Name (what preset name to trigger on)

JFW_Send_Custom_Zone_Enter (sends a custom when a zone is entered or exited)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Team_ID (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
These last 3 send to the object that has entered or exited the zone

JFW_Gate_Zone (this script is useful for doing a Tiberian Sun style gate)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the gate should be open for)
	TimerNum (what number to use for the timer)
	ID (the ID of the gate object, needs to be a StaticAnimPhys, something like a door or a gate but without the Door settings (i.e. a StaticAnimPhys instead of a DoorPhys)
	Animation (the animation to play)
This one doesn't work in multiplayer, use JFW_Gate_Zone_2 instead

JFW_Repair_Zone (this script acts like a repair zone, when it's entered it starts repairing the vehicle that entered it)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Repair_Distance (how far away from the "position" of the zone the vehicle can move before repairing stops)
	Repair_Health (how much health to add per timer)
When this is entered, it starts a timer with a period of 1 second, a different timer per vehicle (i.e. multiple vehicles can use this at once).
Then, when the timer trips, it checks to see if the vehicle needs health, armour or both. Then, it ads "Repair_Health" worth of health to the object and restarts the timer.
If the vechcle moves more than "Repair_Distance" units away from the repair bay, it will stop repairing.
Also, to make the repair bay stop working when the building is blown up, use something like JFW_Death_Send_Custom and JFW_Custom_Destroy_Object to delete the zone.
Also, you need to use a Script_Zone_All for it or else it won't work.

JFW_Repair_Zone_No_Aircraft (like JFW_Repair_Zone but for land vehicles only)
JFW_Repair_Zone_Aircraft_Only (like JFW_Repair_Zone but for aircraft only)
	Preset1 (aircraft preset)
	Preset2 (aircraft preset)
	Preset3 (aircraft preset)
	Preset4 (aircraft preset)
	Preset5 (aircraft preset)
	Preset6 (aircraft preset)
	Preset7 (aircraft preset)
	Preset8 (aircraft preset)
Note that if you don't have 4 aircraft presets (or 4 per team), it doesn't matter what you put for the remaining parameters as long as you put something other than the name of a valid preset that's not an aircraft.
Note that if you have team-specific repair zones, you can have the Nod zone with the preset names of up to 4 Nod aircraft and the GDI zone with the preset names of up to 4 GDI aircraft)
All the stuff above pertaining to JFW_Repair_Zone also applies.

JFW_Base_Defence_No_Aircraft (like JFW_Base_Defence but for land vehicles only)
JFW_Base_Defence_Aircraft_Only (like JFW_Base_Defence but for aircraft only)
	Preset1 (aircraft preset)
	Preset2 (aircraft preset)
	Preset3 (aircraft preset)
	Preset4 (aircraft preset)
	Preset5 (aircraft preset)
	Preset6 (aircraft preset)
	Preset7 (aircraft preset)
	Preset8 (aircraft preset)
Note that if you don't have 4 aircraft presets (or 4 per team), it doesn't matter what you put for the remaining parameters as long as you put something other than the name of a valid preset that's not an aircraft.
Note that you can have the Nod defence with the preset names of up to 4 Nod aircraft and the GDI defence with the preset names of up to 4 GDI aircraft
All the stuff above pertaining to JFW_Base_Defence also applies.
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vehicle is gone, the pilot is shot at)

JFW_Building_Gun_No_Aircraft
JFW_Building_Gun_Weapon_No_Aircraft
These 2 are just like JFW_Building_Gun and JFW_Building_Gun_Weapon except that they take 2 extra parameters:
	Preset1 (aircraft preset)
	Preset2 (aircraft preset)
	Preset3 (aircraft preset)
	Preset4 (aircraft preset)
	Preset5 (aircraft preset)
	Preset6 (aircraft preset)
	Preset7 (aircraft preset)
	Preset8 (aircraft preset)
They will ignore anything they would otherwise shoot at if the preset is one of the 4 specified
Note that if you don't have 4 aircraft presets (or 4 per team), it doesn't matter what you put for the remaining parameters as long as you put something other than the name of a valid preset that's not an aircraft.
Note that you can have the Nod defence with the preset names of up to 4 Nod aircraft and the GDI defence with the preset names of up to 4 GDI aircraft
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Building_Gun_Aircraft_Only
JFW_Building_Gun_Weapon_Aircraft_Only
These 2 are just like JFW_Building_Gun and JFW_Building_Gun_Weapon except that they take 2 extra parameters:
	Preset1 (aircraft preset)
	Preset2 (aircraft preset)
	Preset3 (aircraft preset)
	Preset4 (aircraft preset)
	Preset5 (aircraft preset)
	Preset6 (aircraft preset)
	Preset7 (aircraft preset)
	Preset8 (aircraft preset)
They will ignore anything they would otherwise shoot at if the preset is not of the 4 specified
Note that if you don't have 4 aircraft presets (or 4 per team), it doesn't matter what you put for the remaining parameters as long as you put something other than the name of a valid preset that's not an aircraft.
Note that you can have the Nod defence with the preset names of up to 4 Nod aircraft and the GDI defence with the preset names of up to 4 GDI aircraft
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

N.B., the presets for the "aircraft" stuff above can be other things like spies, stealth units or whatever you like. They can be vehicles or infantry (except for the repair bay obviously)

JFW_Apply_Damage_On_Enter_Timer (This script will apply damage to the object that entered the zone as long as it's in the zone)
	Warhead (the warhead to use)
	Damage (how much damage to do per timer tick)
	Time (how much time to wait before doing the next bit of damage)
	Distance (how far away from the location of the zone the object has to be before the damage stops)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Customs_Play_Cinematic (basicly, when it's sent all the customs, it fires the cinematic)
	Message1 (first message)
	Message2 (second message)
	Message3 (third message)
	Message4 (forth message)
	Message5 (fifth message)
	Message6 (sixth message)
	Message7 (seventh message)
	Message8 (eighth message)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Facing (what direction to fire it off in)

Passing 0 for a message will cause that to be marked as "tripped" on startup (i.e. so you don't have to use all the customs)

JFW_Turret_Spawn_2 (Makes an object at it's controller's bone, this is a clone of Reborn_MMK2_Turret_Spawn but with the extra explosion_bone parameter)
	Turret_Preset (preset name of the object to spawn)
	Bone_Name (the name of the bone to attach the object to)
	Explosion_Preset (made at the bone when the MMK2 or whatevers dies to get rid of the object spawned)
	Explosion_Bone (where to make the explosion)

JFW_Zone_Send_Custom_Keycard (sends a custom when a zone is entered or exited but only if the object that entered it has a specific keycard)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Keycard_Number (the keycard number to look for)

JFW_Zone_Send_Custom_Not_Preset (sends a custom when a zone is entered or exited but only if the object that entered it not a specific preset)
	ID (the id to send the messages to)
	EnterMessage (message to send on entry)
	EnterParam (param to send on entry)
	ExitMessage (message to send on exit)
	ExitParam (param to send on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Preset_Name (what preset name to not trigger on)

With JFW_Zone_Send_Custom_Not_Preset, JFW_Zone_Send_Custom_Keycard, JFW_Zone_Send_Custom_Enable, JFW_Zone_Send_Custom_Preset, JFW_Zone_Send_Custom_Team and 
TDA_Send_Custom_Zone, passing 0 for EnterMessage or ExitMessage will cause no message to be sent. Passing 0 for EnterParam or ExitParam will cause
"the ID of the object that entered/exited the zone" to be passed as the parameter.

JFW_Teleport_Custom (teleports an object when sent a custom)
	Custom (the custom to look for, the parameter passed is the ID of the object to teleport)
	Location (Vector location of where to be sent)
	Object_ID (Object to teleport to)

JFW_Give_Points_On_Death (this gives points to the player that kills whatever it's attached to)
	Points (how many points to give)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Whole_Team (give to team or just player)

JFW_Armour_Regen
	Time (how often to regen armour)
	TimerNum (timer number for the timer)
	Points (how much armour to add)

JFW_Heath_Regen
	Time (how often to regen health)
	TimerNum (timer number for the timer)
	Points (how much health to add)

Note that with Armour_Regen and Health_Regen, the scripts heal the object the script is attached to (so don't attach it to a spawner and think you will make a regen powerup)
Use JFW_Attatch_Script for regen powerups.

JFW_Random_Teleport_Custom (like JFW_Teleport_Custom but teleports to 1 of 5 random locations)
	Custom (the custom to look for, the parameter passed is the ID of the object to teleport)
	Location1 (Vector location of where to be sent)
	Object_ID1 (Object to teleport to)
	Location2 (Vector location of where to be sent)
	Object_ID2 (Object to teleport to)
	Location3 (Vector location of where to be sent)
	Object_ID3 (Object to teleport to)
	Location4 (Vector location of where to be sent)
	Object_ID4 (Object to teleport to)
	Location5 (Vector location of where to be sent)
	Object_ID5 (Object to teleport to)

JFW_Timed_Teleport_Custom (like JFW_Teleport_Custom but has to wait a certain amount of time before it can teleport again)
	Time (how long before it resets)
	TimerNum (timer number for the timer)
	Custom (the custom to look for, the parameter passed is the ID of the object to teleport)
	Location (Vector location of where to be sent)
	Object_ID (Object to teleport to)

JFW_Blow_Up_Location_On_Custom (This script will create an explosion at the location specified in the script when a message is sent)
	Explosion (the explosion to create)
	Location (the loaction to create the explosion)
	Message (the message to trigger on)

JFW_Domination_Zone (when entered, displays 1 of 2 models and sends
	Controler_ID (ID of the controller, a daves arrow or something)
	NeutralCustom (what custom to send to the controler if the zone was neutral before it was captured)
	OwnedCustom (what custom to send to the controler if the zone was already owned before it was captured)
	Nod_Model (name of the w3d file to use for the "Nod has captured the zone" model)
	GDI_Model (name of the w3d file to use for the "GDI has captured the zone" model)
	Neutral_Model (name of the w3d file to use for the "no-one has captured the zone yet" model)
	Capture_Delay_Time (how many seconds to wait after it's captured before the zone can change hands again)
	TimerNum (timer number for the timer)
Note that the parameters should be without the .w3d on the end. (e.g. if the w3d is abc.w3d, pass abc)
Also, note that the models won't animate, even if there is animation info in the w3d file (because I don't know how to make
animation work in this case)
Using models instead of presets makes the code better and easier to test/debug plus also it makes things more light-weight (since you don't
actually need a preset)

JFW_Domination_Controler (controller for the domination mode)
	NeutralCustom (what custom to send to the controller if the zone was neutral before it was captured)
	OwnedCustom (what custom to send to the controller if the zone was already owned before it was captured)
	Time (how often to give points)
	TimerNum (timer number for the timer)
	PointsToGive (how many points to give to GDI for each GDI zone/Nod for each Nod zone)
	PointsToTake (how many points to take from Nod for each GDI zone/GDI for each Nod zone)
	GDIObjectID (the ID of an object somewhere that is guranteed to remain around for the entire game and that is owned by GDI, this is needed to give points to GDI)
	NodObjectID (the ID of an object somewhere that is guranteed to remain around for the entire game and that is owned by Nod, this is needed to give points to Nod)

The GDIObjectID and NODObjectID should refer to objects placed on the map somewhere that are hidden (so the player doesn't know they are there)
Also note that (due to factors beyond my control), it is possible for the points for a team to go negative. However, if they capture enough zones, points can go positive again)

JFW_Object_Draw_In_Order (draws a series of models in order)
	Location (where to draw the model)
	Custom (what custom will cause the next model to be shown)
	BaseName (the base name of the models, numbers from 1 on up will be appended to this to generate the actual name)
	Count (how many models to show)

JFW_Object_Draw_Random (same thing as JFW_Object_Draw_In_Order but random order)
	Location (where to draw the model)
	Custom (what custom will cause the next model to be shown)
	BaseName (the base name of the models, numbers from 1 on up will be appended to this to generate the actual name)
	Count (how many models to show)
	if basename is abc the first model would be abc1.w3d then abc2.w3d and so on up to abc9.w3d then abc10.w3d

JFW_3D_Sound_Custom (plays a 3d sound when it recieves a custom)
	Message (which message to trigger on)
	Preset (which sound to play)
	Position (the position to play the sound at)

JFW_Weapon_Buy (this script is supposed to act as a "purchase point" for weapons, it triggers on zone entry)
	Weapon_Name (the name of the weapon to grant, any valid powerup name should work)
	Cost (the cost)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Note that if the player doesn't have enough cash, no purchase will take place
This won't work on a ScriptZoneAll (i.e. vehicles trigger it too) for granting weapons.
Granting health/armour/etc will work, as will granting ammo.

JFW_Heal_Zone (this script acts like a heal zone, when its entered it starts healing the infantry that entered it)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Heal_Distance (how far away from the "position" of the zone the infantry can move before healing stops)
	Heal_Health (how much health to add per timer)
When this is entered, it starts a timer with a period of 1 second, a different timer per infantry (i.e. multiple infantry units can use this at once). It only works for things other than "Stars" (i.e. anything other than players)
Then, when the timer trips, it checks to see if the infantry needs health, armour or both. Then, it ads "Heal_Health" worth of health to the object and restarts the timer.
If the vehicle moves more than "Heal_Distance" units away from the heal zone, it will stop healing.
Also, to make the heal zone stop working when the building is blown up, use something like JFW_Death_Destroy_Object to delete the zone.

JFW_Disable_Transitions_Zone (when a vehicle enters this zone, transitions are disabled, when it exits the zone, transitions are enabled)

JFW_Bounce_Zone_Entry_All_Directions (this script adds a fixed value to the position of anything that enters the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ZAmount (what to add to the position)
	Amount (what to add to the x,y position in the direction the object is facing)

JFW_Death_Destroy_Object (will destroy an object when the thing it's attached to dies)
	ID (what object to destroy, don't use this for buildings/building controlers)

JFW_Drive_To_Player (when created, this object will drive to the nearest player)
	Speed (how fast to drive)
	Arrive_Distance (how close to the player to arrive)
Note, if there is no player around, this script may fail or crash renegade. So make sure it's only 
used on vehicles that are created at run-time, not vehicles that are on the map at startup

JFW_Invulnerability_Timer (invulerability script for iron curtain and whatever else you want)
	Invulerability_Time (how long to be invulnerable for)
	Invunerability_TimerNum (tumer number for the invulnerability timer)
Note that because of how this works, if the object is damaged enough to kill it in one hit, it will probably still die.
Since there is no other way to make things invulnerable, this is the best I can do.

JFW_Iron_Curtain_Zone (zone script for iron curtain)
	Time (charge timer)
	TimerNum (charge timer number)
	Animation1 (animation 1 script)
	Animation1Loc (animation 1 location)
	Animation1Facing (animation 1 facing)
	Animation1Slot (animation 1 slot number)
	Animation2 (animation 2 script)
	Animation2Loc (animation 2 location)
	Animation2Facing (animation 2 facing)
	Animation2Slot (animation 2 slot number)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Animation3 (animation 3 script)
	Animation3Loc (animation 3 location)
	Animation3Facing (animation 3 facing)
	Animation3Slot (animation 3 slot number)
	BuildingID (building ID)
	Invulerability_Time (invulnerability time)
	Invunerability_TimerNum (invulnerability timer number)
	Animation1Msg (message that is sent by the Animation 1 script to indicate that it's finished)
	Animation2Msg (message that is sent by the Animation 2 script to indicate that it's finished)
	Animation3Msg (message that is sent by the Animation 3 script to indicate that it's finished)
	OffID (powered off object ID)
	OnID (powered on object ID)
	CinematicPowerOff (hide the objects before playing the cinematics?, 0 = indicates don't hide, 1 = hide)

To make it stop working when the power goes down or the building, use JFW_Death_Destroy_Object
What this script does:
On startup, sets the enabled flag to false, hides the "powered on" object and shows the "powered off" object
Then, starts the charge timer
On zone entry, it checks the player type to make sure it's the right team and also checks the zone is enabled
Then, if the object that entered the zone is a player, it disables the zone (this way, if a player drives a 
vehicle in, both player and vehicle get the effect).
Then, it re-starts the charge timer (ready for the next use of the zone)
And, it plays animation 2 (activated)
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
The cinematic will only play and the charge timer will only be reset when the player enters, not the vehicle (that way it doesn't play twice)
Plus, it attaches the JFW_Invulnerability_Timer script to make the vehicle invulnerable
On death of the zone, it plays animation 3 (power down) but only if it's currently powered up.
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
On timer expired, if the timer that expired was the charge timer, it plays animation 1 (powerup) and enables the zone
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
On custom, if the message is the first animation message, it shows the "power on" object and hides the "power off" object.
If it is the second animation message, it plays animation 3 (power down)
If it is the third animation message, it shows the "power off" object and hides the "power on" object.

JFW_Chrono_Controler_Zone (zone script for the chronosphere)
	Spawner_Time (how long before the beacon will spawn)
	Spawner_TimerNum (timer number for the beacon timer)
	Spawner_Preset (beacon powerup preset)
	Spawner_Location (beacon powerup spawn location)
	Chrono_Time (time the zone is active for)
	Chrono_TimerNum (timer number for the active timer)
	Activate_Time (time from when the beacon is planted to when the zone goes live)
	Activate_TimerNum (timer number for the active timer)
	DeathMessage (message to indicate beacon disablement)
	Message (message to indicate beacon placement)
	Animation1 (animation 1 script)
	Animation1Loc (animation 1 location)
	Animation1Facing (animation 1 facing)
	Animation1Slot (animation 1 slot number)
	Animation2 (animation 2 script)
	Animation2Loc (animation 2 location)
	Animation2Facing (animation 2 facing)
	Animation2Slot (animation 2 slot number)
	Animation3 (animation 3 script)
	Animation3Loc (animation 3 location)
	Animation3Facing (animation 3 facing)
	Animation3Slot (animation 3 slot number)
	BuildingID (building ID)
	ZAdjust (how much to add to the Z when teleporting, so you don't teleport into the ground)
	Animation1Msg (message that is sent by the Animation 1 script to indicate that it's finished)
	Animation2Msg (message that is sent by the Animation 2 script to indicate that it's finished)
	Animation3Msg (message that is sent by the Animation 3 script to indicate that it's finished)
	OffID (powered off object ID)
	OnID (powered on object ID)
	CinematicPowerOff (hide the objects before playing the cinematics?, 0 = indicates don't hide, 1 = hide)

On startup, it disables the zone and starts the spawner timer.
And it hides the "power on" object and shows the "power off" object.
On custom, if it's the beacon custom it records the position of the beacon object and starts the beacon timer.
If it's the death custom, it sets the flag to tell the script that it's recieved the death custom
If it's the first animation message, it shows the "power on" object and hides the "power off" object.
If it is the second animation message, it plays animation 3 (power down)
If it is the third animation message, it shows the "power off" object and hides the "power on" object.
on zone entered it checks to see if the zone is enabled
then it disables the zone
then it plays animation 2 (activated)
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
and then it teleports to the stored location + the ZAdjust
On death of the zone, it plays animation 3 (power down) but only if it's currently powered up.
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
On timer expired, if it's the spawner timer that expired, it creates the object specified
If it's the beacon timer, and it hasnt recieves the beacon death message, it enables the zone and plays animation 1 (powerup)
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
if it's the chrono timer and the zone is still enabled, it plays animation 3 (power down) and starts the spawner timer again and also disables the zone
If the CinematicPowerOff flag is set to 1, it will hide both objects before playing the cinematic.
This last test makes sure that when the zone is disabled by it being acivated, it doesn't play the power down animation on an 
already powered down chronosphere

This is what happens when the Iron Curtain script/Chronosphere script play the animations:
Firstly, an Invisible_Object is created at the appropriate location.
Then the facing is set for that object.
Then the script Test_Cinematic is attached to the object with the correct parameter.
Then the "BuildingID" parameter is sent to the object along with the slot number. Basicly, the Test_Cinematic script
will take this ID and will put the object it corresponds to into the appropriate slot (where it can then be accessed
in the cinematic just like an object created with the cinematic object creation commands)
Note that the custom must send the appropriate message (with the Send_Custom script command) to tell the object the cinematic has finished

The "powered on" and "powered off" objects are objects that are placed in leveledit (tiles for example) that look the same except that one plays an effect or something to indicate "power on".

JFW_Looping_Cinematic (plays a cinematic on a repeating timer, starting at object creation)
	Script_Name (the name of the *.txt script to fire)
	Location (the location to fire it off at)
	Facing (what direction to fire it off in)
	Time (time to wait before re-starting the cinematic)
	TimerNum (timer number)

JFW_Spawn_Object_Death_Enable (This script will create an object upon death of a unit, does not work on player spawners, only if it's enabled)
	Drop_Object (Preset STRING Name) example POW_Flamethrower_Player
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	Enable_Message (what message will be sent to enable the drop)
	Disable_Message (what message will be sent to disable the drop)
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died

JFW_Spawn_Object_Death_Enable_Random (This script will create an object upon death of a unit, does not work on player spawners, only if it's enabled, with random chance to spawn/not spawn)
	Drop_Object (Preset STRING Name) example POW_Flamethrower_Player
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	Enable_Message (what message will be sent to enable the drop)
	Disable_Message (what message will be sent to disable the drop)
	Probobility (what % chance for the drop to happen)
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died

One use for these last 2 is to make a visceroid spawn when something dies inside a tiberium field.
Use the zone send custom scripts plus one of the 2 above to implement the feature

JFW_Spawn_Object_Death_Random (This script will create an object upon death of a unit, does not work on player spawners, with random chance to spawn/not spawn)
	Drop_Object (Preset STRING Name) example POW_Flamethrower_Player
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	Probobility (what % chance for the drop to happen)
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died

JFW_Created_Send_Custom (this script will send a custom when the object it's attached to is created)
	ID (the object to send it to)
	Message (the message to send)

JFW_Damaged_Send_Custom (this script will send a custom when the object it's attached to is damaged)
	ID (the object to send it to)
	Message (the message to send)

JFW_Enemy_Seen_Send_Custom (this script will send a custom when the object it's attached to spots an enemy)
	ID (the object to send it to)
	Message (the message to send)
Note that for this one, the Parameter for the message is the ID of the enemy object that was seen.
You could use this script on an object combined with e.g. JFW_Teleport_Custom to make a "transport 
any enemies seen by this object to x location" setup.

JFW_Create_Object_Custom (will spawn an object when it recieves a custom)
	Object (preset to spawn)
	Location (where to spawn)
	Facing (direction to face)
	Mesasge (message to listen for)

JFW_Enable_Spawner_On_Custom (will enable or disable a spawner when sent a custom)
	ID (the ID of the spawner)
	Enable (1 = enable, 0 = disable)
	Message (the message to listen for)

JFW_Set_Animation_On_Custom (starts an animation when a custom is sent)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Loop (whether to loop or not, 0 = not loop, 1 = loop)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	Message (message to listen for)
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)

JFW_Enable_Disable_Animation_On_Custom (enables or disables an animation on custom)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	Enable (enable message)
	Disable (disable message)
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
The animation will now (as of 2.1) be stopped automatically when the disable happens.
When enabled, it will loop automaticly.

JFW_vehicle_Animation (script for animated vehicles such as bots or mecha)
	Time (how often to check for movement, 0.1 is a good value)
	TimerNum (number for the timer)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
How this script works:
On created, it sets the stored position to 0,0,0 (this is because vehicles can fall slightly when created)
It also sets the "enabled" flag to false.
And it starts a timer to check for movement (since there is no direct way to detect movement)
On timer expired, it checks the current position of the object against the stored position, if they are different it stores the new position.
But first, it checks if the stored position is 0,0,0, if it is, it sets it to the current position.
If the enabled flag is false, it starts the animation and sets the enabled flag to true.
If the positions are the same, it sets the enabled flag to false.
The animation will loop until the vehicle stops moving then stop animating

I recommend using weapons without "recoil" (or whatever engine feature it is that makes e.g. the med tank move backwards slightly when it fires)
since it will look better :)

JFW_Custom_Toggle_Visible (toggles whether an item will be visible (i.e. rendered) or not)
	Mesasge (the message to listen for)
	Visible (0 = invisible, 1 = visible)

JFW_Visible_Person_In_vehicle (makes a person model show up in a vehicle when someone is inside it)
	BoneName (the bone to attach the person model to)
	ModelName (the name of the w3d file to use for the person model, without the .w3d on the end)
	Animation (the name of the animation to play on the person model, it will loop indefinatly as long as the person model is visible)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)

Note that only one model will appear no matter how many people are in the vehicle
However, if you want 2 models to appear (e.g. if you want 1 model for gunner and 1 for driver), check the script below.
Also, there is no way to make different characters show different models when inside the vehicle, there is a different script for that.

JFW_Visible_People_In_vehicle (makes a person model show up in a vehicle when someone is inside it, this one does it twice for 2 people in the vehicle)
These parameters apply to the model that will be used when the first person enters the vehicle
	BoneName1 (the bone to attach the person model to)
	ModelName1 (the name of the w3d file to use for the person model, without the .w3d on the end)
	Animation1 (the name of the animation to play on the person model, it will loop indefinetly as long as the person model is visible)
	Subobject1 (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame1 (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame1 (frame to go to, -1 means "end of animation")
	Blended1 (some value for blended animation, only applies to Solders)
These parameters apply to the model that will be used when the second person enters the vehicle.
	BoneName2 (the bone to attach the person model to)
	ModelName2 (the name of the w3d file to use for the person model, without the .w3d on the end)
	Animation2 (the name of the animation to play on the person model, it will loop indefinatly as long as the person model is visible)
	Subobject2 (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame2 (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame2 (frame to go to, -1 means "end of animation")
	Blended2 (some value for blended animation, only applies to Solders)

If a third person enters the vehicle, nothing happens (i.e. no more models are displayed)
Note that the Worldbox of the character model may cause the vehicle not to move properly.
So, using a character model without a worldbox may be necessary. (I don't know gmax so I can't be sure)

JFW_Blow_Up_On_Enter_Random (This script will create an explosion at the location of the object that entered the zone, there is a % chance that this will happen)
	Explosion (the explosion to create)
	Probobility (what % chance for the explosion to happen)

JFW_Zone_Send_Custom_Cost (this script sends a custom on zone entry but only if the player can pay the cost specified, it will take money away if the cost is payable)
	ID (what object to send the custom to)
	Message (message to send)
	Param (what parameter to send, 0 means "the ID of the thing that entered the zone)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Cost (how much money to take away, use positive numbers to remove money. don't use negative numbers, they won't work)

JFW_Engineer_Target (clone of M03_Engineer_Target)

JFW_Engineer_Repair (clone of M03_Engineer_Repair)
	Repair_Priority (priority for repairing)

To prevent the problem where when you get out of a vehicle, it is no longer being repaired by the bots, use NH_SetTeam_OnLeave, it should work.
Also, in addition, JFW_Engineer_Target has been modified to check the Shield Strength (i.e. armour) of the object in addition to the health.
The repair object can be any vehicle or infantry that has a repair gun weapon and wheels/legs/tracks/whatever so it can move.
Also, there is a limit to how far away the repair script will detect a damaged target (since I can't find the sound presets it's using, I can't identify how far that is)

JFW_vehicle_Damage_Zone (clone of JFW_Apply_Damage_On_Enter_Timer but for vechcles only)
	Warhead (the warhead to use)
	Damage (how much damage to do per timer tick)
	Time (how much time to wait before doing the next bit of damage)
	Distance (how far away from the location of the zone the object has to be before the damage stops)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This script damages vehicles only, not infantry

JFW_Cinematic_Attack (when this script is created, it will attack a specifc object, use in cinematics for example)
	Priority (priority for the action)
	ID (the object to attack)
	Range (maximum effective range of the attack)
	Deviation (deviation of the attack)
	Primary (1 = use primary weapon,2 = don't use primary weapon)

JFW_Customs_Send_Custom (basicly, when it's sent all the customs, it sends another custom)
	Message1 (first message)
	Message2 (second message)
	Message3 (third message)
	Message4 (forth message)
	Message5 (fifth message)
	Message6 (sixth message)
	Message7 (seventh message)
	Message8 (eighth message)
	Mesasge (the message to send)
	Param (the parameter to send)
	ID (the object to send it to)

Passing 0 for a message will cause that to be marked as "tripped" on startup (i.e. so you don't have to use all the customs)
Note that you can dasy-chain these if you need more customs.

JFW_Object_Draw_In_Order_2 (draws a series of models in order)
	Location (where to draw the model)
	Custom (what custom will cause the next model to be shown)
	BaseName (the base name of the models, numbers from Start_Number on up will be appended to this to generate the actual name)
	Count (how many models to show)
	Start_Number (what model number to start at)

JFW_Custom_Multiple_Send_Custom (when this recieves a particular custom a certain number of times it sends a custom)
	RecieveMessage (message to recieve)
	RecieveCount (how many times to recieve, when it has recieved it exactly this many times, it sends the other custom)
	SendMessage (message to send)
	SendParam (parameter to send)
	SendID (ID of object to send it to)

JFW_Create_Destroy_Object_On_Custom (creates an object on a custom then destroys it on another custom)
	Location (where to create the object)
	Facing (what direction it should face)
	Preset (what preset to use)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)

JFW_Create_Destroy_Object_On_Custom_Animate (creates an object on a custom plus sets an animation on it then destroys it on another custom)
	Location (where to create the object)
	Facing (what direction it should face)
	Preset (what preset to use)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)
	Animation (animation to play on creation)
	Loop (whether to loop or not, 0 = not loop, 1 = loop)
	Subobject (the subobject to use for the creation animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)

JFW_Create_Destroy_Object_On_Custom_Animate_2 (creates an object and sets an animation on it on a custom then plays another animation on a second custom and destroys the object when the second animation is finished)
	Location (where to create the object)
	Facing (what direction it should face)
	Preset (what preset to use)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)
	CreateAnimation (animation to play on creation)
	CreateLoop (whether to loop or not, 0 = not loop, 1 = loop)
	CreateSubobject (the subobject to use for the creation animation, a value of 0 will mean "no subobject")
	CreateFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	CreateLastFrame (frame to go to, -1 means "end of animation")
	CreateBlended (some value for blended animation, only applies to Solders)
	DestroyAnimation (animation to play on destroy)
	DestroySubobject (the subobject to use for the destroy animation, a value of 0 will mean "no subobject")
	DestroyFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	DestroyLastFrame (frame to go to, -1 means "end of animation")
	DestroyBlended (some value for blended animation, only applies to Solders)
	Time (how long to wait after starting the DestroyAnimation before destroying the object)
	TimerNum (number to use for the timer)

JFW_Damaged_Send_Custom_Amount (sends a custom when the object is damaged based on how much damage was done)
	ID (object to send the message to)
	Message1 (message to send when the damage amount is less than the specified amount)
	Message2 (message to send when the damage amount is greater than or equal to the amount)
	Damage (amount of damage to test for, negative amounts mean repair, positive mean damage)

JFW_Invulnerable_On_Custom (makes an object invulnerable or turns it off when sent a custom)
	Message (message to wait for, send 1 for parameter to turn on, 0 for parameter to turn off)
The object made invulnerable will still die if it's hit by a weapon big enough to kill it in one hit.
But unfortunatly, there is nothing I can do about that :(

JFW_Invulnerable_On_Custom_2 (makes an object invulnerable or turns it off when sent a custom)
	Message (message to wait for, send 1 for parameter to turn on, 0 for parameter to turn off)
	InvulnerableArmour (armour type to use when it's invulnerable)
	NormalArmour (armour type to restore it to when it stops being invulnerable)
This is a better version of JFW_Invulnerable_On_Custom that overcomes the limitation mentioned above.
Use Blamo for the invulnerable armour, this should protect against everything.
The bad news is that it appears as though buildings ignore the Shield and Skin settings altogether.

JFW_Per_Preset_Visible_Person_In_vehicle (makes a person model show up in a vehicle when someone is inside it)
	BoneName (bone to attach the person object to)
	Message (message to send)

JFW_Per_Preset_Visible_People_In_vehicle (makes a person model show up in a vehicle when someone is inside it, this one does it twice for 2 people in the vehicle)
	BoneName1 (bone name to attach the person object to for the first model)
	BoneName2 (bone name to attach the person object to for the second model)
	Message1 (message to send for the first model)
	Message2 (message to send for the second model)

JFW_Visible_Person_Settings (script for doing the per-infantry-preset part of the visible people logic)
	Message (message to listen for)
	ModelName (the name of the w3d file to use for the person model, without the .w3d on the end)
	Animation (the name of the animation to play on the person model, it will loop indefinatly as long as the person model is visible)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)

Basicly, you stick JFW_Visible_Person_Settings on an infantry preset and JFW_Per_Preset_Visible_Person_In_vehicle or JFW_Per_Preset_Visible_People_In_vehicle on the vehicle preset.
If you don't put JFW_Visible_Person_Settings on an infantry unit, you will get an invisible model.
don't forget to do it for both teams (even if you only have vehicles with visible drivers for one team, remember that the other team may steal one)
You can use different messages (and different settings for JFW_Visible_Person_Settings) for each vehicle if you want.
The model you use must have no worldbox/boundingbox/etc and should have all collision flags set to off.

JFW_Building_Explode_No_Damage (clone of M00_BUILDING_EXPLODE_NO_DAMAGE_DAK

JFW_Visceroid_Innate (clone of M00_Visceroid_Innate_DAY)

JFW_No_Innate (clone of M00_No_Innate)

JFW_Credit_Trickle (clone of GTH_Credit_Trickle)
	Credits (how many credits to give)
	Delay (how often to give them)

JFW_PCT_Pokable (clone of M00_PCT_Pokable_DAK)
This script will now be used instead of the westwood one, it contains a fix to prevent the cheat where cheaters can shoot the PTs, killing them.

JFW_vehicle_Regen (clone of M00_vehicle_Regen_DAK)

JFW_Disable_Transition (clone of M00_Disable_Transition)

JFW_Permanent_No_Falling_Damage (clone of M00_Permanent_No_Falling_Damage_IML)

JFW_Death_Powerup (clone of M00_Death_Powerup)

JFW_GrantPowerup_Created (clone of M00_GrantPowerup_Created)

JFW_CnC_Crate (clone of M00_CnC_Crate)

JFW_Tiberium_Refinery (clone of M00_Tiberium_Refinery)
	MoneyAmount (how many credits to give)
	Delay (how often to give them)

JFW_GrantMoney_Powerup (clone of M00_GrantMoney_Powerup)
	ScoreAmount (how much cash to give)
	Entire_Team (give to everyone)
	Randomizer (randomize how much cash to give)

JFW_GrantScore_Powerup (clone of M00_GrantScore_Powerup)
	ScoreAmount (how many points to give)
	Entire_Team (give to everyone)
	Randomizer (randomize how many points to give)

JFW_Disable_Physical_Collision (clone of M00_Disable_Physical_Collision_JDG)

JFW_Enable_Physical_Collision (clone of M00_Enable_Physical_Collision_JDG)

JFW_DestroyedStateObject (clone of M00_DestroyedStateObject_DAY)
	OriginalModelFacing (direction to make the destroyed state object face)
	DestroyedModelPreset (preset to use for the destroyed state object)

JFW_Play_Sound (clone of M00_Play_Sound)
	Sound_Preset (sound preset to use)
	Is_3D (is it a 3d sound)
	Offset (offset from object this is attached to to play the sound)
	Offset_Randomness (randomness for the offset)
	Frequency_Min (minumum replay frequency)
	Frequency_Max (maximum replay frequency)

JFW_Engine_Sound (clone of RMV_Engine_Sound)
	Preset (preset to play)
	Bone (bone to play it on)

JFW_Building_Gun_Secondary
JFW_Building_Gun_Weapon_Secondary
JFW_Building_Gun_No_Aircraft_Secondary
JFW_Building_Gun_Weapon_No_Aircraft_Secondary
JFW_Building_Gun_Aircraft_Only_Secondary
JFW_Building_Gun_Weapon_Aircraft_Only_Secondary

These scripts are clones of the non-secondary version 
with the only difference being that they alternate 
between primary and secondary weapons when firing
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Base_Defence_Secondary
JFW_Base_Defence_No_Aircraft_Secondary
JFW_Base_Defence_Aircraft_Only_Secondary

These scripts are clones of the non-secondary version 
with the only difference being that they alternate 
between primary and secondary weapons when firing
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Base_Defence_Animated (new version of JFW_Base_Defence that has an animation that is played before and after firing.
	MaxAttackDistance (maximum distance away from the object that attack will take place)
	AttackTimer (how long the defence should stay "active" (e.g. the popup sam site stay popped up)
	Animation (what animation to play on the defence vehicle)
	LastFrame (how long the animation is in frames)
	PopupTime (how long it should wait after it starts the animation before it attacks stuff, this is to give it enough time to finish animating)
Note that it will play the animation from 0 to "lastframe" when it activates and then from "lastframe" to 0 when it deactivates.
Note that this script has had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vehicle is gone, the pilot is shot at)
Basicly, what this script does is, when it first spots an enemy, it plays the animation from 0 to lastframe, starts the popup timer and starts the attack timer.
When the popup timer expires, it will start shooting like regular turrets do.
When the attack timer expires, it plays the animation from lasframe to 0 and wont shoot anymore.

There is a gmax file in the buildings.zip on the westwood FTP site in the single_player\NOD sam site\_current\_keep folder called b_samsite-up.gmax.
This gmax file should be usable as-is with no modifications for this script.

JFW_Base_Defence_Animated_No_Aircraft same as JFW_Base_Defence_Animated but only pops up and attacks presets not in the list
JFW_Base_Defence_Animated_Aircraft_Only same as JFW_Base_Defence_Animated but only pops up and attacks presets in the list
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vehicle is gone, the pilot is shot at)

JFW_Base_Defence_Animated_Secondary
JFW_Base_Defence_Animated_No_Aircraft_Secondary
JFW_Base_Defence_Animated_Aircraft_Only_Secondary
These scripts are clones of the non-secondary version 
with the only difference being that they alternate 
between primary and secondary weapons when firing
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Building_Gun_Animated (version of JFW_Building_Gun that allows for a charge animation before it fires)
	Disable_Custom (what custom to use internally to disable the building when the power goes out or when the building is destroyed)
	Positon (what offset from the building controller to spawn the weapon at)
	Weapon (what "vehicle" to use for the weapon, e.g. GDI_AGT is for the AGT missile)
	Visible (if the "vehicle" should be visible (like the AGT guns) or invisible (like the AGT missile), 0 = invisible, 1 = visible)
	Min_Range (if the thing it's going to shoot at is closer than this range not counting any differences in Z position, it won't be shot at)
	Max_Range (if the thing it's going to shoot at is further away than this range, it won't be shot at)
	EffectPreset (preset to use for the charge effect)
	EffectAnimation (animation to use for the charge effect)
	EffectTime (how long to wait after starting the charge effect animation before firing)
	EffectFrame (last frame of the effect animation)

JFW_Building_Gun_Animated_Weapon (version of JFW_Building_Gun_Weapon that allows for a charge animation before it fires)
	Disable_Custom (what custom to use internally to disable the building when the power goes out or when the building is destroyed)
	Visible (if the "vehicle" should be visible (like the AGT guns) or invisible (like the AGT missile), 0 = invisible, 1 = visible)
	Min_Range (if the thing it's going to shoot at is closer than this range not counting any differences in Z position, it won't be shot at)
	Max_Range (if the thing it's going to shoot at is further away than this range, it won't be shot at)
	EffectPreset (preset to use for the charge effect)
	EffectAnimation (animation to use for the charge effect)
	EffectTime (how long to wait after starting the charge effect animation before firing)
	EffectFrame (last frame of the effect animation)
Note that this script has had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

Tips when making this:
Make sure that the team of the gun is different from the team of the thing it's shooting at, otherwise it won't work.
Also, the JFW_Building_Gun script is very sensitive to the sight range of the "gun" vehicle, make sure that the sight
range of the gun vehicle and the range of the weapon preset are all BIGGER than the max range set in the script.
Also, with the animation, it starts at frame 0 when it's not shooting. Then, when it charges up, it goes from frame 0 to EffectFrame.
Then, when it fires (i.e. after EffectTime has run out), it goes back from EffectFrame to 0.

JFW_Building_Gun_Animated_No_Aircraft same as JFW_Building_Gun_Animated but only attacks presets not in the list
JFW_Building_Gun_Animated_Aircraft_Only same as JFW_Building_Gun_Animated but only attacks presets in the list
JFW_Building_Gun_Animated_Weapon_No_Aircraft same as JFW_Building_Gun_Animated_Weapon but only attacks presets not in the list
JFW_Building_Gun_Animated_Weapon_Aircraft_Only same as JFW_Building_Gun_Animated_Weapon but only attacks presets in the list
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Building_Gun_Animated_Secondary
JFW_Building_Gun_Animated_Weapon_Secondary
JFW_Building_Gun_Animated_No_Aircraft_Secondary
JFW_Building_Gun_Animated_Weapon_No_Aircraft_Secondary
JFW_Building_Gun_Animated_Aircraft_Only_Secondary
JFW_Building_Gun_Animated_Weapon_Aircraft_Only_Secondary
These scripts are clones of the non-secondary version 
with the only difference being that they alternate 
between primary and secondary weapons when firing
Note that these scripts have had a bug fix in 1.8 so it will always attack the vehicle, never the pilot of the vehicle (obviously once the vechile is gone, the pilot is shot at)

JFW_Play_Animation_Destroy_Object (on startup, this script plays an animation once and when the animation finishes, it destroys itself)
	Animation (the animation to use)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)

JFW_Custom_Send_Custom_Parameter (this script listens for messages in a certain range and sends it on)
	ID (ID of object to send to)
	BaseMessage (first message)
	Limit (last message)
What this script does is, when it gets a message that is >= BaseMessage and <= Limit, it subtracts 
BaseMessage from the passed in message. Then, it sends that value as the param and BaseMessage as the message.
It sends it to the object denoted by ID

JFW_Create_Destroy_Object_On_Custom_Bone (creates an object on a custom then destroys it on another custom)
	Location (where to create the object)
	Facing (what direction it should face)
	Preset (what preset to use)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)
	BoneName (which bone on the object holding the JFW_Create_Destroy_Object_On_Custom it should attach the created object to)

JFW_Mobius_Script (clone of Dr_Mobius_Script)

JFW_ArmorMedal_TextMessage (clone of M00_ArmourMedal_TextMessage_JDG)

JFW_HealthMedal_TextMessage (clone of M00_HealthMedal_TextMessage_JDG)

JFW_C130_Explosion (clone of M00_C130_Explosion)

JFW_Send_Object_ID (clone of M00_Send_Object_ID)
	Receiver_ID (the reciever of the message)
	Param (the parameter to send)
	Delay (the delay before sending)

JFW_Ion_Cannon_Sound (clone of M00_Ion_Cannon_Sound)
	Number (sound number to use)

JFW_NukeStrike_Anim (clone of M00_NukeStrike_Anim)

JFW_PlayAnimation_DestroyObject (clone of M00_PlayAnimation_DestroyObject_DAY)
	AnimationName (animation to play)

JFW_BuildingStateSoundSpeaker (clone of M00_BuildingStateSoundSpeaker)
	Sound_Normal (normal sound to play)
	Sound_Destroyed (destroyed sdound to play)
	BuildingController_ID (building controler ID)
	Is_3D (is the sound 3D)
	Offset (offset to play the sound at)
	Offset_Randomness (random factor for the offset)
	Frequency_Min (minumum time between the sound playing)
	Frequency_Max (maximum time between the sound playing)
	Is_3D_Destroyed (is the destroyed sound 3D)
	Offset_Destroyed (offset to play the destroyed sound at)
	Offset_Randomness_Destroyed (random factor for the destroyed offset)
	Frequency_Min_Destroyed (minumum time between the destroyed sound playing)
	Frequency_Max_Destroyed (maximum time between the destroyed sound playing)
	Explosion_Name (explosion to use)

JFW_BuildingStateSoundControler (clone of M00_BuildingStateSoundControler)
	BuildingSpeaker_ID (ID of the building speaker)

JFW_Play_Sound_Object_Bone (clone of M00_Play_Sound_Object_Bone_DAY)
	Sound_Preset (sound)
	Frequency_Min (minumum time between the sound playing)
	Frequency_Max (maximum time between the sound playing)

JFW_Cinematic_Primary_Killed (clone of Test_Cinematic_Primary_Killed)
	CallbackID (ID to send back to when killed)

JFW_Spawn_Timer (starts a timer when sent a custom, spawns an object when the timer expires)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	SpawnCustom (Custom to watch for)
	TriggerCustom (Custom to send when spawning)
	TriggerID (ID to send spawn custom to)
	SpawnPreset (preset to spawn)
	SpawnLocation (location to spawn)

JFW_Death_Destroy_Object_Delay (like JFW_Death_Destroy_Object but with a delay)
	ID (object to destroy)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Conyard (script for a construction yard)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	DisableCustom (custom to send to disable the construction yard, e.g. on low power)
	Health (how much health to repair every tick)
Stick this on the construction yard building controller, fill in the parameters and the script does the rest.
No need to put anything on the other buildings or to input their IDs anywhere.

JFW_Character_Buy (this will buy a new character for the player entering the zone)
	Preset_Name (character preset to buy)
	Cost (cost of the preset)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This won't trigger if the player buying is in a vehicle.

JFW_Zone_Character (this will change the character of the player entering the zone to a new one)
	Preset_Name (character preset to change to)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This won't trigger if the player buying is in a vehicle.

JFW_Teleport_Zone_Team (like TDA_Teleport_Zone but team specific)
	Location (Vector location of where to be sent)
	Object_ID (Object to teleport to)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Create_Destroy_Object_On_Enter (will create an object on zone entry and destroy it on zone exit)
	Location (lcoation to create the object)
	Facing (direction it should face)
	Preset (preset to create)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Custom_Send_Ordered_Customs (will send 8 customs in order when it recieves a custom, i.e. first time it recieves custom, custom1 is sent)
	ID1 (first ID to send to)
	ID2 (second ID to send to)
	ID3 (third ID to send to)
	ID4 (forth ID to send to)
	ID5 (fifth ID to send to)
	ID6 (sixth ID to send to)
	ID7 (seventh ID to send to)
	ID8 (eighth ID to send to)
	Custom1 (first custom to send)
	Custom2 (second custom to send)
	Custom3 (third custom to send)
	Custom4 (forth custom to send)
	Custom5 (fifth custom to send)
	Custom6 (sixth custom to send)
	Custom7 (seventh custom to send)
	Custom8 (eighth custom to send)
	WaitCustom (custom to listen for)

JFW_Custom_Send_Customs (will send multiple customs when it recieves a custom)
	ID1 (first ID to send to)
	ID2 (second ID to send to)
	ID3 (third ID to send to)
	ID4 (forth ID to send to)
	ID5 (fifth ID to send to)
	ID6 (sixth ID to send to)
	ID7 (seventh ID to send to)
	ID8 (eighth ID to send to)
	Custom1 (first custom to send)
	Custom2 (second custom to send)
	Custom3 (third custom to send)
	Custom4 (forth custom to send)
	Custom5 (fifth custom to send)
	Custom6 (sixth custom to send)
	Custom7 (seventh custom to send)
	Custom8 (eighth custom to send)
	WaitCustom (custom to listen for)

JFW_Custom_Send_Custom_Multiple_IDs (will send a custom to multiple objects when it recieves a custom)
	ID1 (first ID to send to)
	ID2 (second ID to send to)
	ID3 (third ID to send to)
	ID4 (forth ID to send to)
	ID5 (fifth ID to send to)
	ID6 (sixth ID to send to)
	ID7 (seventh ID to send to)
	ID8 (eighth ID to send to)
	WaitCustom (custom to wait for)
	SendCustom (custom to send)
	
JFW_Set_Health_On_Custom
JFW_Add_Health_On_Custom
JFW_Set_Max_Health_On_Custom
JFW_Add_Max_Health_On_Custom
JFW_Set_Shield_Strength_On_Custom
JFW_Add_Shield_Strength_On_Custom
JFW_Set_Max_Shield_Strength_On_Custom
JFW_Add_Max_Shield_Strength_On_Custom
	Custom (custom to listen for)
	Value (how much to add)
All of these scripts, when they recieve a custom will affect the health/max health/shield strength/max shield strength of the object with the script attached.

JFW_Set_Health_On_Custom_Sender
JFW_Add_Health_On_Custom_Sender
JFW_Set_Max_Health_On_Custom_Sender
JFW_Add_Max_Health_On_Custom_Sender
JFW_Set_Shield_Strength_On_Custom_Sender
JFW_Add_Shield_Strength_On_Custom_Sender
JFW_Set_Max_Shield_Strength_On_Custom_Sender
JFW_Add_Max_Shield_Strength_On_Custom_Sender
	Custom (custom to listen for)
	Value (how much to add)
All of these scripts, when they recieve a custom will affect the health/max health/shield strength/max shield strength of the sender of the message.
Use these for powerups.

JFW_Create_Destroy_Object_On_Custom_Death (creates an object on a custom then destroys it on another custom)
	Location (location to spawn)
	Facing (what direction to face)
	Preset (preset to create)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)
When the object with this script attached to it dies, the created object is destroyed)

JFW_Create_Destroy_Object_On_Custom_Death_Bone (creates an object on a custom then destroys it on another custom)
	Location (location to spawn)
	Facing (what direction to face)
	Preset (preset to create)
	CreateMessage (message for creation)
	DestroyMessage (message for destruction)
	BoneName (bone to spawn at)
When the object with this script attached to it dies, the created object is destroyed)

JFW_Custom_Destroy_All_Buildings (destroys all buildings of a specified team on custom, this will end the game)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI)

JFW_Random_Timer_Play_Cinematic (plays a cinematic on a random timer)
	TimeMin (minumum amount of how long the timer should run for)
	TimeMax (maximum amount of how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (repeat multiple times)
	Script_Name (script name to play)
	Location (location to play at)
	Facing (direction to face)

JFW_Flying_Infantry (calls Toggle_Fly_Mode on the thing the script is attached to on startup)

JFW_Flying_Infantry_Custom (calls Toggle_Fly_Mode the thing the script is attached to on custom)
	Message (message to listen for)

JFW_Flying_Infantry_Powerup (powerup that calls Toggle_Fly_Mode on the collector)

JFW_Flying_Infantry_Enter (calls Toggle_Fly_Mode on the thing that entered the zone)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Flying_Infantry_Exit (calls Toggle_Fly_Mode on the thing that exited the zone)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Toggle_Fly_Mode will make the infantry that it's called on fly if they are not flying and not fly if they are.
Controls for flying infantry are the same as for helicoptors (C and space being defaults)

JFW_Warp_On_Pickup_Team (like KAK_Warp_On_Pickup but team specific)
	Position (Location to warp player)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Base_Defence_No_VTOL
JFW_Base_Defence_VTOL_Only
JFW_Base_Defence_No_VTOL_Secondary
JFW_Base_Defence_VTOL_Only_Secondary
JFW_Base_Defence_Animated_No_VTOL
JFW_Base_Defence_Animated_VTOL_Only
JFW_Base_Defence_Animated_No_VTOL_Secondary
JFW_Base_Defence_Animated_VTOL_Only_Secondary
JFW_Building_Gun_No_VTOL
JFW_Building_Gun_Weapon_No_VTOL
JFW_Repair_Zone_No_VTOL
JFW_Building_Gun_No_VTOL_Secondary
JFW_Building_Gun_Weapon_No_VTOL_Secondary
JFW_Building_Gun_Animated_No_VTOL
JFW_Building_Gun_Animated_Weapon_No_VTOL
JFW_Building_Gun_Animated_No_VTOL_Secondary
JFW_Building_Gun_Animated_Weapon_No_VTOL_Secondary
JFW_Building_Gun_VTOL_Only
JFW_Building_Gun_Weapon_VTOL_Only
JFW_Repair_Zone_VTOL_Only
JFW_Building_Gun_VTOL_Only_Secondary
JFW_Building_Gun_Weapon_VTOL_Only_Secondary
JFW_Building_Gun_Animated_VTOL_Only
JFW_Building_Gun_Animated_Weapon_VTOL_Only
JFW_Building_Gun_Animated_VTOL_Only_Secondary
JFW_Building_Gun_Animated_Weapon_VTOL_Only_Secondary
These are exactly like the _Aircraft_Only and _No_Aircraft except instead of checking preset names, they specifically check the physics type of the object for VTOLPhysics

JFW_Refill_Buy (buys refill for the object that entered the zone)
	Cost (cost for the refill)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Note that the money will still be deducted even if they don't need refill.

JFW_Refill_Powerup (gives refill for the collector of the powerup)
This one would be great for Deathmatch maps where there are no PTs.

JFW_Zone_Refill (gives refill for the object that entered the zone)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
by "refill", I refer to the same thing as what happens when you press the "refill" button on the PCT.

JFW_Vehicle_Buy (buys a vehicle for a given player, it will be created at the weapons factory/airstrip)
	Preset_Name (vehicle preset to buy)
	Cost (cost)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, also specifies wich factory will be used)

JFW_Create_Vehicle_On_Custom (creates a vehicle at the WF/AS when sent a custom)
	Preset_Name (vehicle preset to create)
	Delay (some sort of delay value for creating the custom)
	Player_Type (which teams factory to use, 0 = Nod, 1 = GDI)

JFW_Zone_Vehicle (creates a vehicle for the thing that entered the zone, will use the WF/AS to create it)
	Preset_Name (vehicle preset to buy)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, also specifies wich factory will be used)
Note that if you spawn a Nod vehicle at the GDI factory, it will still be owned by Nod (and vice versa)

JFW_Powerup_Buy (buys a powerup for a given player)
	Preset_Name (powerup preset to buy)
	Cost (cost)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This won't work on a ScriptZoneAll (i.e. vehicles trigger it too) for granting weapons.
Granting health/armour/etc will work, as will granting ammo.

JFW_Timer_Custom_On_Off (starts a timer when sent a custom, sends a custom when the timer expires, stops the system when sent another custom)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (repeat the timer)
	ID (ID to send to)
	Message (message to send)
	Param (parameter to send)
	EnableMessage (message to enable the timer)
	DisableMessage (message to disable the timer)

JFW_Zone_Weapon (will give a weapon to the thing that enters the zone)
	Weapon_Name (the name of the weapon to grant, any valid powerup name should work)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This won't work on a ScriptZoneAll (i.e. vehicles trigger it too) for granting weapons.
Granting health/armour/etc will work, as will granting ammo.

JFW_Zone_Powerup (grants a powerup for the thing that entered the zone)
	Preset_Name (powerup preset to grant)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
This won't work on a ScriptZoneAll (i.e. vehicles trigger it too) for granting weapons.
Granting health/armour/etc will work, as will granting ammo.

JFW_Timer_Play_Random_Cinematic (like JFW_Timer_Play_Cinematic but chooses between one of 5 cinematics at random)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (repeat the cinematic)
	Script_Name1 (first cinematic)
	Script_Name2 (second cinematic)
	Script_Name3 (third cinematic)
	Script_Name4 (forth cinematic)
	Script_Name5 (fifth cinematic)
	Location (location to play)
	Facing (direction to face)

JFW_Random_Timer_Play_Random_Cinematic (like JFW_Random_Timer_Play_Cinematic but chooses between one of 5 cinematics at random)
	TimeMin (minumum amount of how long the timer should run for)
	TimeMax (maximum amount of how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (repeat the cinematic)
	Script_Name1 (first cinematic)
	Script_Name2 (second cinematic)
	Script_Name3 (third cinematic)
	Script_Name4 (forth cinematic)
	Script_Name5 (fifth cinematic)
	Location (location to play)
	Facing (direction to face)

JFW_Vehicle_Powerup (when this powerup is collected, a custom is sent to the vehicle the collector is driving)
	Message (message to send)

JFW_Vehicle_Powerup_Preset (when this powerup is collected, a custom is sent to the vehicle the collector is driving but only if it is a certain preset)
	Message (message to send)
	Preset (preset to check for)
These 2 can be used (in conjunction with various scripts on vehicles to respond to the custom) to do Vehicle Upgrades like in Generals.

JFW_Powerup_Send_Custom (Sends a custom when this powerup is collected)
	ID (id to send to)
	Message (message to send)

JFW_Powerup_Send_Custom_Team (sends a custom when this powerup is collected based on the team of the collector)
	GDIID (GDI id to send to)
	GDIMessage (GDI message to send)
	NodID (Nod ID to send to)
	NodMessage (Nod message to send)

JFW_Zone_Send_Custom_Team (sends a custom on zone entry/exit based on the team of the thing that entered/exited the zone)
	GDIID (GDI id to send to)
	GDIEnterMessage (GDI enter message to send)
	GDIEnterParam (GDI enter parameter to send)
	GDIExitMessage (GDI exit message to send)
	GDIExitParam (GDI exit parameter to send)
	NodID (Nod id to send to)
	NodEnterMessage (Nod enter message to send)
	NodEnterParam (Nod enter parameter to send)
	NodExitMessage (Nod exit message to send)
	NodExitParam (Nod exit parameter to send)

JFW_Regenerate_Health_Conditional (starts regenrating health on custom, stops on another custom)
	EnableMessage (enable message)
	DisableMessage (disable message)
	Amount (amount to heal per second)

JFW_Drone_Damage (will start damaging an object when that object is attacked by a particular preset)
	RepairStop (stop the drone damage on repair, 1 = yes, 0 = no)
	Preset (drone preset)
	Amount (amount to damage per second)
	Custom (this message will be sent back to the drone unit when the damage starts, use it to e.g. kill the drone unit)
This one could work for e.g. the RA2 Terror Drone
It will ensure that if the thing doing the damage is an infantry unit inside a vehicle, the vehicle is tested against, not the infantry

JFW_vehicle_Animation_2 (like JFW_vehicle_Animaton but with "up" and "down" animations)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Time (how long the timer should run for, 0.1 is a good value)
	TimerNum (what number to use for the timer)
	UpAnimation (animation to play when movement starts)
	UpSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	UpFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	UpLastFrame (frame to go to, -1 means "end of animation")
	UpBlended (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownAnimation (animation to play after movement stops)
	DownSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	DownFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownLastFrame (frame to go to, -1 means "end of animation")
	DownBlended (some value for blended animation, only applies to Solders)

JFW_Zone_Animation (plays an animation on zone entry then one on repeat then one on zone exit)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	UpAnimation (animation to play when zone enter is triggered)
	UpSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	UpFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	UpLastFrame (frame to go to, -1 means "end of animation")
	UpBlended (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownAnimation (animation to play when zone exit is triggered)
	DownSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	DownFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownLastFrame (frame to go to, -1 means "end of animation")
	DownBlended (some value for blended animation, only applies to Solders)

JFW_Group_Purchase_Zone (allows multiple people to contribute to a purchase)
	CashPerPerson (how much cash to take per person)
	TotalCash (total cash to take)
	ID (ID to send to)
	Custom (custom to send once enough cash has been collected)
if the same person enters the zone twice with enough cash, it will let them contribute twice

JFW_Zone_Timer (starts a timer when the right team enters the zone, if enough players of that team are in the zone on timer expired, sends a custom)
	Count (how many people need to be there)
	ID (ID to send to)
	Custom (custom to send)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Building_Damage (this will damage all buildings in a certain radius)
	Message (what message to listen for to do the damage)
	Player_Type (which teams buildings to damage, 0 = Nod, 1 = GDI, 2 = any)
	Damage (how much damage to do)
	Warhead (warhead to use)
	Distance (max distance to damage)

JFW_Building_Damage_Scale (this will damage all buildings in a certain radius, it uses a scaling factor to scale the damage based on how far away the building is)
	Message (what message to listen for to do the damage)
	Player_Type (which teams buildings to damage, 0 = Nod, 1 = GDI, 2 = any)
	Damage (how much damage to do)
	Warhead (warhead to use)
	Distance (max distance to damage)

JFW_Building_Damage_Percentage (this will damage all buildings in a certain radius, it does various percentage of damaged based on the buildings max health)
	Message (what message to listen for to do the damage)
	Player_Type (which teams buildings to damage, 0 = Nod, 1 = GDI, 2 = any)
	Percengage (what percentage of the buildings health to take off)
	Warhead (warhead to use)
	Distance (max distance to damage)
These 3 are intended to overcome the "explosions don't damage buildings" problem
The location used in the test is the location of the building controler.
The location used for the centerpoint is the location of the sender of the message.
Note that the parameter of the custom is what gets passed as the "damager" of the buildings.
This means that that object (which may be a player if JFW_Death_Send_Custom_ID is used) will recieve the points for the damage.

JFW_Weapon_Change_On_Custom (changes the weapon given to an object)
	Message (message to listen for)
	Powerup (powerup to grant for the weapon)
	Weapon (weapon to select)
What this script does is removes all current weapons held by whatever it's attached to.
It then gives a new powerup followed by a new weapon. This will work for vehicles as well as infantry.
This will remove all weapons (including pistol/C4)
The powerup and weapon need to match.
When doing this for vehicles (and perhaps infantry too in some cases), it is essential that both the primary and secondary ammo fields are filled in.
If you don't want secondary fire, set them both to the same ammo.
If you don't do this and someone activates secondary fire, renegade will crash.
Also, sound issues may arrise if the fire sound for the weapon you are using has a "loop" setting of anything other than 1.

JFW_Weapon_Change_On_Custom_Sender (changes the weapon given to an object)
	Message (message to listen for)
	Powerup
	Weapon
What this script does is removes all current weapons held by the sender of the custom.
It then gives a new powerup followed by a new weapon. This will work for vehicles as well as infantry.
This will remove all weapons (including pistol/C4)
The powerup and weapon need to match
This script can be used on a powerup
When doing this for vehicles (and perhaps infantry too in some cases), it is essential that both the primary and secondary ammo fields are filled in.
If you don't want secondary fire, set them both to the same ammo.
If you don't do this and someone activates secondary fire, renegade will crash.
Also, sound issues may arrise if the fire sound for the weapon you are using has a "loop" setting of anything other than 1.

JFW_Weapon_Change_On_Custom_Multiple (changes the weapon given to an object, this one gives multiple powerups)
	Message (message to listen for)
	Powerup1 (powerup to grant)
	Powerup2 (powerup to grant)
	Powerup3 (powerup to grant)
	Powerup4 (powerup to grant)
	Powerup5 (powerup to grant)
	Weapon (weapon to select)
What this script does is removes all current weapons held by whatever it's attached to.
It then gives new powerups followed by a new weapon.
This will remove all weapons (including pistol/C4)
The weapon needs to match one of the powerups.
Pass 0 for any powerups you don't want to use.
I recommend not using this for vehicles.

JFW_Weapon_Change_On_Custom_Multiple_Sender (changes the weapon given to an object, this one gives multiple powerups)
	Message (message to listen for)
	Powerup1 (powerup to grant)
	Powerup2 (powerup to grant)
	Powerup3 (powerup to grant)
	Powerup4 (powerup to grant)
	Powerup5 (powerup to grant)
	Weapon (weapon to select)
What this script does is removes all current weapons held by the sender of the custom.
It then gives new powerups followed by a new weapon.
This will remove all weapons (including pistol/C4)
The weapon needs to match one of the powerups.
Pass 0 for any powerups you don't want to use.
This script can be used on a powerup
I recommend not using this for vehicles.

JFW_Clear_Weapons_On_Custom (removes all weapons from the object this script is attached to)
	Message (message to listen for)
This script can be used on a powerup
This will work for vehicles too, rendering the vehicles weaponless.

JFW_Set_Model_On_Custom_Sender (when this script recieves a custom, it changes the model of the thing that sent the custom)
	Message (message to listen for)
	Model (model to change to)
This script can be used on a powerup

JFW_Send_Driver_Custom_On_Enter (this sends a custom to the driver of a vehicle when they enter and exit it)
	EnterMessage (message to send on vehicle enter)
	ExitMessage (message to send on vehicle exit)
If you want to implement something like the IFV in RA2, you can put JFW_Send_Driver_Custom_On_Enter on the vehicle.
Then you can put JFW_Set_Model_On_Custom_Sender and JFW_Weapon_Change_On_Custom_Sender on all infantry units to change 
the appearence and weapon of the vehicle.

these scripts:
JFW_Base_Defence_Animated_Sound
JFW_Base_Defence_Animated_Sound_No_Aircraft
JFW_Base_Defence_Animated_Sound_Aircraft_Only
JFW_Base_Defence_Animated_Sound_Secondary
JFW_Base_Defence_Animated_Sound_No_Aircraft_Secondary
JFW_Base_Defence_Animated_Sound_Aircraft_Only_Secondary>
JFW_Base_Defence_Animated_Sound_No_VTOL
JFW_Base_Defence_Animated_Sound_VTOL_Only
JFW_Base_Defence_Animated_Sound_No_VTOL_Secondary
JFW_Base_Defence_Animated_Sound_VTOL_Only_Secondary
are the same as the non sound ones except they take a 3d sound name to play at the location of the effect preset when it "animates up" (e.g. a motor sound for a samsite)
Sound (sound to play when they pop up)

these scripts:
JFW_Building_Gun_Animated_Sound_No_VTOL
JFW_Building_Gun_Animated_Sound_Weapon_No_VTOL
JFW_Building_Gun_Animated_Sound_No_VTOL_Secondary
JFW_Building_Gun_Animated_Sound_Weapon_No_VTOL_Secondary
JFW_Building_Gun_Animated_Sound_VTOL_Only
JFW_Building_Gun_Animated_Sound_Weapon_VTOL_Only
JFW_Building_Gun_Animated_Sound_VTOL_Only_Secondary
JFW_Building_Gun_Animated_Sound_Weapon_VTOL_Only_Secondary
JFW_Building_Gun_Animated_Sound
JFW_Building_Gun_Animated_Sound_Weapon
JFW_Building_Gun_Animated_Sound_No_Aircraft
JFW_Building_Gun_Animated_Sound_Aircraft_Only
JFW_Building_Gun_Animated_Sound_Weapon_No_Aircraft
JFW_Building_Gun_Animated_Sound_Weapon_Aircraft_Only
JFW_Building_Gun_Animated_Sound_Secondary
JFW_Building_Gun_Animated_Sound_Weapon_Secondary
JFW_Building_Gun_Animated_Sound_No_Aircraft_Secondary
JFW_Building_Gun_Animated_Sound_Aircraft_Only_Secondary
JFW_Building_Gun_Animated_Sound_Weapon_No_Aircraft_Secondary
JFW_Building_Gun_Animated_Sound_Weapon_Aircraft_Only_Secondary
are the same as the non sound ones except they take a 3d sound name to play at the location of the effect preset when it "charges up"
Sound (sound to play)

JFW_Death_Send_Custom_ID (when this object dies, it sends a custom with the ID of the object as the parameter)
	ID (what ID to send to)
	Message (what message to send)

JFW_Death_Send_Custom_Driver_ID (when this object dies, it sends a custom with the ID of the driver of the object as the parameter)
	ID (what ID to send to)
	Message (what message to send)

JFW_Blow_Up_On_Death_Driver (this script creates an explosion when whatever it's attached to dies, usefull for e.g. a Demo Truck, this one gives points from the explosion to the driver of the vehicle)
	Explosion (which explosion to create)

JFW_Random_DriverDeath (like RA_DriverDeath but randomly triggered)
	Percentage (what percentage change for death to happen)

JFW_Cinematic_Attack_Position (when this script is created, it will attack a specifc position, use in cinematics for example)
	Priority (priority for the action)
	Postion (the location to attack)
	Range (maximum effective range of the attack)
	Deviation (deviation of the attack)
	Primary (1 = use primary weapon,2 = don't use primary weapon)

JFW_Repair_On_Custom (when sent a custom, it repairs a certain amount of health/shield strength)
	Message (message to listen for)
	Health (health/shield strength to repair)

JFW_Conyard_Vehicle_Buildings (like JFW_Conyard but sends a custom to all vehicles with DecorationPhys pysics type (i.e. all nonmovable vehicles) so that they can repair also)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	DisableCustom (custom to send to disable the construction yard, e.g. on low power)
	Health (how much health to repair every tick)
	Message (what message to send)
Stick this on the construction yard building controler, fill in the parameters and the script does the rest.
No need to put anything on the other buildings or to input their IDs anywhere.

JFW_Gate_Zone_2 (this script is usefull for doing a Tiberian Sun style gate)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the gate should be open for)
	TimerNum (what number to use for the timer)
	ID (the ID of the gate object)
	Animation (the animation to play)

JFW_Animated_Effect (plays an animated effect then destroys itself)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Model (model to set to)
	Message (message to wait for)
	Location (location to spawn at)

JFW_Animated_Effect_2 (plays an animated effect then destroys itself,uses the location of the object with JFW_Animated_Effect_2 on it as the location)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Model (model to set to)
	Message (message to wait for)
	
JFW_Random_Animated_Effect (plays an animated effect then destroys itself)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Model (model to set to)
	Message (message to wait for)
	Location (location to spawn at)
	Offset (random offset to spawn at)
The object will spawn at a location no greater than offset.x from the specified location and no less than -offset.x
Same for y and z.

JFW_Random_Animated_Effect_2 (plays an animated effect then destroys itself. uses the location of the object with JFW_Animated_Effect_2 on it as the location)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Model (model to set to)
	Message (message to wait for)
	Offset (random offset to spawn at)
The object will spawn at a location no greater than offset.x from the specified location and no less than -offset.x
Same for y and z.

JFW_2D_Sound_Damage (when the building gets damaged below <damage> health, the sound is played and the sound is disabled. If the building goes above <damage> health, it will enable the sound again)
	Sound (sound to play)
	Damage (damage to watch for)

JFW_3D_Sound_Damage (when the building gets damaged below <damage> health, the sound is played and the sound is disabled. If the building goes above <damage> health, it will enable the sound again)
	Sound (sound to play)
	Damage (damage to watch for)
	Offset (offset from the location of the object with JFW_3D_Sound_Damage to play the sound at)

JFW_Character_Buy_Poke (like JFW_Character_Buy but triggers on poke)
	Preset_Name (character preset to buy)
	Cost (cost of the preset)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Refill_Buy_Poke (like JFW_Refill_Buy but triggers on poke)
	Cost (cost for the refill)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Note that the money will still be deducted even if they don't need refill.

JFW_Vehicle_Buy_Poke (like JFW_Vehicle_Buy but triggers on poke)
	Preset_Name (vehicle preset to buy)
	Cost (cost)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, also specifies wich factory will be used)

JFW_Powerup_Buy_Poke (like JFW_Powerup_Buy but triggers on poke)
	Preset_Name (powerup preset to buy)
	Cost (cost)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Weapon_Buy_Poke (like JFW_Weapon_Buy but triggers on poke)
	Weapon_Name (the name of the weapon to grant, any valid powerup name should work)
	Cost (the cost)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Preset_Buy_Poke (like JFW_Preset_Buy but triggers on poke)
	Preset_Name (the name of the preset to create)
	Cost (the cost)
	Location (the location to spawn it at)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
All of these should work with a 0 cost.

JFW_PCT (triggers a PCT)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = trigger GDI terminal for GDI players, Nod terminal for Nod players)
	
JFW_Group_Purchase_Poke (like JFW_Group_Purchase_Zone but you poke it)
	CashPerPerson (how much cash to take per person)
	TotalCash (total cash to take)
	ID (ID to send to)
	Custom (custom to send once enough cash has been collected)
if the same person enters the zone twice with enough cash, it will let them contribute twice

JFW_Gate_Poke (like JFW_Gate_Zone_2 but you poke it)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the gate should be open for)
	TimerNum (what number to use for the timer)
	ID (the ID of the gate object)
	Animation (the animation to play)

JFW_Set_Background_Music_On_Custom (will set the background music for all players on custom)
	Message (message to listen for)
	Music (music to play)

JFW_Fade_Background_Music_On_Custom (will fade the background music for all players on custom)
	Message (message to listen for)
	Music (music to play)
	unk1 (unknown value passed to Fade_Background_Music)
	unk2 (unknown value passed to Fade_Background_Music)

JFW_Stop_Background_Music_On_Custom (will stop the background music for all players on custom)
	Message (message to listen for)

JFW_Set_Background_Music_On_Enter (will set the background music for the object that entered the zone on enter)
	Music (music to play)

JFW_Fade_Background_Music_On_Enter (will fade the background music for the object that entered the zone on enter)
	Music (music to play)
	unk1 (unknown value passed to Fade_Background_Music)
	unk2 (unknown value passed to Fade_Background_Music)

JFW_Stop_Background_Music_On_Enter (will stop the background music for the object that entered the zone on enter)

JFW_Fog_Create (changes the fog on startup)
	Fog_Enable (enable fog, 1 = enable, 0 = disable)
	Fog_Start_Distance (distance to start drawing the fog at)
	Fog_End_Distance (distance to stop drawing the fog at)
	Delay (delay before drawing it)
	OnCreate (trigger on create)
	OnDestroy (trigger on destroy)

JFW_Fog_Custom (changes the fog on custom)
	Fog_Enable (enable fog, 1 = enable, 0 = disable)
	Fog_Start_Distance (distance to start drawing the fog at)
	Fog_End_Distance (distance to stop drawing the fog at)
	Delay (delay before drawing it)
	Message (mesasge to wait for)

JFW_Fog_Zone (changes the fog on entry)
	Fog_Enable (enable fog, 1 = enable, 0 = disable)
	Fog_Start_Distance (distance to start drawing the fog at)
	Fog_End_Distance (distance to stop drawing the fog at)
	Delay (delay before drawing it)
	OnEnter (trigger on enter)
	OnExit (trigger on exit)

JFW_War_Blitz_Create (changes the war blitz on create)
	War_Blitz_Intensity (how strong to make the war blitz)
	Start_Distance (how close to start the war blitz)
	End_Distance (how far away to end the war blitz)
	War_Blitz_Heading (what direction to make the war blitz)
	War_Blitz_Distribution (how often to draw the war blitz)
	Delay (delay before drawing it)
	OnCreate (trigger on create)
	OnDestroy (trigger on destroy)

JFW_War_Blitz_Custom (changes the war blitz on custom)
	War_Blitz_Intensity (how strong to make the war blitz)
	Start_Distance (how close to start the war blitz)
	End_Distance (how far away to end the war blitz)
	War_Blitz_Heading (what direction to make the war blitz)
	War_Blitz_Distribution (how often to draw the war blitz)
	Delay (delay before drawing it)
	Message (mesasge to wait for)

JFW_War_Blitz_Zone (changes the war blitz on entry)
	War_Blitz_Intensity (how strong to make the war blitz)
	Start_Distance (how close to start the war blitz)
	End_Distance (how far away to end the war blitz)
	War_Blitz_Heading (what direction to make the war blitz)
	War_Blitz_Distribution (how often to draw the war blitz)
	Delay (delay before drawing it)
	OnEnter (trigger on enter)
	OnExit (trigger on exit)

JFW_Lightning_Custom (changes the lightning on custom)
	Lightning_Intensity (how strong to make the lightning)
	Start_Distance (how close to start the lightning)
	End_Distance (how far away to end the lightning)
	Lightning_Heading (what direction to make the lightning)
	Lightning_Distribution (how often to draw the lightning)
	Delay (delay before drawing it)
	Message (mesasge to wait for)

JFW_Wind_Custom (changes the wind on custom)
	Wind_Heading (direction of the wind)
	Wind_Speed (speed of the wind)
	Wind_Variability (how often to blow)
	Delay (delay before drawing it)
	Message (mesasge to wait for)

JFW_Precipitation_Custom (changes the precipitation on custom)
	Type (type, can be "Rain", "Snow" or "Ash")
	Density (how much should be drawn)
	Delay (delay before drawing it)
	Reset_Precipitation (set to 1 to turn off all other forms of precipitation)
	Message (mesasge to wait for)

JFW_Clouds_Custom (changes the clouds on custom)
	Cloud_Cover (how many clouds to draw)
	Cloud_Gloominess (how dark to make them)
	Delay (delay before drawing it)
	Message (mesasge to wait for)
	
JFW_Custom_Grant_Key (will grant a key on custom)
	Message (message to listen for)
	Key (key to grant)
	
JFW_Screen_Fade_On_Custom (will fade the screen on custom)
	Message (message to listen for)
	Red (red value between 0 and 1)
	Green (green value between 0 and 1)
	Blue (blue value between 0 and 1)
	Opacity (opacity value)

JFW_Screen_Fade_On_Enter (will fade the screen on enter)
	Red (red value between 0 and 1)
	Green (green value between 0 and 1)
	Blue (blue value between 0 and 1)
	Opacity (opacity value)

JFW_Screen_Fade_On_Exit (will fade the screen on exit)
	Red (red value between 0 and 1)
	Green (green value between 0 and 1)
	Blue (blue value between 0 and 1)
	Opacity (opacity value)
This is the same logic RenAlert used for the underwater effect.
values of 0 for red, 0 for green and 1 for blue with 150 for opacity look good for underwater
and using 0 for red, 1 for green and 0 for blue would work for e.g. a vat of acid (perhaps in combination with a script to slowly kill you)

JFW_Enable_Stealth_On_Custom (enables stealth of the object with JFW_Enable_Stealth_On_Custom on it)
	Message (message to listen for)
	Enable (enable or disable, 1 = enable, 0 = disable)

JFW_Goto_Object_On_Startup (when this script is created, it will make an object go to another object)
	ID (object to go to)
	Speed (speed to go there)
	ArriveDistance (how close to go)

JFW_Enable_Team_Radar_On_Custom (will enable the radar for a team on custom)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (message to wait for)
	Enable (1 = enable, 0 = disable)
Note that if someone joins after Enable_Team_Radar_On_Custom triggers, their radar won't be disabled.
So use some kind of timer to make sure that JFW_Enable_Team_Radar_On_Custom triggers peroidically

JFW_Disable_Radar_Zone (will disable the radar of the object that enters the zone on enter and enable it on exit)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Poke_Send_Custom_2 (sends a custom when poked)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (ID to send to)
	Message (message to send)
	Param (parameter to send)

JFW_Poke_Send_Custom_Preset (sends a custom when poked if the player is a certain preset)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (ID to send to)
	Message (message to send)
	Param (parameter to send)
	Preset (preset name to check for)

JFW_Poke_Send_Custom_Keycard (sends a custom when poked if the player has a certain key)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (ID to send to)
	Message (message to send)
	Param (parameter to send)
	Key (key to check for)

JFW_Poke_Send_Custom_Cost (sends a custom when poked if the player has enough money)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (ID to send to)
	Message (message to send)
	Param (parameter to send)
	Cost (cost to send)

JFW_Poke_Play_2D_Sound (plays a 2D sound when poked)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_Poke_Play_3D_Sound (plays a 3D sound when poked)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)
	Position (location to play it at)

JFW_Console_Message_On_Custom (prints a mesasge on the server console on custom)
	Message (message to listen for)
	Output (string to output)
This would be usefull to output special messages to tell server owners whats going on.
(e.g. any special notes that server owners need to know when using your map)

JFW_BHS_DLL (prints a mesasge to tell the server that bhs.dll is required for this map)

JFW_Invulnerable_On_Custom_3 (makes an object invulnerable or turns it off when sent a custom)
	Message (message to wait for, send 1 for parameter to turn on, 0 for parameter to turn off)
	InvulnerableArmour (armour type to use when it's invulnerable)
	NormalArmour (armour type to restore it to when it stops being invulnerable)
This is a better version of JFW_Invulnerable_On_Custom that overcomes the limitation mentioned above.
Use Blamo for the invulnerable armour, this should protect against everything.
This changes the skin instead of the shield type.
The bad news is that it appears as though buildings ignore the Shield and Skin settings altogether.

JFW_Powerup_Buy_Poke_Timer (like JFW_Powerup_Buy_Poke but with a time limit before it can be used again)
	Preset_Name (powerup preset to buy)
	Cost (cost)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Vehicle_Model_Team (changes the model of a vehicle depending on the team of the last thing that entered the vehicle)
	Model (model to use)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Vehicle_Model_Preset (changes the model of a vehicle depending on the preset of the last thing that entered the vehicle)
	Model (model to use)
	Preset (preset to check for)

JFW_Vehicle_Weapon_Preset (changes the weapon of a vehicle depending on the preset of the last thing that entered the vehicle)
	Powerup (powerup to grant)
	Weapon (weapon to set, should match the powerup)
	Preset (preset to check for)
When doing this for vehicles (and perhaps infantry too in some cases), it is essential that both the primary and secondary ammo fields are filled in.
If you don't want secondary fire, set them both to the same ammo.
If you don't do this and someone activates secondary fire, renegade will crash.
Also, sound issues may arrise if the fire sound for the weapon you are using has a "loop" setting of anything other than 1.

JFW_Vehicle_Model_Weapon (changes the model of a vehicle depending on if the last thing that entered the vehicle has a given weapon)
	Model (model to use)
	CharWeapon (weapon to check for)

JFW_Vehicle_Weapon_Weapon (changes the weapon of a vehicle depending on if the last thing that entered the vehicle has a given weapon)
	Powerup (powerup to grant)
	Weapon (weapon to set, should match the powerup)
	CharWeapon (weapon to check for)
When doing this for vehicles (and perhaps infantry too in some cases), it is essential that both the primary and secondary ammo fields are filled in.
If you don't want secondary fire, set them both to the same ammo.
If you don't do this and someone activates secondary fire, renegade will crash.
Also, sound issues may arrise if the fire sound for the weapon you are using has a "loop" setting of anything other than 1.

Note that for these 2 scripts, if more than one copy is attached to a vehicle and the last thing that entered the vehicle has more than one matching weapon,
one of the scripts will be triggered but there is no way to know which one.
If more than one copy of any of these 5 scripts is attached with the same weapon, preset or team value to check, things won't work right.
Also, if you e.g. had JFW_Vehicle_Model_Team and JFW_Vehicle_Model_Preset attached and something matched both, things won't work right.

JFW_Console_Input_On_Custom (sends a string to the console input function on a custom)
	Message (message to listen for)
	Input (string to pass)
Use this to run a console command via scripting.
For example you can use the MESSAGE command to send a message to the players.

JFW_Team_DM_Controller_2 (clone of JFW_Team_DM_Controller but uses the engine.cpp stuff to kill the buildings)
	Deaths_To_Loose (how many deaths must happen before the game is over)
	Death_Message (the message that is sent to the controller to record a death for that team)
	Team (which teams buildings to kill)

JFW_Heavy_Vehicle_Damage_Zone (clone of JFW_vehicle_Damage_Zone but only damages vehicles heavier than a certain amount)
	Warhead (the warhead to use)
	Damage (how much damage to do per timer tick)
	Time (how much time to wait before doing the next bit of damage)
	Distance (how far away from the location of the zone the object has to be before the damage stops)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Mass (the mass to check for)

JFW_Light_Vehicle_Damage_Zone (clone of JFW_vehicle_Damage_Zone but only damages vehicles lighter than a certain amount)
	Warhead (the warhead to use)
	Damage (how much damage to do per timer tick)
	Time (how much time to wait before doing the next bit of damage)
	Distance (how far away from the location of the zone the object has to be before the damage stops)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Mass (the mass to check for)

JFW_Vehicle_Thief (stick this on a vehicle to make that vehicle stealable)
	Weapon (weapon to check for)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
When the vehicle with this is shot by the specified weapon and team, the people inside will be kicked out and then the thief can get inside.

JFW_2D_Sound_Damage_Range (when the health goes within the range, it will play a sound, when the health goes back outside it will reset so that it plays the sound again)
	Sound (sound to play)
	MinHealth (minumum health to look for)
	MaxHealth (maximum health to look for)

JFW_3D_Sound_Damage_Range (when the health goes within the range, it will play a sound, when the health goes back outside it will reset so that it plays the sound again)
	Sound (sound to play)
	MinHealth (minumum health to look for)
	MaxHealth (maximum health to look for)
	Offset (offset from the location of the object with to play the sound at)

JFW_Animation_Frame_Damage (when the health goes within the range it will set the animation frame to the specified frame)
	Animation (animaion to play)
	Frame (frame to go to)
	MinHealth (minumum health to look for)
	MaxHealth (maximum health to look for)
This can be used to create e.g. damage animations.

JFW_Damage_Do_Damage (when health goes below a certain amount, it will apply "damage" damage every timer tick, when it goes back over, it will stop)
	Health (health to look for)
	Damage (damage to do)
	Warhead (warhead to use)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
You can also use this to heal if you want.

JFW_Change_Team_On_Custom (when a custom is recieved, the team of the object with this on it is changed)
	Message (custom to listen for)

JFW_3D_Sound_Timer (play a 3d sound on a timer)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Repeat (repeat the sound, 1 = yes, 0 = no)
	Sound (sound to play)
	Offset (offset from the location of the object with this script to play the sound at)

JFW_2D_Sound_Zone (play a 2d sound when a player enters the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_3D_Sound_Team_Zone (play a 3d sound for the team of the player the entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_3D_Sound_Player_Zone (play a 3d sound for the player the entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_2D_Sound_Team_Zone (play a 2d sound for the team of the player the entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_2D_Sound_Player_Zone (play a 2d sound for the player the entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_2D_Sound_Team_Custom (play a 2d sound for a given team on custom)
	Message (message to listen for)
	Sound (sound to play)
	Team (team to play for, 0 = Nod, 1 = GDI, 2 = both)

JFW_3D_Sound_Team_Custom (play a 3d sound for a given team on custom)
	Message (message to listen for)
	Sound (sound to play)
	Position (the position to play the sound at)
	Team (team to play for, 0 = Nod, 1 = GDI, 2 = both)

JFW_Poke_Play_2D_Sound_Team (play a 2d sound for the team of the player that poked the object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_Poke_Play_3D_Sound_Team (play a 3d sound for the team of the player that poked the object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)
	Position (the position to play the sound at)

JFW_Poke_Play_2D_Sound_Player (play a 2d sound for the player that poked the object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)

JFW_Poke_Play_3D_Sound_Player (play a 3d sound for the player that poked the object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)
	Position (the position to play the sound at)

JFW_Remove_Script_Preset_Custom (remove a script from all objects of a given preset on custom)
	Script (script to remove)
	Message (message to listen for)
	Preset (preset to remove from)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Preset_Custom (add a script to all objects of a given preset on custom)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)
	Preset (preset to add to)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Remove_Script_Type_Custom (remove a script from all objects of a given type on custom)
	Script (script to remove)
	Message (message to listen for)
	Type (type to remove from)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Type_Custom (add a script to all objects of a given type on custom)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)
	Type (type to add to)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
For these last 2, here are the types you can use: (all of these match with the stuff you see in leveledit presets window)
Beacon 12310
Building 53249 (only covers generic buildings, not the special stuff like PowerPlant or Refinery)
C4 12294 (placed C4 that is on the ground at this point)
Power Plant 53251
Power Up 12291
Refinery 53250
Repair Bay 53257
SAM Site 12295 (only the special "sam site" preset counts for this)
Script Zone 12302
Simple 12292
Soldier 12289
Soldier Factory 53252
Special Effects 12307
Vehicle 12304
WarFactory 53255
Airstrip 53254
Cinematic 12305
ComCenter 53256

JFW_Remove_Script_Custom (removes a script from this object on custom)
	Script (script to remove)
	Message (message to listen for)

JFW_Attach_Script_Custom (attach a script to this object on custom)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)

JFW_Remove_All_Scripts_Custom (remove all scripts from this object on custom)
	Message (message to listen for)

JFW_vehicle_Animation_Trigger (this is like JFW_vehicle_Animation but it has 2 extra animations that trigger on 2 different customs)
	Animation (the animation to use e.g. ref_crusher.ref_crusher)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	UpAnimation (animation to play when first trigger is recieved)
	UpSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	UpFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	UpLastFrame (frame to go to, -1 means "end of animation")
	UpBlended (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownAnimation (animation to play second trigger is recieved)
	DownSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	DownFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownLastFrame (frame to go to, -1 means "end of animation")
	DownBlended (some value for blended animation, only applies to Solders)
	UpTrigger (first trigger to look for)
	DownTrigger (second trigger to look for)
This could be used (with the vehicle enter and exit customs) for e.g. landing gear on a flying vehicle.

JFW_Screen_Fade_Custom_Timer (when the custom is recieved, set the screen fade for all players to the specified settings then when the timer expires, go back to default)
	Message (message to listen for)
	Red (red value between 0 and 1)
	Green (green value between 0 and 1)
	Blue (blue value between 0 and 1)
	Opacity (opacity to use)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
This could be used e.g. for a nuclear explosion (make the screen white for a flash)

JFW_Stealth_Zone (when something that is the right player type and isn't one of the 4 ignore presets enters the zone, it becomes stealth. Then when it leaves, it unstealths)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Stealth1 (first stealth preset to ignore)
	Stealth2 (second stealth preset to ignore)
	Stealth3 (third stealth preset to ignore)
	Stealth4 (fourth stealth preset to ignore)

JFW_Look_At_Location_Entry (when entered, force the camera for the enterer to look at a particular location)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Location (location to look at)

JFW_Look_At_Location_Poke (when poked, force the camera for the poker to look at a particular location)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Location (location to look at)

JFW_Look_At_Object_Entry (when entered, force the camera for the enterer to look at a particular object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ObjectID (object to look at)

JFW_Look_At_Object_Poke (when poked, force the camera for the poker to look at a particular object)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ObjectID (object to look at)

JFW_Clear_Weapons_Create (when the object with this on it is created, all the weapons held by them will be removed)

JFW_Clear_Money_Create (when the object with this on it is created, all the credits held by them will be removed)

JFW_Change_Model_Health (when the health goes below a certain amount, it will change the model)
	Model (model to change to)
	Health (health to look for)
Shield Strength has no affect on this script

JFW_Change_Model_Health2 (when the health goes above a certain amount, it will change the model)
	Model (model to change to)
	Health (health to look for)
Shield Strength has no affect on this script

JFW_Domination_Controler_End_Game (this is the same as JFW_Domination_Controler except when one team has captured a certain number of zones, they will win the game)
	NeutralCustom (what custom to send to the controler if the zone was neutral before it was captured)
	OwnedCustom (what custom to send to the controler if the zone was already owned before it was captured)
	Time (how often to give points)
	TimerNum (timer number for the timer)
	PointsToGive (how many points to give to GDI for each GDI zone/Nod for each Nod zone)
	PointsToTake (how many points to take from Nod for each GDI zone/GDI for each Nod zone)
	GDIObjectID (the ID of an object somewhere that is gauranteed to remain around for the entire game and that is owned by GDI, this is needed to give points to GDI)
	NodObjectID (the ID of an object somewhere that is gauranteed to remain around for the entire game and that is owned by Nod, this is needed to give points to Nod)
	ZoneCount (how many zones must be controled before the game will end)

JFW_Change_Character_Powerup (when this powerup is collected, it changes the caracter of the collector)
	Preset (character preset to change to)
This won't trigger if the player buying is in a vehicle.

JFW_Preset_Buy_Poke_Timer (this is like JFW_Preset_Buy_Poke except that there is a time limit between purchases)
	Preset_Name (the name of the preset to create)
	Cost (the cost)
	Location (the location to spawn it at)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Preset_Buy_Poke_Custom (this is like JFW_Preset_Buy_Poke except that there is a custom that must be sent to enable another purchase)
	Preset_Name (the name of the preset to create)
	Cost (the cost)
	Location (the location to spawn it at)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (message to listen for)

Both of these should work with a 0 cost.

JFW_Slot_Machine (a script for a slot machine)
	Cost (cost to play)
	Winnings (winnings if you win)
	Percent (percent chance to win)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
When this object is poked, it generates a random number between 0 and 99
if that number is < the Prercent value, you recieve "winnings" credits.
Otherwise you loose "cost" credits.

JFW_Jetpack (script for a jetpack or anything else where you want a human that can fly e.g. wizzard with levetation)
	DisableMessage (message to disable the jetpack)
	EnableMessage (message to enable the jetpack)
When this script starts up, it installs a hook for the logical key "Jetpack".
This means that the client needs bhs.dll 2.1 installed and that there must be an entry in keys.cfg on the client for the "Jetpack" logical key.
When it recieves the DisableMessage, it stops allowing you to toggle flying
When it recieves the EnableMessage, it starts allowing you to toggle flying again
The use of the DisableMessage and EnableMessage is to go with a script like JFW_Send_Driver_Custom_On_Enter to turn off the flying switch function
when you get into a vehicle, since it doesn't work if enabled inside a vehicle.
Inside the keyboard hook, if it's enabled, it switches the fly mode for the object every time the key is pressed.

JFW_Jetpack_Model (script for a jetpack or anything else where you want a human that can fly e.g. wizzard with levetation)
	DisableMessage (message to disable the jetpack)
	EnableMessage (message to enable the jetpack)
	OnModel (model to use when in fly mode)
	OffModel (model to use when not in fly mode)
This script is like JFW_Jetpack except that it changes the model of the unit when in fly mode vs no fly mode

JFW_Give_Powerup_Create (when the object is created, a powerup is given)
	Powerup (powerup to give)

JFW_Invulnerable_On_Create
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	InvulnerableArmour
	NormalArmour

JFW_Destroy_Self_Timer (when created, a timer is started, when the timer expires, the object is destroyed)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Attach_Script_Preset_Startup (attaches a script to all objects of a given preset on startup)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Preset (preset to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Type_Startup (attaches a script to all objects of a given type on startup)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Type (type to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Building_Startup (attaches a script to all buildings on startup)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Preset_Once_Custom (like JFW_Attach_Script_Preset_Custom except that it only attaches the script if it's not already attached)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)
	Preset (preset to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Type_Once_Custom (like JFW_Attach_Script_Type_Custom except that it only attaches the script if it's not already attached)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)
	Type (type to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Once_Custom (like JFW_Attach_Script_Custom except that it only attaches the script if it's not already attached)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)

JFW_Attach_Script_Preset_Created (attaches a script to all objects of a given preset when they are created)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Preset (preset to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Type_Created (attaches a script to all objects of a given type when they are created)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Type (type to look for)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Player_Created (attaches a script to all players when they are created)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)

JFW_Attach_Script_Vehicle_Created (attaches a script to all objects vehicles when they are created)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Player_Type (what player type to attach to, 0 = Nod, 1 = GDI, 2 = any)
Note that all the _Created scripts won't trigger for objects that are placed on the map at startup, only for those created by the game
This includes the players, vehicles they buy, harvesters etc but not preplaced things like the building controlers.
Although there are a few instances where the Object Create Hook might not trip (e.g. certain spawner objects). This is because those objects don't
call through to ScriptableGameObj::Start_Observers (the place where the scripts get started up and the place I am hooking)

JFW_Dplbl_Vhcls_Keyboard (like Ra2Ven_Deployable_Vehicle except it triggers on keypress)
	Animation_Preset (preset to use for deploy animation, put Ra2Ven_Deploy_Animation on this)
	oldTnk_Warhead (Warhead used to destroy the deployable vehicle)
	oldTnk_Dammage (Damage to do to destroy the deployable vehicle)
	Explosion_preset (This is used as destroy animation)
When this script starts up, it sets the pilotid to 0
When something gets into the vehicle and the pilotid is still 0, it installs a keyboard hook for the "Deploy" logical key.
This means that the client needs bhs.dll 2.1 installed and that there must be an entry in keys.cfg on the client for the "Jetpack" logical key.
When the pilot leaves the vehicle, the hook is uninstalled and the pilotid is set back to 0
then, when the keyboard hook is triggered (i.e. the "deploy" key is pressed), everyone in the vehicle is kicked out.
then, it creates the animation object and destroys the vehicle.

Thanks to E! for doing a lot of the work on the stealth generator, mobile sensor array and caryall related logic
JFW_Stealthable_Object (script for objects that can be made stealth)
	Message (message to listen for)
When using this and sending a message with e.g. JFW_Send_Custom_Distance_Objects_Timer, use a time value of something like 2 (if you use too much, the vehicle will flicker between stealthed and unstealthed)

JFW_Carryable_Vehicle
	Only_Empty (only permit empty vehicles to be carried)
	Control_ID (ID of the object with JFW_Carry_Control on it)

JFW_CarryAll
	Bone_Name (bone to attach to)
	Cost (cost to return if someone buys one that goes over the caryall limit)
	Control_ID (ID of the object with JFW_Carry_Control on it)
To trigger this, you need to make the primary weapon with a range long enough to "shoot" the vehicle you want to pick up (make it do no damage)
Then you make the secondary weapon short enough to "shoot" the carryall itself (again with no damage) to drop off the vehicle (again, make it do no damage)

JFW_Carry_Control (controller for caryalls & vehicle carriers, put this on the map somewhere)
	Max_Carryalls (maximum number of carryalls to permit, up to 20 is possible)

JFW_Scope (sets the scope for this player)
	Scope (what scope to use, use -1 for no scope)
See bhs.txt for details of the scope logic

JFW_Switch_Door (when poked, plays an animation (e.g. open door) then when timer expires, plays the same animation backwards (e.g. close door))
	ID (what object to play the animation on)
	Animation (what animation to play)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Switch_Lock_Door (like JFW_Switch_Door but requires a key to open)
	ID (what object to play the animation on)
	Animation (what animation to play)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Key (what key is required to open this)

JFW_Toggle_Door (when poked, plays an animation. When poked again, plays the animation backwards)
	ID (what object to play the animation on)
	Animation (what animation to play)

JFW_Sensor_Array (script for a mobile sensor array to detect stealth units)
	Distance (what distance from the sensor array to sense)
	Indicator (what object to create for stealthed units)
	IndicatorZOffset (z offset to create the object at)
	Time (how often to "ping" the objects and display new indicators)
For Indicator, stick something like JFW_PlayAnimation_DestroyObject to make the object go away after it is displayed.
Note that the indicator for this will be visible by all players.
To detect underground units use something like JFW_Send_Custom_Distance_Objects_Timer to send a message which JFW_Underground_Logic will pick up on.

JFW_Send_Custom_Distance_Objects_Timer (sends a custom to all objects within a certain distance on a timer)
	Distance (distance from the generator location that objects will be stealthed)
	Message (message to send)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Send_Custom_Distance_Objects_Custom (sends a custom to all objects within a certain distance when sent a custom)
	Distance (distance from the generator location that objects will be stealthed)
	Message (message to send)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ListenMessage (what message to listen for)

JFW_Send_Custom_All_Objects_Timer (sends a custom to all objects on a timer)
	Message (message to send)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)

JFW_Send_Custom_All_Objects_Custom (sends a custom to all objects when sent a custom)
	Message (message to send)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ListenMessage (what message to listen for)

JFW_Underground_Logic
	UpZOffset (what to add to z when surfacing)
	downZOffset (what to subtract from z when digging), this should probobly match the UpZOffset
	DigEffectObj (object to create on the surface when digging)
	SurfaceEffectObj (object to create on the surface when surfacing)
	DisableMessage (message to disable the dig and surface, e.g. for pavement)
	EnableMessage (message to enable the dig and surface)
	IndicatorMessage (message to display an indicator e.g. for mobile sensor array)
	IndicatorObject (what object to create for underground units)
	IndicatorZOffset (z offset to create the object at, this gets added to the z position of the underground vehicle)
	DigRed (red value for "screen color" effect, between 0 and 1)
	DigGreen (green value for "screen color" effect, between 0 and 1)
	DigBlue (blue value for "screen color" effect, between 0 and 1)
	DigOpacity (opacity value for "screen color" effect, between 0 and 1)
	DigSound (sound to play when digging or surfacing)
Stick this script on the vehicle that you want to be able to go underground.
For Indicator, stick something like JFW_PlayAnimation_DestroyObject to make the object go away after it is displayed
Note that the indicator for this will be visible by all players.
Do the same for DigEffectObj and SurfaceEffectObj. (also visible by all players)
If a player gets out of the vehicle underground, they will be killed
To set things up, you need a copy of your terrain <some distance> below the normal terrain which is where the underground
vehicles drive. I suggest putting signs/lights/indicators on this terrain so you know where exactly it is you are.
then, when the keyboard hook is triggered (i.e. the "dig" key is pressed), the object will go underground or surface.
Only the pilot of the vehicle can control dig and surface.
It will subtract or add (as appropriate) DigZOffset. And it will play the appropriate animation (if you don't want one,
put 0 in the box) at the surface location of the object.
Whilst underground, everyone in the vehicle will have the screen effect (like the one on the subs in RenAlert)
The indicator will be displayed if the object is underground and it gets the message (sent e.g. by the Mobile Sensor Array stuff)
The color and opacity will be applied to the screen of any player who is insid the vehicles when it is underground.
For pavement, set up a zone on the surface and one underground (I
don't know if you could just have one but stretch it really big or not) with something like TDA_Send_Custom_Zone on it to send the DisableMessage when
entered and the EnableMessage when exited. You can also do the same thing underground if there are areas where you don't want the player to dig up through
(e.g. underneath a hill or cliff) or you can just block off underground with some walls to achieve the same effect.
Despite the name, this is usable for Submarines and the like too.

JFW_Damage_Animation (script for a damage animation effect)
	Animation (animation to use)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
On startup, this sets a flag to false.
Then, when the thing with this script attached to it is damaged, a timer is started.
And if the flag is set to false, the flag is set to true and the animation is played forwards.
If the object is damaged again before the timer goes off, the timer is reset (but the animation is not played again)
When the timer expires, the flag is set to false and the animation is played backwards.

JFW_Zone_Send_Custom_Multiple (script to send a custom when multiple people are in a zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (The message to send)
	ID (The ID of the object to send to)
Note that this script wnt recognize people who die in the zone (if someone dies in the zone, the zone will still think there is someone inside and will
even let the person who died back into the zone (this is an engine limitation).
Also, if enough people are in the zone, then someone leaves and enters again, it will trigger again.
So, if you want the event to only happen once (i.e. the first time enough people are in the zone), you need to e.g. delete the zone when it fires or something.

JFW_Destroy_Create_Object_On_Custom (when sent a custom, destroys an object and creates another one at the same location)
	Message (what message to listen for)
	ID (the ID of the object to destroy)
	Preset (the preset name of the object to create)

JFW_Zone_Send_Custom_Enter (when entered, sends a custom to the thing that entered it)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (The message to send)

JFW_Zone_Send_Custom_Exit (when exited, sends a custom to the thing that exited it)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (The message to send)

JFW_Spawn_Object_Death_Weapon (This script will create an object upon death of a unit if the killer used a particular weapon, does not work on player spawners)
	Drop_Object (Preset STRING Name) example POW_Flamethrower_Player
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	Weapon (which weapon to look for)
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died

JFW_Send_Message_Preset (This script will send a message to all instances of a preset on creation then another one on destruction)
	Preset (preset to send to)
	CreateMessage (message to send on creation)
	DestroyMessage (message to send on destruction)

JFW_Object_Counter (when sent one message, increments a count, when sent another, decrements a count. If the new count is higher than <number>, the object that sent the message is destroyed)
	Count (count to check for)
	IncrementMessage (message to use for increment)
	DecrementMessage (message to use for decrement)
Use this with something like JFW_Send_Message_Preset to implement limits for certain objects (e.g. visceroid limit)

JFW_Vehicle_Zone_Animation (plays an animation on zone entry then one on repeat then one on zone exit, only triggers for vehicles)
	Animation (the animation to use)
	Subobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	FirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	LastFrame (frame to go to, -1 means "end of animation")
	Blended (some value for blended animation, only applies to Solders)
	UpAnimation (animation to play when zone enter is triggered)
	UpSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	UpFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	UpLastFrame (frame to go to, -1 means "end of animation")
	UpBlended (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownAnimation (animation to play when zone exit is triggered)
	DownSubobject (the subobject to use for this animation, a value of 0 will mean "no subobject")
	DownFirstFrame (frame to start animating at, 0 means first frame, -1 means "current frame")
	DownLastFrame (frame to go to, -1 means "end of animation")
	DownBlended (some value for blended animation, only applies to Solders)
There is code to make sure that only the first vehicle to enter triggers the "up" animation and only the last vehicle to exit triggers the "down" animation
Use this for e.g. repair bay arms.

JFW_Zone_Money_Preset (when an object of a given preset/team enters the zone, an amount of money is given to the team of the object represented by ID)
	Preset (the preset to look for)
	Money (how much money to give)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (the ID of the object to give the money to)
This script contains some code that should prevent the money from being given multiple times per entry. For this to work, there should only be one object of the given preset in the zone at once.

JFW_Spawn_Object_Death_Team (This script will create an object upon death of a unit, does not work on player spawners. The object created depends on the team of the object that did the killing)
	Drop_Object_GDI (GDI drop object)
	Drop_Object_Nod (Nod drop object)
	Drop_Height (Z "modifier") example 2.0 (This is to adjust the height for the dropped object, usually only used for vehicles). 
	A good default for weapons, keycards etc is 0.25 (the figure is in meters)
	Note that the spawned object will be facing the same way as the object that died

JFW_Attach_Script_Collector (attach a script to the collector of the powerup with this on it when it's collected)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)

JFW_Attach_Script_Sender (attach a script to the sender of the custom on custom)
	Script (script to attach)
	Params (parameters to use)
	Delim (delimiter, anytime you see this in the "params" string it will be replaced with a comma)
	Message (message to listen for)

JFW_Toggle_Lock_Door (when poked, plays an animation. When poked again, plays the animation backwards. Only activates if the player has the given key)
	ID (what object to play the animation on)
	Animation (what animation to play)
	Key (what key is required to open this)

JFW_Change_Spawn_Character (changes what characters players spawn as)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Character (the character to change to)
The character name to change to must not be longer than 24 characters (because thats the length of the memory area used to store the character name)

JFW_Slot_Machine_2 (a script for a slot machine with an animation and sound)
	Cost (cost to play)
	Winnings (winnings if you win)
	Percent (percent chance to win)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Animation (what animation to play when the slot machine is poked)
	Sound (what sound to play when the slot machine is poked)
When this object is poked, it generates a random number between 0 and 99
if that number is < the Prercent value, you recieve "winnings" credits.
Otherwise you loose "cost" credits.

JFW_HUD (sets the hud main texture for this player)
	HUD (what texture to use in place of HUD_MAIN.TGA for this character, include .tga on the end even if its a dds file
See bhs.txt for details of this logic

JFW_Vehicle_Thief_2 (stick this on a vehicle to make that vehicle stealable)
	Weapon (weapon to check for)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (what 3D sound to play when the thief kicks players out)
When the vehicle with this is shot by the specified weapon and team, the people inside will be kicked out and then the thief can get inside.

JFW_Vehicle_Block_Preset (script to block a preset from entering a vehicle)
	Preset (what preset to block)
When the specified preset enters the vehicle, a timer is started (because the "get in vehicle" notification happens a tiny bit before the actual entry) then the thing that entered is kicked out.

JFW_PPAGE_Zone (sends a PPAGE to the player that entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

JFW_MSG_Zone (sends a MSG when a player enters the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

JFW_TMSG_Zone (sends a TMSG as though it came from the player that entered the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

JFW_Base_Defence_Swap
JFW_Base_Defence_No_Aircraft_Swap
JFW_Base_Defence_Aircraft_Only_Swap
JFW_Base_Defence_Animated_Swap
JFW_Base_Defence_Animated_No_Aircraft_Swap
JFW_Base_Defence_Animated_Aircraft_Only_Swap
JFW_Base_Defence_No_VTOL_Swap
JFW_Base_Defence_VTOL_Only_Swap
JFW_Base_Defence_Animated_No_VTOL_Swap
JFW_Base_Defence_Animated_VTOL_Only_Swap
JFW_Base_Defence_Animated_Sound_Swap
JFW_Base_Defence_Animated_Sound_No_Aircraft_Swap
JFW_Base_Defence_Animated_Sound_Aircraft_Only_Swap
JFW_Base_Defence_Animated_Sound_No_VTOL_Swap
JFW_Base_Defence_Animated_Sound_VTOL_Only_Swap
These are just like the non-swap versions but they take one extra parameter:
SwapMessage (message to listen for)
When they recieve the SwapMessage, they will switch from primary to secondary or vice versa.

JFW_Pilot_Repair (script to repair any vehicles the infantry with this on gets into)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Health (how much health to heal every time the timer expires)

JFW_Conyard_Radius (script for a construction yard that repairs a limited radius)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	DisableCustom (custom to send to disable the construction yard, e.g. on low power)
	Health (how much health to repair every tick)
	Radius (what radius to repair in)
Stick this on the construction yard building controller, fill in the parameters and the script does the rest.
No need to put anything on the other buildings or to input their IDs anywhere.
If a building controler is in the radius of 2 or more of these scripts, it gets repaired multiple times.

JFW_Conyard_Turrets (script to repair all vehicles with type "turret")
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	DisableCustom (custom to send to disable the construction yard, e.g. on low power)
	Health (how much health to repair every tick)
Stick this on the construction yard building controller, fill in the parameters and the script does the rest.
No need to put anything on the vehicles or to input their IDs anywhere.
This is specifically designed to repair tiberium silos, gun turrets, guard towers and such. (i.e. any vehicle with
"turret" as the mode)

JFW_Chrono_Harvester_Logic
JFW_Chrono_Harvester_Attached
JFW_Chrono_Harvester_Kill
JFW_Chrono_Harvester_Spawn_Kill
These scripts are exactly the same as KAK_Harvester_Logic and friends except that before following the 
Field2Dock waypath, they are moved to the Chrono_Location
	Chrono_Location (location to chrono to)

JFW_Suicide_Bomber (script to make someone who can blow themselves up)
	Explosion (explosion to use when blowing up)
When the BlowUp logical key is pressed, the specified explosion is triggered plus the person with JFW_Suicide_Bomber on them is killed.

JFW_Show_Info_Texture (script to show a texture on the client)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Texture (what texture to display)
When this script is created, the texture is displayed for the player whos object the script is attached to.
Then, when the timer expires, the texture is hidden again and the script is destroyed.

JFW_Switch_Door_Team (when poked, plays an animation (e.g. open door) then when timer expires, plays the same animation backwards (e.g. close door))
	ID (what object to play the animation on)
	Animation (what animation to play)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Player_Type (the player type to trigger on, 0 = Nod, 1 = GDI)

JFW_Toggle_Door_Team (when poked, plays an animation. When poked again, plays the animation backwards)
	ID (what object to play the animation on)
	Animation (what animation to play)
	Player_Type (the player type to trigger on, 0 = Nod, 1 = GDI)
These 2 are team specific

JFW_Damage_Occupants_Death (when the vehicle with this script on it is killed, all occupants are damaged)
	Damage (how much damage to do)
	Warhead (the warhead to use)

JFW_HUD_INI (Loads a new hud ini at runtime for the player it is attached to)
	HUDINI (what ini file to load)
see bhs.txt for more details of the hud ini code

JFW_Wireframe_Mode (changes the wireframe mode untill its set to something else by another call to the relavent script/engine call)
	Mode (what mode to use, 1 = point, 2 = wireframe, 3 = solid)
Also, note that this call changes ALL the graphics into wireframe including the HUD, text strings, dialog boxes (e.g. PTs), menus etc.

JFW_PT_Disable (script to disable a PT object on startup)
	Player_Type (the player type to trigger on, 0 = Nod, 1 = GDI)
	Type (type to disable)
	Pos (position to disable)

JFW_PT_Disable_Death (script to disable a PT object when the thing this is attached to dies)
	Player_Type (the player type to trigger on, 0 = Nod, 1 = GDI)
	Type (type to disable)
	Pos (position to disable)

The values for type are:
0 = characters
1 = vehicles
2 = equipment
3 = secret characters
4 = secret vehicles
5 = enlisted characters
6 = beacon
For types 0,1,2,3,4, pos corresponds to the position in the Purchase Settings in leveledit, e.g. 0 for pos means "entry 1" as labeled in leveledit.
For type 5, pos corresponds to the position in the Team Purcase Settings in leveledit, e.g. 0 for pos means "Enlisted 1" as labeled in leveledit.
For type 6, pos has no effect.
The hud ini script, the wireframe mode script and the PT scripts all require bhs.dll on the client to work

JFW_Character_Buy_Poke_Sound (like JFW_Character_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Refill_Buy_Poke_Sound (like JFW_Refill_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Vehicle_Buy_Poke_Sound (like JFW_Vehicle_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Powerup_Buy_Poke_Sound (like JFW_Powerup_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Preset_Buy_Poke_Sound (like JFW_Preset_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Group_Purchase_Poke_Sound (like JFW_Group_Purchase_Poke but plays a sound if the poker does not have enough cash)
JFW_Powerup_Buy_Poke_Timer_Sound (like JFW_Powerup_Buy_Poke_Timer but plays a sound if the poker does not have enough cash)
JFW_Weapon_Buy_Poke_Sound (like JFW_Weapon_Buy_Poke but plays a sound if the poker does not have enough cash)
JFW_Preset_Buy_Poke_Timer_Sound (this is like JFW_Preset_Buy_Poke_Timer but plays a sound if the poker does not have enough cash)
JFW_Preset_Buy_Poke_Custom_Sound (this is like JFW_Preset_Buy_Poke_Custom but plays a sound if the poker does not have enough cash)
	Sound (what sound to play)

JFW_Switch_Door_2 (like JFW_Switch_Door but works on the object that its attached to)
JFW_Switch_Lock_Door_2 (like JFW_Switch_Lock_Door but works on the object that its attached to)
JFW_Toggle_Door_2 (like JFW_Toggle_Door but works on the object that its attached to)
JFW_Toggle_Lock_Door_2 (like JFW_Toggle_Lock_Door but works on the object that its attached to)
JFW_Switch_Door_Team_2 (like JFW_Switch_Door_Team but works on the object that its attached to)
JFW_Toggle_Door_Team_2 (like JFW_Toggle_Door_Team but works on the object that its attached to)
For all of these, the ID parameter does not exist

JFW_Lock_Sound (when poked, if the poker has the specified key, one sound is played, otherwise another sound is played)
	Key (what key to check)
	LockSound (sound to play if the poker doesnt have the key)
	UnlockSound (sound to play if the poker has the key)

JFW_Vehicle_Extra (script to create an extra effect at the specified bone of an object)
	Extra_Preset (preset to create)
	Bone_Name (bone to attach to)
When the main object is destroyed, the preset is destroyed too.

JFW_Vehicle_Extra_2 (script to create an extra vehicle object e.g. a turret at the specified bone of a vehicle)
	Extra_Preset (preset to create)
	Bone_Name (bone to attach to)

JFW_Vehicle_Extra_Attach (script that is attached to the extra vehicle object)
	Main_Object (ID of the main object)
To use these scripts, create the attach vehicle (e.g. extra turret) so that it has the same health, shield strength, max health, max shield strength, skin and shield type as the main vehicle.
Any damage done to the main vehicle is transfered to the attach object and vice versa. You can have more than one attach object if you want, with multiple copies of JFW_Vehicle_Extra_2.
Also, the player type of the attached object is set such that only players of the team that is in the main object can get into the attached object (so if its neutral, anyone can get into the attach
object but if someone gets into the main object, anyone not on their team will be kicked out of the attach object)

JFW_Advanced_Guard_Tower_2 (clone of M00_Advanced_Guard_Tower that lets you reposition the missile and guns)
	Gun1Offset (offset of the first gun relative to the location of the building controler)
	Gun2Offset (offset of the second gun relative to the location of the building controler)
	Gun3Offset (offset of the third gun relative to the location of the building controler)
	Gun4Offset (offset of the forth gun relative to the location of the building controler)
	MissileOffset (offset of the missile relative to the location of the building controler)

Unlike JFW_Advanced_Guard_Tower, this script uses M00_Advanded_Guard_Tower_Missile and M00_Advanced_Guard_Tower_Gun.

JFW_Change_Radar_Map (script to change the custom radar map for the scrolling radar map feature)
	Scale (the scale of the radar map)
	OffsetX (the x offset of the center of the radar map)
	OffsetY (the y offset of the center of the radar map)
	Texture (the texture for the radar map)
see bhs.txt for details of the scrolling radar map feature.

JFW_Poke_Send_Custom_Toggle (like JFW_Poke_Send_Custom_2 but alternates between 2 customs)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	ID (ID to send to)
	Message1 (first message to send)
	Message2 (second message to send)

JFW_Teleport_Zone_Enable (like JFW_Teleport_Zone_Team but is turned on and off by customs)
	Location (Vector location of where to be sent)
	Object_ID (Object to teleport to)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	EnableCustom (what custom enables the teleport zone)
	DisableCustom (what custom disabls the teleport zone)

JFW_Poke_Play_2D_Sound_Timer (plays a 2D sound when poked but limits how often it will play)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
When poked, the trimer has to expire before the sound will play again

JFW_Poke_Play_3D_Sound_Timer (plays a 3D sound when poked but limits how often it will play)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (sound to play)
	Position (location to play it at)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
When poked, the trimer has to expire before the sound will play again

JFW_Reticle (sets the reticle textures for this player)
	ReticleTexture1 (what texture to use in place of hd_reticle.tga for this character, include .tga on the end even if its a dds file
	ReticleTexture2 (what texture to use in place of hd_reticle_hit.tga for this character, include .tga on the end even if its a dds file
See bhs.txt for details of this logic

JFW_Engineer_Target_2 (clone of JFW_Engineer_Target)

JFW_Engineer_Repair_2 (clone of JFW_Engineer_Repair)
	Repair_Priority (priority for repairing)
	Target_ID (the actual ID of the object that will get repaired)
This is intended so you can stick e.g. a Daves Arrow such that its "inside" the MCT and the engineer will repair that (and hit the MCT since the MCT is in the way)

JFW_Goto_Player_Timer (script to make an object go to the nearest player object everytime a timer expires)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Speed (speed to go there)
	ArriveDistance (how close to go)

JFW_Sidebar_PT (triggers the sidebar when poked)
	Player_Type (which team type to trigger on, 0 = Nod, 1 = GDI, 2 = trigger GDI sidebar for GDI players, Nod sidebar for Nod players)
This will display the "You do not have the required security to access this terminal" dialog if the player type doesnt match.

JFW_Radar_Spy_Zone (script to cover putting a spy in the enemy radar dome/com center and having the radar come back on if yours is gone)
	Spy_Script (name of the script to look for on the spy preset)
	Sound (what 2d sound to play for the team having their radar re-enabled)
When this is entered by an object of the right preset and the communications center of the team of that object is dead and the radar of the enemy is active,
radar is granted for the team of the object that entered the zone

JFW_Reflect_Custom_Delay (this script will send any message it recieves back to whatever object sent it with a delay)
	Delay (how long to delay for)

JFW_Radar_Jammer (script to disable the radar when a "jammer" unit is close to the radar dome)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	DisableCustom (custom to listen for to disable the jamming, e.g. if the power plant is dead and radar goes down)
	Preset (what preset to look for as the radar jammer)
	CenterID (the ID of an object, say, a daves arrow or something that is used as the centerpoint of the circle to check for the unit inside)
	Range (the radius of the circle to check for the unit inside)
	StartSound1 (sound to play for the team that owns the jammer when jamming starts)
	StopSound1 (sound to play for the team that owns the jammer when jamming stops)
	StartSound2 (sound to play for the team being jammed when jamming starts)
	StopSound2 (sound to play for the team being jammed when jamming stops)

This script goes on the radar building.
When the building is killed or the disable custom is sent, it stops working.
Every time the timer is triggered, if the specified preset is inside <range> of <centerid objects location>, the radar is turned off otherwise its turned on.

JFW_2D_Sound_Startup (script to play a 2D sound on startup)
	Sound (the sound to play)
Use this to e.g. play a global announcement when a certain vehicle/infantry is bought or e.g. when
a C4/beacon object is placed.

JFW_Cinematic_Attack_Command (clone of M00_Cinematic_Attack_Command_DLS)
	AttackDuration (how long the attack should last for)

JFW_Cinematic (clone of Test_Cinematic)
	ControlFilename (cinematic text script to use)

JFW_Cinematic_Kill_Object (clone of M00_Cinematic_Kill_Object_DAY)
	no parameters

JFW_Disable_Loiter (clone of M00_Disable_Loiter_DAY)
	no parameters

JFW_InnateIsStationary (clone of M00_InnateIsStationary)
	no parameters

JFW_Generic_Conv (clone of M00_Generic_Conv_DME)
	ConvName (name of the conversation)

JFW_Disable_Hibernation (clone of M07_Disable_Hibernation)
	no parameters

JFW_Sidebar_Key_2 (script to display the sidebar when a key is pressed)
	Key (what key to trigger on)
	Enable_Custom (custom to send to enable the key)
	Disable_Custom (custom to send to disable the key)
	Sound (sound that is played as a player-only 2D sound when the key is enabled)
This script starts out disabled.

JFW_2D_Sound_Zone_Team (plays a 2D sound for a team after a timer has expired)
	Time (how long the timer should run for)
	TimerNum (what number to use for the timer)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Sound (what sound to play)
	Sound_Player_Type (what team to play the sound for, 0 = Nod, 1 = GDI, 2 = any)

JFW_Sonar_Pulse (script for a sonar pulse)
	Spy_Script (name of the script to look for on the spy preset)
	Sub_Preset_1 (preset for the sub)
	Sub_Preset_2 (preset for the sub)
	Indicator (preset to display above the sub to give an indication of the depth)
	ZAdjust (the indicator will be displayed at this Z distance from the sub)
	WaterIndicator (preset to display at water level)
	WaterZ (Z value for the water plane)
	Sound (sound to play when the effect is triggered, everyone will hear this)
	Time (how long to wait before triggering the effect)
	ResetTime (how long to wait after the effect has been triggered before it can be triggered again)
Due to how this script works, all players can see the created objects (hence the delay to give the spy time to get out)
Note that the objects are not deleted by this script, they should delete themselves (with another script or otherwise)

JFW_Global_Stealth_Controller (controller script for all the stealth generators in the level)
	Update_Delay (how often to update)
	Timer_Number (timer number to use)

You need to put this on a certain preset (say, a clone of daves arrow) which will be used later in the other scripts.

JFW_Stealth_Generator_Building (script for a stealth generator)
	Timer_Number (timer number to use)
	Stealth_Range (the range of this generator in meters)
	Gap_Controller (preset name of the object with JFW_Global_Stealth_Controler on it)
	Team (Team this generator works for. 0 = Nod, 1 = GDI)	
If the object with this on it dies or looses power, the generator stops working.

JFW_Stealth_Generator_Vehicle (script for a mobile stealth generator)
	Stealth_Range (the range of this generator in meters)
	Gap_Controller (preset name of the object with JFW_Global_Stealth_Controler on it)
An unoccupied vehicle does not cloak anyone, occupied vehicles will cloak for the team of the driver.

JFW_Stealth_Generator_Ignored (script for units that stealth generators should ignore (such as stealth tanks)
	no parameters.

This script doesn't do anything on its own. The function controlling stealth generator effects is designed to ignore any object with this script attached to it. This script should be used for vehicles that are stealth on their own, such as stealth tanks, so that their stealth ability is not incorrectly removed by stealth generators. 

JFW_Time_Remaining_Sounds (script to play 2D sounds to indicate time remaining in the game)
	ConfigFile (which config file to use to specify the sounds to play)
The config file is a normal ini file containing a [Sounds] section and the following keywords:
SoundCount=n for how many sounds there are in the file
SoundxName for the name of sound x (e.g. Sound0Name for the first sound)
SoundxTime for the time remaining in minutes to play sound x at (e.g. Sound0Time for the first sound)

JFW_Vehicle_Lock (script to manage vehicle locking including theft by spies)
	Message (the message to listen for)
	Spy_Script (name of the script to look for on the spy preset)
	Time (time to lock for)
If the time is set to 0, it will remain locked untill unlocked by the owner.
Also, when its locked, its player type is set to the type of the owner.
If the owner object doesnt exist, the vehicle is automatically unlocked (for example, if the owner has left the game or died or whatever)
When the message is sent, if the parameter to it is zero, the owner is whoever is driving the vehicle at the time.
If its non zero, the parameter is used as the ID of the owner player object.
Note that you need VehicleOwnershipDisable=true in hud.ini for this feature to work
Use the value 1000000036 for the message if listening for a message from the ExpVehFac naval yard/helipad scripts or from the
vehicle ownership send bhs.dll feature.

JFW_Repair_Zone_2 (script for a repair zone that costs money)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Repair_Distance (how far away from the "position" of the zone the vehicle can move before repairing stops)
	Repair_Health (how much health to add per timer)
	Sound (sound to play to say "you can repair here")
	Credits (how many credits to deduct)
	RepairSound (sound to play when repairing starts)
	RepairStopSound (sound to play when repairing stops)
When entered by a vehicle, the vehicle/driver are recorded.
Then when the driver presses the "Repair" logical key, repairing starts.
Only one vehicle can use the repair zone at any one time. Repairing stops when the vehicle is full or when it leaves the repair radius.

JFW_Infantry_Force_Composition_Zone (script to display enemy infantry composition for the spy)
	Spy_Script (name of the script to look for on the spy preset)
	Player_Type (player type of the team who is to see the force composition information, 0 = Nod, 1 = GDI)
	Sound (sound to play when it triggers)
This script uses the PT data to identify which infantry to count.
The results are displayed in the same box as the CMSG console command and the f2/f3 chat messages.

JFW_Vehicle_Force_Composition_Zone (script to display enemy vehicle composition for the spy)
	Spy_Script (name of the script to look for on the spy preset)
	Player_Type (player type of the team who is to see the force composition information, 0 = Nod, 1 = GDI)
	Sound (sound to play when it triggers)
This script uses the PT data to identify which vehicles to count.
The results are displayed in the same box as the CMSG console command and the f2/f3 chat messages.

For more information on shader customs, look up Set_Shader_Number and post process shaders in bhs.txt.
JFW_Shader_Custom_Zone_Entry (script to send a shader custom on zone entry)
	Player_Type (player type of the team to recieve the custom, 0 = Nod, 1 = GDI, 2 = both)
	Number (first number to send)
	Number2 (second number to send)

JFW_Shader_Custom_Zone_Exit (script to send a shader custom on zone exit)
	Player_Type (player type of the team to recieve the custom, 0 = Nod, 1 = GDI, 2 = both)
	Number (first number to send)
	Number2 (second number to send)
These 2 send to the player that entered/exited the zone

JFW_Shader_Custom_Vehicle_Entry (script to send a shader custom on vehicle entry)
	Number (first number to send)
	Number2 (second number to send)

JFW_Shader_Custom_Vehicle_Exit (script to send a shader custom on vehicle exit)
	Number (first number to send)
	Number2 (second number to send)
These 2 sent to the player that entered/exited the vehicle

JFW_Shader_Custom_Create (script to send a shader custom on create)
	Number (first number to send)
	Number2 (second number to send)
This one sends to the player its attached to

JFW_Shader_Custom_Poke (script to send a shader custom on poke)
	Number (first number to send)
	Number2 (second number to send)
This one sends to the player that poked the object its attached to

JFW_Sell_Zone (script for a vehicle sell zone)
	Player_Type (player type to trigger on, 0 = Nod, 1 = GDI, 2 = both)
	Sound (2D sound to play)
	SellSound (2D sound to play when the vehicle is actually sold)
When this script is entered by a vehicle of the correct team, a 2D sound is played for the driver of the vehicle.
Also a keyhook for the "Sell" logical key is attached to the driver.
If the vehicle leaves the zone, the keyhook is removed.
If the keyhook is pressed, 50% of the vehicle cost (read from the PT data and not taking into account any power plant down doubling up of cost) is given to the driver.
Then all occupants of the vehicle are ejected and the vehicle is destroyed.

JFW_Resize_Zone (script to resize a zone at runtime)
	Size (distance x,y,z away from the centerpoint/position of the zone that it should extend)
	ZRotate (angle to rotate about the z axis)
Basicly, on startup, the script zone this script is attached to is resized per the parameters.

JFW_Cash_Spy_Zone (script to display the enemies current cash for the team of the spy)
	Spy_Script (name of the script to look for on the spy preset)
	Sound (sound to play when it triggers)
	ResetTime (how long after the zone is entered before it can be entered again by a spy)

JFW_Power_Spy_Zone (script to bring down enemy power temporarily when spy enters zone)
	Spy_Script (name of the script to look for on the spy preset)
	Power_Time (how long to bring power down for)
	Reset_Time (how long after the power comes back up before it can be brought down again)
	Sound (2d sound to play when power is brought down to tell spies team that power is down)
	Sound2 (2d sound to play when power comes back up to tell enemy team that power is back up)
Will not trigger if the power plant is dead before the spy enters the zone or if it is killed whilst the power is down (i.e. it wont come back up)

JFW_Kill_Message (script for custom kill messages)
	Message (the number to send to the object with JFW_Kill_Message_Display on it)

JFW_Kill_Message_Display (script for custom kill messages)
	ConfigFile (the config file for the kill messages)
	Message (the number to listen to from JFW_Kill_Message)
You attach JFW_Kill_Message_Display to an object in the game world and JFW_Kill_Message on objects that display the custom kill messages.
You may also want to use the DisableKillMessages hud.ini keyword to disable the stock kill messages
The config file should look something like this:
[PresetNames]
PresetNameCount=2
Preset0Name=CnC_GDI_APC
Preset0StringID=1234
Preset0SendDriverCustom=false
Preset1Name=CnC_GDI_Orca
Preset1StringID=5678
Preset1SendDriverCustom=true
The name is the preset name of the object.
The string ID is the string ID (in strings.tdb) of the object.
SendDriverCustom should be set to true for units where the driver of the vehicle is not inside it at the time of the kill.
Currently, the RA_Mine, RA_Demolition_Truck_Improved and RA_MAD_Tank_Improved scripts listen for the message sent as a result of this flag.

JFW_Low_Power_Message (script to display a message when power goes down)
	Message (what message to display)
When the team of the object with this script on it looses power, this message is displayed for everyone in the game

JFW_Spy_Switch_Team (script to switch the team of a spy)
When the object with this script on it is created, it is switched into a sort of "half enemy team" mode. You still use friendly PTs and such and can shoot the enemy.
But the enemy sees you as one of them. You do not get any points whilst acting as a spy but you CAN trigger spy zones (if they have been setup).

JFW_Spy_Vehicle_Ignore (script to ignore vehicles piloted by spies)
	Spy_Script (name of the script to look for on the spy preset)
If all the people who are inside a vehicle with this script on it have the Spy_Script script on it, the vehicle will be renedered "invisible" to base defences.

Messages for all of these scripts are displayed using the same code as for CMSG, CMSGP, CMSGT
JFW_Message_Send_Zone (script to display a message when the zone is entered)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_Message_Send_Zone_Team (Like JFW_Message_Send_Zone but the message and sound go only to the team of the player that enterd the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_Message_Send_Zone_Player (Like JFW_Message_Send_Zone but the message and sound go only to the player that enterd the zone)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_Message_Send_Death (script to display a message when the object with this script on it dies)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)

JFW_Message_Send_Death_Team (Like JFW_Message_Send_Death but the message and sound go only to a specific team)
	Player_Type (what player type to display to, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)

JFW_Message_Send_Custom (script to display a message when a custom is recieved)
	Display_Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Message (what message to listen for)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_Message_Send_Custom_Team (Like JFW_Message_Send_Custom but the message and sound go only to a specific team)
	Player_Type (what player type to display to, 0 = Nod, 1 = GDI, 2 = any)
	Display_Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Message (what message to listen for)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_2D_Sound_Death_Team (plays a 2D sound when an object dies for the team of the object that died)
	Sound (sound to play)

JFW_Vehicle_Full_Sound (plays a 2D sound and displays a message for the team that owns a vehicle when its full)
	Time (how often to check if the vehicle is full)
	TimerNum (what number to use for the timer)
	Sound (sound to play)

JFW_C4_Sound (stick this on the "Tossed C4" preset and it will play a sound when the C4 is thrown)
	Sound (2D sound to play)
The sound is played if the ammo used for this C4 has a damage value greater than zero.

JFW_Blow_Up_On_Enter_Delay (like JFW_Blow_Up_On_Enter but waits before blowing up
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Time (how long to wait)
	TimerNum (what number to use for the timer)
	Explosion (the explosion to create)

JFW_Conyard_Spy_Zone (script to stop construction yard repair activity when entered by the spy)
	Spy_Script (name of the script to look for on the spy preset)
	Power_Time (how long to stop repairing for)
	Reset_Time (how long after the repairing starts again before it can be stopped)
	Sound (2d sound to play when repairing is stopped to tell spies team that repairing has stopped)
	Sound2 (2d sound to play when repairing starts again to tell enemy team that repairing has started again)
Will not trigger if the construction yard is dead before the spy enters the zone or if it is killed whilst the repairing is stopped (i.e. it wont come back up)
Use with RA_Conyard_Controller_Improved for it to actually work

JFW_Message_Send_Zone_Player_Vehicle (Like JFW_Message_Send_Zone_Player but only triggers if the player is in a vehicle)
	Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
	Message (what message to display)
	Red (red value for the color to use)
	Green (green value for the color to use)
	Blue (blue value for the color to use)
	Sound (2D sound preset to play when the message is displayed)
	Delete (1 = delete this script when the message is displayed, 0 = dont delete)

JFW_Credit_Grant (script to grant credit to both teams at once without the need for a special building/unit)
	Credits (how many credits to grant)
	Delay (how often to grant them)

JFW_Vehicle_Regen_2 (script to regenerate health of an object over time)
	Health (how much health to regen)
	Time (how often to regen it)
This script works like M00_Vehicle_Regen_DAK only with controlable parameters

JFW_Aircraft_Refill (script to refill aircraft)
	TeamSelection (what team this zone is for)
	Reload_Interval (how often to grant reload powerup)
	Reload_Powerup (what powerup to grant to reload)

JFW_Kill_All_Buildings_Customs (kills all buildings for a team to end the game after it receieves a certain custom a certain number of times)
	Message (what message will be sent)
	Count (how many times it must be recieved before killing buildings)
	Player_Type (player type of the player whos buildings will be killed, 0 = nod, 1 = gdi)

JFW_Escort_Poke (script to make an infantry unit follow whoever last poked them)
	Shield (when someone pokes this infantry unit, the shield type will be changed to this type)
	Shield2 (when the person the unit is following pokes this infantry unit, the shield type will be changed to this type)
If the person who the infantry unit is following pokes the unit, it stops.

JFW_PT_Hide (script to hide an object on the sidebar on startup)
	Player_Type (which player type to hide this for, 0 = nod, 1 = gdi
	Preset (the name of the preset to hide)

JFW_PT_Hide_Death (script to hide an object on the sidebar when the thing its attached to dies)
	Player_Type (which player type to hide this for, 0 = nod, 1 = gdi
	Preset (the name of the preset to hide)

JFW_PT_Hide_Custom (script to hide an object on the sidebar when the thing its attached to gets a custom)
	Player_Type (which player type to hide this for, 0 = nod, 1 = gdi
	Preset (the name of the preset to hide)
	Message (the message to look out for)

JFW_Follow_Waypath_Zone (script to make all objects of a certain preset follow a waypath)
	Waypathid (the waypath ID to follow)
	Speed (how fast to follow it)
	Preset (which preset should follow it)

JFW_Vehicle_Reinforcement (script to spawn a vehicle reinforcement and make it follow a waypath)
	Vehicle (what preset to create)
	Time (how soon after map creation to create it)
	FactoryID (if this is non zero, it will check for the existance of this building before spawning the vehicle. Use this if you want the vehicle to only spawn if the war factory is still alive)
	Waypathid (the waypath ID to follow)
	Speed (how fast to follow it)

Some things such as sounds, some animations, explosions, stealth and poke don't work properly in Multiplayer.
However, thanks to the great work on bhs.dll, these things will work in multiplayer.
Note that Poke only works for people, vehicles can't poke (for obvious reasons).
Poke refers to the act of walking up to an object and pressing the action key ("e" by default).
You can poke AI controled soldiers and various static objects (the stuff listed under "simple" in leveledit) but not players or vehicles.
Any machine with scripts.dll 1.9 or greater installed must also have bhs.dll installed.
Certain features require that the clients have bhs.dll installed (see bhs.txt for details and information)
Also as far as the game is concerned, frames in an animation start at 0 not 1 like w3dview shows them.

When using a script that expects the name of a w3d, use the name without the .w3d extension.

Also, Dante has written some scripts. See Readme2.txt for details of those scripts.
And, see Readme3.txt for information on scripts by NameHunter
And, readme4.txt describes some scripts written for the RenAlert modification (but that might be useful for other things)
And, readme5.txt describes the scripts for the reborn mod, Tech Assault stuff and Survival stuff.
And, readme6.txt describes some scripts written by vloktboky.
And, readme7.txt describes some scripts written by vloktboky and improved slightly/documented by me
And, readme8.txt describes some scripts written by TheKGBSpy for his RA2 mod (but they may be useful for other mods also)
And, readme9.txt describes some scripts written be E!
And, readme10.txt describes some scripts written by NeoSaber for his Ren maps
And, readme11.txt describes some scripts written by TimeFX
And, readme12.txt describes some scripts written by Dan
And, readme13.txt describes some scripts written by SaberHawk
And, readme14.txt describes some scripts written by Matt Bailey "Whitedragon"
And, readme15.txt describes some scripts written by Andrew "Kamuix" Jones
And, readme16.txt describes scripts written by Matt Bailey "Whitedragon" to implement working naval yards and helipads where you can buy from any regular PT
And, readme17.txt describes scripts written by Zunnie
And, readme18.txt describes scripts written by Jerard Grey for the RP2 map
Also, linux.txt contains info about the LFDS port of the scripts.
And, bhs.txt contains details of the fixes and stuff in bhs.dll
see keys.txt for details of the valid key names for the keys.cfg config file.
see keycfg.txt for details of the GUI key config application.
see console.txt for some details of how to get various things from the game engine.

System Messages:
Certain messages are special system messages. Any script taking a message number can take one of these message numbers (as far as I know).
So far, I am aware of the folowing messages: (some of these are educated guesses)
1000000000 this is what I believe is the marker that specifies the first system message (I don't know what it does, just that it's the first one)
1000000001 this one is sent when a sound finishes playing (if it was monitored by Monitor_Sound)
1000000002 this one is sent on toggle power
1000000003 this one is sent when the thing gets to the dock
1000000025 this one is sent to an item when it's collected
1000000026 all I know is that this one has something to do with damage
1000000027 all I know is that this one has something to do with getting health back
1000000028 this one gets sent to a vehicle when something enters it
1000000029 this one gets sent to a vehicle when something exits it
1000000030 this one gets sent on something to do with arriving at attack location
1000000031 this one gets sent on something to do with audience for a conversation
1000000032 this one gets sent on something to do with say next remark
1000000033 this one also gets sent on something to do with say next remark but a different bit of code
1000000035 this one gets sent when the jump is complete
1000000036 this one is sent anytime a vehicle is bought from the helipad or naval yard setup with the ExpVehFac scripts or when a vehicle is bought from a weapons factory or airstrip (with or without the ExpVehFac scripts)

That should be (I hope) all the system messages
Note that when using 1000000025 the sender is the object that collected the item.
For 1000000028 and 1000000029, the sender is the object that entered/exited the vehicle

As far as I can tell, the sounds passed to these scripts are preset names. Unless specified otherwise, they should be 3D sounds.
Note that a "custom" and a "message" are the same thing, it's something that gets sent from one script to another, usually to tell the second script to do something based on what the first script has done

Another note about script zones. If you enter a script zone inside a vehicle and the zone has "check stars only" = on, only the player will trigger the zone, not the vehicle.
If "check stars only" = off, it will trigger on the vehicle and on the player. (the vehicle is triggered first on zone entry, the player is triggered first on zone exit)

Another warning: Never leave a script parameter blank. If it's a string parameter, set it to some dummy value. If it's a numeric parameter, set it to 0 or something.

Some notes for those coding new scripts:
Please only code scripts to work with the latest dll. The older versions may contain bugs that are fixed in the latest dll.
Also, NEVER store a GameObject as a class member variable. Instead, store the ID of that object and use Commands->Get_ID and Commands->Find_Object to convert between ID and object.
In fact, NEVER store a pointer to anything as a class member variable.
Referencing the contents of a GameObject is not recommended unless you know what you are doing.
If you pass the wrong thing to one of the Get_Parameter functions, things may freeze or crash
Also, don't use the Static keyword for anything.
Also, the ::Created event might be called twice in some cases, this is an engine limit/issue and you can't avoid that.
If anyone wants help with scripts coding or related items, let me know and I will do the best I can.
note that gamedata.h is now obsolete, its contents are now part of engine.h.
So you can delete gamedata.h.

USE THE SCRIPTS.DLL AT YOUR OWN DISCRETION, THIS IS NOT SUPPORTED BY WESTWOOD STUDIOS NOR IS IT RELATED IN ANY WAY.
