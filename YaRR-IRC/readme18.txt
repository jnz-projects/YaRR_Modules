========================
Jerad Gray's RP2 Scripts
========================

JMG_Slot_Mahine_On_Poked - This is an advanced slot machine script triggered by poking
	Cost - cost it takes to play
	Winnings - amount you can win
	Percent - percent chance you have to win
	Animation - Animation to play when poked (this also controls how much time it takes to play again)
	Sound - Sound to play when you poke
	EnableMessage - message to enable the slot machine
	DisableMessage - Message used to disable the slot machine
	StartEnabled - Whether or not to start enabled (1 = start enabled 0 = start disabled)

JMG_Visible_Infantry_In_Vehicle - Basically a clone of JFW's vehicle driver script the only difference is that the attached 	
solder well have its collisions disabled (this is attached to the soldier).
	ModelName - name of 3d model for driver (do not include .w3d)
	Message - custom to make visible
	Animation - animation to play
	SubObject - just set this to zero if you don't know what its for
	FirstFrame - starting animation frame
	LastFrame - ending frame (set this to -1)
	Blended - just leave this at zero unless you know otherwise

JMG_Attach_Turret_And_Send_Custom - this is a turret attach script that sends a custom to destroy the attached object.
	Preset - name of object ot attach
	CreateMessage - Message used to create the turret object
	SendMessageMessage - message to send to the turret object
	SendMessage - message that triggers the message send
	SendParam - just use 1 unless you have a good idea of what you are doing
	BoneName - bone to attach to

JMG_Flash_Light_Toggle - toggles a custom every time a custom is received
	SendID1 - First object to send to
	SendID2 - Second  object to send to
	SendMessage1 - First message to send
	SendMessage2 - Second message to send
	TriggerMessage - Message that triggers the sending of messages

JMG_Dual_Weapon_Script - RP2's Dual Weapon script (there will be a new better script soon, I will be bringing the one from Renhalo over, this one works much better)!
	Message - message to send when specific weapon is out
	Weapon - Weapon that must be out to send custom

JMG_Free_For_All_Script - Only use this for RP2 (beings it attaches a bunch of other scripts)

MDB_Block_Refill_RP2 - Permanently Prevents you from EVER getting more health
