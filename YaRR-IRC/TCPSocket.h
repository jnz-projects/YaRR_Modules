class TCPSocket
{
	SOCKET m_Socket;
	char m_Mode;
	bool m_Connected;
	WSAEVENT m_Event;
public:

	TCPSocket();
	TCPSocket(SOCKET &Socket, char Type);
	TCPSocket(const char *Host, int Port);
	TCPSocket(const char *IP, int Port, int Backlog);
	TCPSocket(TCPSocket &Socket);

	~TCPSocket();

	static bool Startup();
	static bool Shutdown();

	bool Client(const char *Host, int Port);
	bool Server(const char *IP, int Port, int Backlog);
	bool Sendf(const char *Format, ...);
	bool Send(const char *Data, int Length);
	bool Receive(char *Buffer, int Length);
	bool Accept(TCPSocket *Socket);
	bool Is_Connected();
	bool Is_DataAvaliable();
	bool Destroy();
};