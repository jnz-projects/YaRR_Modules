typedef void (*_IRC_Packet_Callback)(const char *);
typedef void (*_IRC_Channel_Callback)(const char *, const char *, const char *);

class YaRRIRC_Base
{
protected:
	TCPSocket *Socket;
	char Server[128];
	char m_Nick[128];
	int Port;
	bool Connected;
	

	_IRC_Packet_Callback IRC_Packet_Callbacks[16];
	_IRC_Channel_Callback IRC_Channel_Callbacks[16];
public:
	
	struct User
	{
		char Nick[32];
		char Mode;
	};

	struct Channel
	{
		User *Users[64];
		char Name[32];
	};
	

	Channel *Channels[32];

	
	virtual void Send(const char *Command) = 0;
	virtual void Send_Channel(const char *Channel, const char *Message) = 0; 
	virtual void Sendf(const char *Format, ...) = 0;
	virtual void Send_Channelf(const char *Channel, const char *Format, ...) = 0;
	virtual void Add_Packet_Callback(_IRC_Packet_Callback _Callback) = 0;
	virtual void Add_Channel_Callback(_IRC_Channel_Callback _Callback) = 0;
	virtual void Send_Handshake(const char *Nick) = 0;
	virtual Channel *Find_Channel(const char *Name) = 0;
	virtual User *Find_User(const char *Channel, const char *Nick) = 0;
};

class YaRRIRC : public YaRRIRC_Base
{
	char Packet[512];
	int Packet_Length;
public:

	YaRRIRC();
	~YaRRIRC();

	void Load();
	void Unload();
	void Send(const char *Command);
	void Send_Channel(const char *Channel, const char *Message);
	void Sendf(const char *Format, ...);
	void Send_Channelf(const char *Channel, const char *Format, ...);
	void Add_Packet_Callback(_IRC_Packet_Callback _Callback);
	void Add_Channel_Callback(_IRC_Channel_Callback _Callback);

	Channel *Find_Channel(const char *Name);
	User *Find_User(const char *Channel, const char *Nick);

	void Connect(const char *Host, int Port);
	void Send_Handshake(const char *Nick);
	void Packet_Callback(const char *Packet);
	void Channel_Callback(const char *Nick, const char *Channel, const char *Message);
	void Think();
};

extern YaRRIRC *IRCInstance;