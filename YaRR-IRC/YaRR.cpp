#include "Winsock2.h"
#include "Windows.h"
#include "scripts.h"
#include "engine.h"
#include "TCPSocket.h"
#include "YaRR.h"
#include "Hooks.h"
#include "YaRR-IRC.h"

_Request_Load Request_Load;
_Request_Unload Request_Unload;
_Request_Reload Request_Reload;
DWORD BaseAddress = 0;

void YaRR_Load()
{
	IRCInstance = new YaRRIRC;
	IRCInstance->Load();
	Load_Hooks();
	AddThinkHook(Think_Hook);

	IRCInstance->Connect("irc.n00bless.com", 6667);
}

void YaRR_Unload()
{
	IRCInstance->Unload();
	delete IRCInstance;
	IRCInstance = 0;
	Unload_Hooks();
}

void Think_Hook()
{
	IRCInstance->Think();
}

extern "C" __declspec(dllexport) void *Get_Instance()
{
	return (void *)&IRCInstance;
}





void Explode(const char *String, char Delimiter, char **Array)
{
	char buf[1024];
	int y = 0;
	for(unsigned int i = 0; i <= strlen(String); i++)
	{
		buf[y] = String[i];
		if(buf[y] == Delimiter)
		{
			buf[y] = '\0';
			*Array = _strdup(buf);
			Array++;
			y = 0;
			continue;
		}
		else if(buf[y] == '\0')
		{
			if(y > 0)
			{
				*Array = _strdup(buf);
				Array++;
			}
			break;
		}
		y++;
	}
}