#include "Winsock2.h"
#include "Windows.h"
#include "scripts.h"
#include "engine.h"
#include "TCPSocket.h"
#include "YaRR.h"
#include "Hooks.h"
#include "YaRR-IRC.h"

_Request_Load Request_Load;
_Request_Unload Request_Unload;
_Request_Reload Request_Reload;
DWORD BaseAddress = 0;

YaRRIRC_Base **IRC_Handle = 0;

void YaRR_Load()
{
	Load_Hooks();
	Request_Load("YaRR-IRC.dll");
	IRC_Handle = (YaRRIRC_Base **)Get_Dll_Instance("YaRR-IRC.dll");
}

void YaRR_Unload()
{
	Unload_Hooks();
}

void *Get_Dll_Instance(const char *dll)
{
	HMODULE mod = GetModuleHandle("YaRR-IRC.dll");
	if(mod)
	{
		_Get_Instance Get_Instance = (_Get_Instance)GetProcAddress(mod, "Get_Instance");
		if(Get_Instance)
		{
			return Get_Instance();
		}
	}
	return 0;
}
