; ************************* Scripts by SCUD *************************



====================================================================================================

; ************************* [Script Name] SCUD_CHHarvester

====================================================================================================

 [Description]

 - Attach this to the China Harvester.

 [Parameters]

 - Harvester_Control_ID	(ID of the object that the SCUD_Harvester_Control script is attached to)

 - Explosion_Preset_Name (The explosion to destroy the harvester on limit reached)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_CHHarvester_Terminal

====================================================================================================

 [Description]

 - China version. This is the same as SCUD_CustomSend_PresetBuy_FollowWaypath but it works together with the SCUD_Harvester_Control script.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - SendID (ID where the message gets sent to)

 - SendMessage (Message that gets sent)

 - SendParam (Parameter that gets sent with the message)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 - Waypath_enabled (Enable waypath following after creation. 1 = enable, 0 = disable)

 - WaypathID (ID of the waypath the vehicle will follow)

 - Speed (Speed of the vehicle while following the waypath)

 - Harvester_Control_ID (The ID of the object that the SCUD_Harvester_Control script is attached to)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Custom_Follow_Waypath

====================================================================================================

 [Description]

 - The object follows a waypath when a custom is sent to it.

 [Parameters]

 - WaypathID (The ID of the waypath the object should follow)

 - Speed (The speed with that it should follow)

 - Message (The message to make the object start following the Waypath)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_CustomSend_Preset_Buy

====================================================================================================

 [Description]

 - This is the same as SCUD_Preset_Buy but you now send a custom on vehicle enter and the preset will be bought on exit. Your terminal has to set up as a vehicle because this script uses the enter message that is only sent by vehicles.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - SendID (ID where the message gets sent to)

 - SendMessage (Message that gets sent)

 - SendParam (Parameter that gets sent with the message)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_CustomSend_PresetBuy_FollowWaypath

====================================================================================================

 [Description]

 - This is the same as SCUD_CustomSend_PresetBuy but now it�s possible to let the bought vehicle follow a waypath after creation.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - SendID (ID where the message gets sent to)

 - SendMessage (Message that gets sent)

 - SendParam (Parameter that gets sent with the message)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 - Waypath_enabled (Enable waypath following after creation. 1 = enable, 0 = disable)

 - WaypathID (ID of the waypath the vehicle will follow)

 - Speed (Speed of the vehicle while following the waypath)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_CustomSend_PresetBuy_GeneralPresets_FollowWaypath

====================================================================================================

 [Description]

 - This is the same as SCUD_CustomSend_PresetBuy but now it�s possible to let the bought vehicle follow a waypath after creation.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - SendID (ID where the message gets sent to)

 - SendMessage (Message that gets sent)

 - SendParam (Parameter that gets sent with the message)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 - General1_Preset_ID (Preset ID of the First General)

 - General2_Preset_ID (Preset ID of the Second General)

 - General3_Preset_ID (Preset ID of the Third General)

 - General1_Preset_Name (Preset that gets bought by the First General)

 - General2_Preset_Name (Preset that gets bought by the Second General)

 - General3_Preset_Name (Preset that gets bought by the Third General)

 - General1_Preset_Cost (Money that the First General's preset Cost)

 - General2_Preset_Cost (Money that the Second General's preset Cost)

 - General3_Preset_Cost (Money that the Third General's preset Cost)

 - Waypath_enabled (Enable waypath following after creation. 1 = enable, 0 = disable)

 - WaypathID (ID of the waypath the vehicle will follow)

 - Speed (Speed of the vehicle while following the waypath)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_DeathZoneOnCustom

====================================================================================================

 [Description]

 - This script is thought to work with all SCUD_CustomSend... scripts to clear the location where a bought vehicle spawns.

 [Parameters]

 - Message (The message to clear the zone)

 - Explosion (The explosion that is used to clear the zone)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Deploy_Animation

====================================================================================================

 [Description]

 - The animation for SCUD_Deployable_Vehicle.

 [Parameters]

 - Time_Till_Death (The time that it takes the deploy animation to finish. After this time the preset gets destroyed)

 - Deployed_Tank_Preset	(The deployed state preset that gets created when Time_Till_Death expires)

 - Explosion_Preset (The preset name of the explosion that destroys the animated preset)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Deployable_Vehicle

====================================================================================================

 [Description]

 - Works with SCUD_Deploy_Animation to make something like an buildup animation for vehicles.

 [Parameters]

 - Animated_Preset (The animated preset name that shows the deploy animation)

 - Explosion_Preset (The preset name of the explosion that destroys the undeployed preset)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_DozerBuildTurret

====================================================================================================

 [Description]

 - This is for the bulldozer to build turrets.

 [Parameters]

 - Turret_PresetName (Name of the turret preset)

 - Cost (How much the turret costs)

 - Controller_ID (A controller that can be used for a turret limit. The harvester controller should work)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Follow_Waypath_Until_Custom

====================================================================================================

 [Description]

 - The object follows a Waypath until a custom is sent to it.

 [Parameters]

 - WaypathID (ID of the waypath the object will follow)

 - Speed (Speed of the object while following the waypath)

 - Message (The message to make the object stop following the Waypath)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Harvester_Control

====================================================================================================

 [Description]

 - This script works together with the SCUD_CHHarvester, SCUD_USHarvester, SCUD_CHHarvester_Terminal, and SCUD_USHarvester_Terminal scripts.

 [Parameters]

 - USA_Harvester_Limit (The limit for USA�s harvesters. However, you have to enter 1 more harvester than you want. If you allow 5 harvesters it will be 4 ingame)

 - China_Harvester_Limit (The limit for China�s harvesters. However, you have to enter 1 more harvester than you want. If you allow 5 harvesters it will be 4 ingame)

 - US_Harvester_Terminal_ID (ID of the object that the SCUD_USHarvester_Terminal script is attached to)

 - CH_Harvester_Terminal_ID (ID of the object that the SCUD_CHHarvester_Terminal script is attached to)

 - USA_Harvester_Cost (Cost of the USA harvester for getting money back upon limit reached)

 - China_Harvester_Cost (Cost of the China harvester for getting money back upon limit reached)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Harvester_Zone

====================================================================================================

 [Description]

 - This zone allows 2 harvester presets from a team to earn money and 1 thief from the enemy Team.

 [Parameters]

 - Credits (How many credits the harvesters earn)

 - Next_Harvester_Time (How long until the next harvester can earn credits)

 - Harvester_Preset_ID (Preset ID of the primary harvester)

 - LvlHarvester_Preset_ID (Preset ID of the secondary harvester)

 - Thief_Credits (How many credits a thief can get while in the zone)

 - Thief_Time (How much time has to pass until a thief is able to earn money again)

 - Thief_Preset_ID (Preset ID of the thief)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Poke_Preset_Buy

====================================================================================================

 [Description]

 - This can make your own "purchase terminal" but for only 1 Vehicle. This way you can realize tech levels!

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Preset_Buy

====================================================================================================

 [Description]

 - This is the same as SCUD_Poke_Preset_Buy but it works in MP too. Your Terminal has to set up as a vehicle because this script use the enter message that only is sent by vehicles.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Set_Player_Type_OnCustom

====================================================================================================

 [Description]

 - This is just a test script. It sets the Player_type of the object to whatever it is attached to.

 [Parameters]

 - Message (The message to change the Player_Type)

 - Player_Type (Change to a certain Player_type. 0 = China, 1 = USA, 2 = Player_type of the sender)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_SuperWeapon

====================================================================================================

 [Description]

 - This script destroys itself after a time that starts with the first shot (the shot have to hit the object).

 [Parameters]

 - SuperWeaponAmmo (Enter the ammo of the superweapon the timer will start after the last ammo is shot)

 - FireTimeSinceFirstShot (Input the time in seconds)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_Turret_Spawn

====================================================================================================

 [Description]

 - Spawns a turret on a bone of the object it is attached to and destroys the turret with the object.

 [Parameters]

 - Turret_Preset (Name of the turret preset)

 - Bone_Name (Bone where the turret spawns)

 - Explosion_Preset (Preset name of the explosion that destroys the turret)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_TurretBuildup

====================================================================================================

 [Description]

 - This is a buildup animation for use with turrets.

 [Parameters]

 - Buildup_Time (Time that the animation needs to finish "building" the Turret)

 - Turret_PresetName (Name of the finished turret preset)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_USHarvester

====================================================================================================

 [Description]

 - Attach this to the USA Harvester.

 [Parameters]

 - Harvester_Control_ID	(ID of the Object that the SCUD_Harvester_Control script is attached to)

 - Explosion_Preset_Name (The explosion to destroy the harvester on limit reached)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] SCUD_USHarvester_Terminal

====================================================================================================

 [Description]

 - USA version. This is the same as SCUD_CustomSend_PresetBuy_FollowWaypath but it works together with the SCUD_Harvester_Control script.

 [Parameters]

 - Unknown (Unknown parameter, 0 works)

 - SendID (ID where the message gets sent to)

 - SendMessage (Message that gets sent)

 - SendParam (Parameter that gets sent with the message)

 - Preset_Name (Name of the preset to buy)

 - Cost (Cost for the preset on purchase)

 - location (Location where the preset gets created on purchase)

 - Player_Type (Which side the terminal works for. 0 = China, 1 = USA)

 - Rotation (Where the bought object faces on creation)

 - Waypath_enabled (Enable waypath following after creation. 1 = enable, 0 = disable)

 - WaypathID (ID of the waypath the vehicle will follow)

 - Speed (Speed of the vehicle while following the waypath)

 - Harvester_Control_ID (The ID of the object that the SCUD_Harvester_Control script is attached to)

 [NOTES]

 - NONE