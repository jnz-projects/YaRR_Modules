/*	Renegade Scripts.dll
	Main loading code
	Copyright 2007 Datalore, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "engine.h"
#include "YaRR.h"

#define SCRIPTSAPI __declspec(dllexport)

typedef void (*srdf) (rdf);
typedef void (*ds) (ScriptClass *scr);
typedef ScriptClass *(*cs) (const char *);
typedef int (*gsc) ();
typedef char *(*gsn) (int);
typedef char *(*gspd) (int);
typedef bool (*ssc) (ScriptCommandsClass *);

ScriptCommands *Commands = 0;

BOOL SCRIPTSAPI __stdcall DllMain(HINSTANCE hinstDLL, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			BaseAddress = (DWORD)hinstDLL;
			DisableThreadLibraryCalls(hinstDLL);
			if (Exe == 6)
			{
				InitEngine();
			}
			break;
		case DLL_PROCESS_DETACH:
			YaRR_Unload();
			DestroyEngineMath();
			break;
	}
	return TRUE;
}


extern "C" void SCRIPTSAPI Set_Script_Commands(const int *Hooks, ScriptCommands *_Commands, _Request_Load RL, _Request_Unload RU, _Request_Reload RR)
{
	Request_Load = RL;
	Request_Unload = RU;
	Request_Reload = RR;
	Commands = _Commands;

	Enable_Stealth_Player = (_Enable_Stealth_Player)Hooks[0];
	Set_Fog_Enable_Player = (_Set_Fog_Enable_Player)Hooks[1];
	Set_Fog_Range_Player = (_Set_Fog_Range_Player)Hooks[2];
	Set_Background_Music_Player = (_Set_Background_Music_Player)Hooks[3];
	Fade_Background_Music_Player = (_Fade_Background_Music_Player)Hooks[4];
	Stop_Background_Music_Player = (_Stop_Background_Music_Player)Hooks[5];
	Enable_Radar_Player = (_Enable_Radar_Player)Hooks[6];
	Display_GDI_Player_Terminal_Player = (_Display_GDI_Player_Terminal_Player)Hooks[7];
	Display_NOD_Player_Terminal_Player = (_Display_NOD_Player_Terminal_Player)Hooks[8];
	Set_Screen_Fade_Color_Player = (_Set_Screen_Fade_Color_Player)Hooks[9];
	Set_Screen_Fade_Opacity_Player = (_Set_Screen_Fade_Opacity_Player)Hooks[10];
	Force_Camera_Look_Player = (_Force_Camera_Look_Player)Hooks[11];
	Enable_HUD_Player = (_Enable_HUD_Player)Hooks[12];
	Create_Sound_Player = (_Create_Sound_Player)Hooks[13];
	Create_2D_Sound_Player = (_Create_2D_Sound_Player)Hooks[14];
	Create_2D_WAV_Sound_Player = (_Create_2D_WAV_Sound_Player)Hooks[15];
	Create_3D_WAV_Sound_At_Bone_Player = (_Create_3D_WAV_Sound_At_Bone_Player)Hooks[16];
	Create_3D_Sound_At_Bone_Player = (_Create_3D_Sound_At_Bone_Player)Hooks[17];
	Set_Display_Color_Player = (_Set_Display_Color_Player)Hooks[18];
	Display_Text_Player = (_Display_Text_Player)Hooks[19];
	Display_Int_Player = (_Display_Int_Player)Hooks[20];
	Display_Float_Player = (_Display_Float_Player)Hooks[21];
	Set_Obj_Radar_Blip_Shape_Player = (_Set_Obj_Radar_Blip_Shape_Player)Hooks[22];
	Set_Obj_Radar_Blip_Color_Player = (_Set_Obj_Radar_Blip_Color_Player)Hooks[23];
	AddObjectCreateHook = (aoch)Hooks[24];
	RemoveObjectCreateHook = (roch)Hooks[25];
	AddKeyHook = (akh)Hooks[26];
	RemoveKeyHook = (rkh)Hooks[27];
	Set_Scope = (ss)Hooks[28];
	Set_HUD_Texture = (sh)Hooks[29];
	AddChatHook = (ach)Hooks[30];
	AddHostHook = (ahh)Hooks[31];
	AddLoadLevelHook = (allh)Hooks[32];
	AddGameOverHook = (allh)Hooks[33];
	AddPlayerJoinHook = (apjh)Hooks[34];
	GetCurrentMusicTrack = (gcmt)Hooks[335];
	Set_Info_Texture = (sit)Hooks[36];
	Clear_Info_Texture = (cit)Hooks[37];
	Set_Vehicle_Limit = (svl)Hooks[38];
	Get_Vehicle_Limit = (gvl)Hooks[39];
	Send_Message = (sm)Hooks[40];
	Send_Message_Player = (smp)Hooks[41];
	AddVersionHook = (avh)Hooks[42];
	Set_Wireframe_Mode = (sw)Hooks[43];
	Load_New_HUD_INI = (lnhi)Hooks[44];
	Remove_Weapon = (rw)Hooks[45];
	Update_PT_Data = (upd)Hooks[46];
	Change_Radar_Map = (crm)Hooks[47];
	AddPowerupPurchaseHook = (aph)Hooks[48];
	AddVehiclePurchaseHook = (aph)Hooks[49];
	AddCharacterPurchaseHook = (aph)Hooks[50];
	AddPowerupPurchaseMonHook = (apmh)Hooks[51];
	AddVehiclePurchaseMonHook = (apmh)Hooks[52];
	AddCharacterPurchaseMonHook = (apmh)Hooks[53];
	RemovePowerupPurchaseHook = (rph)Hooks[54];
	RemoveVehiclePurchaseHook = (rph)Hooks[55];
	RemoveCharacterPurchaseHook = (rph)Hooks[56];
	RemovePowerupPurchaseMonHook = (rph)Hooks[57];
	RemoveVehiclePurchaseMonHook = (rph)Hooks[58];
	RemoveCharacterPurchaseMonHook = (rph)Hooks[59];
	Get_Build_Time_Multiplier = (gbm)Hooks[60];
	Set_Currently_Building = (scb)Hooks[61];
	Is_Currently_Building = (icb)Hooks[62];
	AddConsoleOutputHook = (acoh)Hooks[63];
	AddCRCHook = (acrch)Hooks[64];
	AddDataHook = (adh)Hooks[65];
	Set_Reticle_Texture1 = (srt)Hooks[66];
	Set_Reticle_Texture2 = (srt)Hooks[67];
	Set_Fog_Color = (sfc)Hooks[68];
	Set_Fog_Color_Player = (sfcp)Hooks[69];
	Set_Fog_Mode = (sfm)Hooks[70];
	Set_Fog_Mode_Player = (sfmp)Hooks[71];
	Set_Shader_Number = (ssn)Hooks[72];
	Send_HUD_Number = (shn)Hooks[73];
	Set_Fog_Density = (sfd)Hooks[74];
	Set_Fog_Density_Player = (sfdp)Hooks[75];
	Change_Time_Remaining = (ctr)Hooks[76];
	Change_Time_Limit = (ctl)Hooks[77];
	Display_GDI_Sidebar = (dps)Hooks[78];
	Display_NOD_Sidebar = (dps)Hooks[79];
	Display_Security_Dialog = (dps)Hooks[80];
	AddPlayerLeaveHook = (aplh)Hooks[81];
	GetExplosionObj = (geo)Hooks[82];
	GetBHSVersion = (gbhsv)Hooks[83];
	Set_Shader_Number_Vector = (ssn2)Hooks[84];
	AddShaderNotify = (asn)Hooks[85];
	RemoveShaderNotify = (rsn)Hooks[86];


	YaRR_Load();

}

