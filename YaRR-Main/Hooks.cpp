#include "Windows.h"
#include "stdio.h"
#include "scripts.h"
#include "engine.h"
#include "YaRR.h"
#include "Hooks.h"

_RequestSerial RequestSerial = 0;
_AddSerialHook AddSerialHook = 0;
_AddLoadingEHook AddLoadingEHook = 0;
_AddDamageHook AddDamageHook = 0;
_AddChatEHook AddChatEHook = 0;
_AddPingHook AddPingHook = 0;
_AddSuicideHook AddSuicideHook = 0;
_AddRadioHook AddRadioHook = 0;
_AddThinkHook AddThinkHook = 0;

inline void LoadHook(HMODULE dll, void **hook, const char *name)
{
	*hook = (void *)GetProcAddress(dll, name);
	if(!*hook)
	{
		printf("Error loading \"%s\"", name);
		*hook = 0;
	}
}

HMODULE hooks;
void Load_Hooks()
{
	hooks = LoadLibrary("Hooks.dll");
	YaRR_Assert(hooks);
	LoadHook(hooks, (void **)&RequestSerial, "RequestSerial");
	LoadHook(hooks, (void **)&AddSerialHook, "AddSerialHook");
	LoadHook(hooks, (void **)&AddLoadingEHook, "AddLoadingEHook");
	LoadHook(hooks, (void **)&AddDamageHook, "AddDamageHook");
	LoadHook(hooks, (void **)&AddChatEHook, "AddChatHook");
	LoadHook(hooks, (void **)&AddPingHook, "AddPingHook");
	LoadHook(hooks, (void **)&AddSuicideHook, "AddSuicideHook");
	LoadHook(hooks, (void **)&AddRadioHook, "AddRadioHook");
	LoadHook(hooks, (void **)&AddThinkHook, "AddThinkHook");
}

void Unload_Hooks()
{
	FreeLibrary(hooks);
}