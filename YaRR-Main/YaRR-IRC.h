typedef void (*_IRC_Packet_Callback)(const char *);
typedef void (*_IRC_Channel_Callback)(const char *, const char *, const char *);

class YaRRIRC_Base
{
protected:
	TCPSocket *Socket;
	char Server[128];
	int Port;
	bool Connected;

	_IRC_Packet_Callback IRC_Packet_Callbacks[16];
	_IRC_Channel_Callback IRC_Channel_Callbacks[16];
public:
	virtual void Send(const char *Command) = 0;
	virtual void Send_Channel(const char *Channel, const char *Message) = 0;
	virtual void Sendf(const char *Format, ...) = 0;
	virtual void Send_Channelf(const char *Channel, const char *Format, ...) = 0;
	virtual void Add_Packet_Callback(_IRC_Packet_Callback _Callback) = 0;
	virtual void Add_Channel_Callback(_IRC_Channel_Callback _Callback) = 0;
	virtual void Send_Handshake(const char *Nick) = 0;
};

extern YaRRIRC_Base **IRC_Handle;