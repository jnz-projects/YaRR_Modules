#define YaRR_Assert(c) if(!(c)) { char _tmp[256]; sprintf(_tmp, "Assertion failed! (%s) %s:%s:%d", #c, __FILE__, __FUNCTION__, __LINE__); MessageBox(NULL, _tmp, "Fatal Error", MB_OK | MB_ICONERROR); exit(1); }

typedef void (*_Request_Load)(const char *);
typedef void (*_Request_Unload)(HMODULE);
typedef void (*_Request_Reload)(HMODULE);
typedef void *(*_Get_Instance)();

extern _Request_Load Request_Load;
extern _Request_Unload Request_Unload;
extern _Request_Reload Request_Reload;
extern DWORD BaseAddress;

void YaRR_Load();
void YaRR_Unload();

void Think_Hook();
void *Get_Dll_Instance(const char *dll);