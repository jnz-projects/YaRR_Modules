********************************************************************************************
RenAlert Scripts...
By Tom "Dante" Anderson
Dante@planetcnc.com
********************************************************************************************


**********************************************************************************************


Script Name:	"RA_Thief_Script"

Desc:			Thief Script, very useful indeed..., will award however many credits to whichever team owns the thief.

Script Parameters:
				Credits_To_Award=500:float
				Prevent_Theft_Timer=30:float
				Thief_Preset_ID:string

Notes:			Set this on a zone, will trigger on zone entry.
				The Thief_Preset_ID should be the ID of the thief preset
				
				
**********************************************************************************************

Script Name:	"RA_Soviet_Defence"

Desc:			Base Defense Script, to ignore the Spy (use for Soviets only)

Script Parameters:
				MinAttackDistance=0:float
				MaxAttackDistance=300:float
				AttackTimer=10:float
				Spy_Preset_ID:string
				Aircraft_Preset_ID:string

Notes:			Set this up exactly like M00_Base_Defense, but will ignore the spy and aircraft.

				
**********************************************************************************************	

Script Name:	"RA_Repair_Controler"

Desc:			Service Depot building controler script

Script Parameters:
			ScriptZoneID=0:int

Notes:			Stick this on the building controller for the repair bay

				
**********************************************************************************************	

Script Name:	"RA_Repair_Script"

Desc:			Repair Zone script

Script Parameters:
			TeamSelection=0:int

Notes:			Stick this on the repair bay zone to make it repair. It is hardcoded to only repair presets whos ID matches that of the land vehicles in RenAlert.
			Use JFW_Repair_Bay & friends for a more generic script.

				
**********************************************************************************************	

********************************************************************************************
RenAlert Scripts
By Joshua "NeoSaber" Kraft
********************************************************************************************

**********************************************************************************************	

Script Name: "RA_Allied_Thief"


Description:

A new script for the thief, it grants the thief's team credits when he enters a zone with the script "RA_Soviet_Refinery_Theft_Zone", as well as every 30 seconds (by default, this can be changed) the thief remains inside the zone.

Script Parameters:

-Credits_To_Award: How many credits the thief gets per theft. The default amount is 500.00

-Prevent_Theft_Timer: How many seconds to wait between thefts. Default is 30.00

Notes: 

This script is attached to the preset for whatever soldier is being used as the thief

**********************************************************************************************	

Script Name: "RA_Soviet_Refinery_Theft_Zone"


Description:

This script works in conjunction with the script "RA_Allied_Thief". It checks when a thief enters or exits a zone and sends the thief a custom. Also, when it recieves a custom from the script "RA_Soviet_Refinery_Controller" it will stop sending the thief a message when entered

Script Parameters:

-Thief_Preset_ID: This is the preset ID for the thief. Default is 0

Notes: 

This script is attached to a script zone. Where ever the script zone is placed is where the thief has to go to steal.

**********************************************************************************************	

Script Name: "RA_Soviet_Refinery_Controller"


Description:

This script is designed to tell the script "RA_Soviet_Refinery_Theft_Zone" when the building it is attached to has been destroyed.

Script Parameters:

-ScriptZoneID: This is the ID of the script zone with "RA_Soviet_Refinery_Theft_Zone". This is NOT the preset ID, it is the ID of the zone placed on the map. Default is 0

Notes:

This script is attached to a building controller, for example the refinery a thief can steal from.

**********************************************************************************************	

Script Name: "RA_Infantry_Spy"


Description: 

This script makes the soldier it is attached to invisible to base defenses. 

Script Parameters: 

None

Notes: 

This script is attached to the preset for whatever soldier is a Spy

**********************************************************************************************	

Script Name: "RA_Infantry_NotSpy"


Description:

This script makes the soldier it is attached to visible to base defenses. This is necessary because the 'invisibility' of the script "RA_Infantry_Spy" is actually given to the player who bought the soldier and not the soldier itself. This means that if a player bought a spy and then bought another type of soldier, they would still be invisible to base defenses. This script corrects that.

Script Parameters:

None

Notes: 

This script is attached to the preset of every soldier that is NOT a Spy, unless that soldier can never be purchased by a player that can purchase a spy. For example, an allied player can buy a Spy, so all allied infantry that are not spies need this script. However, since a soviet player can never be a spy, no Soviet infantry need this script.

**********************************************************************************************	

Script Name: "RA_Helipad_Zone"


Description: 

This script is designed to reload the ammunition of specified vehicles.

Script Parameters:

-TeamSelection: Determines which team this zone works for. 0 = Soviet, 1 = Allied

-Reload_Interval: Interval between individual reloads. Time is in seconds, default is 2.00. This allows for slowly reloading a vehicle, like a repair script slowly repairs a vehicle.

-Apache_Preset_ID: This is the preset ID of a vehicle that will be reloaded by the powerup specified by Apache_Reload_Powerup. Default is 0

-Apache_Reload_Powerup: This is the preset name of a powerup that will be granted to a vehicle matching the Apache_Preset_ID parameter. Default is 'Blank'

-Hind_Preset_ID: This is the preset ID of a vehicle that will be reloaded by the powerup specified by Hind_Reload_Powerup. Default is 0

-Hind_Reload_Powerup: This is the preset name of a powerup that will be granted to a vehicle matching the Hind_Preset_ID parameter. Default is 'Blank'

Notes: 

Script is attached to a script zone. It's setup for the option to allow slow reloads, but can be used for instant refills by specifying a powerup that refills all the ammunition instead of a limited amount.

**********************************************************************************************	

Script Name: "RA_Mine_Layer"


Description: 

This script is designed to be attached to a mine laying vehicle.

Script Parameters:

-Mine_Manager_ID: This is the instance ID of the object with the script RA_Mine_Manager

Notes: 

This script detects when the driver does 0.0 damage to the vehicle. The weapon on the vehicle needs to be setup properly to allow for this.

**********************************************************************************************	

Script Name: "RA_Mine"


Description: 

This script determines if a mine is anti-personnel and/or anti-tank. If the appropriate enemy target comes in to range, the mine self destructs. When destroyed it sends a custom to the Mine Manager.

Script Parameters:

-Mine_Manager_ID: This is the instance ID of the object with the script RA_Mine_Manager. Default is 0.

-Is_Anti-Personnel: Determines if this will blow up on soldiers. 0 = no, 1 = yes

-Is_Anti-Tank: Determines if this will blow up on vehicles. 0 = no, 1 = yes

-Trigger_Damage: Amount of damage to destroy the mine with. Default is 1.0

-Trigger_Warhead:  Name of warhead type used when destroying the mine. Default is "Death"

Notes: 

This script should be attached to a vehicle. It uses the SightRange of the vehicle as the proximity range for triggering the mine.

Update:

This script has been modified to use newer script commands that should allow for more accurate detection of target types.

**********************************************************************************************	

Script Name: "RA_Mine_Manager"


Description:

This script approves all mine placements, keeps track of the amount of mines in use, and ensures that Soviets can only place soviet mines and Allies can only place allied mines even if a player steals the other team's mine layer.

Script Parameters:

-Allied_Mine: Preset name for the Allied team's land mine. 

-Soviet_Mine: Preset name for the Soviet team's land mine. 

-Mine_Reload: Preset name for the powerup used to refund one 'shot' in the Mine Layer if a mine laying request is denied.

-BoneName: This is the name of the bone on the Mine Layer to create a mine at. Defalut is "mine"

-Mine_Limit: This is the team mine limit. It applies seperately to each team. Default is 30

Notes: 

This script should be attached to an invisible object, such as a Daves Arrow. When placed on a map, its ID must match (or be changed to match) the Mine_Manager_ID param on the RA_Mine_Layer and RA_Mine scripts.

Update: 

A bug associated with the team mine limits has been fixed. Previously, a bug in the script caused the Allied team mine count to never decrease after a mine detonated.

**********************************************************************************************	

Script Name: "RA_ObjectDeath_OnCreate"


Description:

This script is designed to create a new object at the location of another object's creation, then kill the script generated object to make use of its KilledExplosion setting to place a sound on the map.

Script Parameters:

-Object: The preset name of the object to be created and destroyed. Default is "null"

Notes: 

The script should be attached to an object, such as a soldier, so that when it is created the object in the script parameter is created and destroyed. If used for its intended purpose (creating sounds) the script created object should be have no model, and the explosion used for its KilledExplosion setting should have no model and be damageless.

**********************************************************************************************	

Script Name: "RA_Naval_Yard"


Description: 

This script tracks the purchasing of units from an object using the script RA_Naval_PT. It approves requests for units, keeps track of it's team's unit count, and destroys objects that request new units after ther building it is attached to is destroyed.

Script Parameters:

-Limit: The team's limit for the units tracked by this script. Default is 8.

-Disable_Time: This is the amount of time, after a unit is bought, that no unit may be purchased. This is to allow a newly purchased vehicle time to get out of the construction zone. Time is measured in seconds. Default is 5.0.

Notes: 

Currently there is nothing to actually make the unit leave the zone, a player has to come and remove it. Because of this, Disable time should be set to an amount that gives a player a decent amount of time to reach the vehicle they purchased.

**********************************************************************************************	

Script Name: "RA_Naval_PT"


Description: 

This script acts as the new 'purchase terminal'. It checks when a player enters the object it is attached to, requests purchase of a unit specified in the parameters, and carries out the unit construction when told to. It basically acts as the intermediary between all the other naval scripts, which unfortunately gives it the most parameters to fill out.

Script Parameters:

-Preset_Name: The preset name of the unit to be purchased from this terminal. Defeault is 'blank'

-Cost: How much the unit costs. Default is 0.0

-Team: which team this Terminal belongs to. 0 = Soviet, 1 = Allied. Default is 0

-ControllerID: The instance ID of the building controller this terminal should contact for purchase requests (Building controller would have script RA_Naval_Yard on it). Default is 0.

-ZoneID: The instance ID of the script zone to construct new units at. (Script zone would have the script RA_Naval_Construction_Zone on it) Default is 0.

-TestID: This parameter is used during gameplay to carry information to recreated terminals. Leave it at its default of 0 otherwise maps using this script may crash.

Notes: 

This script should be setup on an object that has vehicle transitions (players can get in and out of it). When entered the terminal destroys itself and creates a new one in its place. This 'kicks out' the player so they don't remain in a terminal. 

**********************************************************************************************	

Script Name: "RA_Naval_PT_Pokable"


Description: 

Basically a clone of RA_Naval_PT, this script also acts as the new 'purchase terminal'. It checks when a player pokes the object it is attached to, requests purchase of a unit specified in the parameters, and carries out the unit construction when told to. It basically acts as the intermediary between all the other naval scripts, which unfortunately gives it the most parameters to fill out.

Script Parameters:

-Preset_Name: The preset name of the unit to be purchased from this terminal. Defeault is 'blank'

-Cost: How much the unit costs. Default is 0.0

-Team: The team this Terminal belongs to. 0 = Soviet, 1 = Allied. Default is 0

-ControllerID: The instance ID of the building controller this terminal should contact for purchase requests (Building controller would have script RA_Naval_Yard on it). Default is 0.

-ZoneID: The instance ID of the script zone to construct new units at. (Script zone would have the script RA_Naval_Construction_Zone on it) Default is 0.

Notes: 

This script uses the 'poked' function instead of customs sent by entering a vehicle like its predecessor. It should be attached to an object that is not a 'vehicle' so it can properly use this function. 'Poke' does not work in multiplayer with a normal Renegade installation.
If both the server and the client have scripts.dll 1.9/bhs.dll installed however, it will work.

**********************************************************************************************	

Script Name: "RA_Naval_Unit"


Description:

This script is attached during gameplay to units purchased through the RA_Naval_PT script. It allows the unit to inform the controller of its demise.

Script Parameters:

-ControllerID: The ID of a building controller to send a custom to when this script is destroyed (unit it is attached to dies)

Notes: 

This script shouldn't be attached to anything. The script RA_Naval_PT will attach it automatically to units it contructs as well as fill in the script parameters.

**********************************************************************************************	

Script Name: "RA_Naval_Zone"


Description: 

This script is designed to create an explosion at its location when it receives the appropriate message from the script RA_Naval_PT.

Script Parameter:

-Explosion: This is the preset name of the explosion to use. Default is 'blank'

Notes: 

This script, although originally designed for a script zone, should now be attached to a Dave's Arrow to get full functionality. It will trigger its explosion just prior to a unit being constructed at its location, so the explosion should be capable of killing anything in the way. An example of this would be the explosion having a Death warhead, do 1000 damage in a 4 meter radius, and the damage not being scaled. If attached to an object, like a Dave's arrow, instead of a script zone, the vehicle will be created facing the same direction of as the Dave's Arrow. 

**********************************************************************************************	

Script Name: "RA_Damaged_Credits"


Description:

This script is designed to award credits to the player who damages the object this script is attached to. It does so based on the Damage dealt divided by the script parameter Divisor. It has also been updated to award credits to players that heal the object this script is attached to.

Script Parameter:

-Divisor: This is the number that the damage is divided by to determine the amount of credits to award. Default is 4.0.

Notes: 

This script can be attached to a soldier, vehicle, or any other object you want to get extra credits from when its damaged.

**********************************************************************************************	

Script Name: "RA_GameStart_Detector"


Description: 

This script is designed to detect when a game actually begins on an FDS. It has been shown that an FDS will start running scripts while still in a "Gameplay Pending" state before players have loaded into the game. This can cause problems for some scripts that run off timers or do something when first created. This script is designed to send a message when normal gameplay starts. Other scripts can be setup to listen for this message, so they know when the game has started.

Script Parameters:

-Interval: This is the delay in seconds between times that the script will check if gameplay has started, so as not to risk overburdening a server. Default is 5.0.

-ReceiverID: ID of the object to send the message to. Default is 0

-Message: This is the message to send. Default is 0

-Parameter: This is an extra parameter that can be sent with the message. Default is 0

Notes: 

This script should be attached to a Dave's Arrow

**********************************************************************************************	

Script Name: "RA_DriverDeath"


Description: 

Half a second after being attached to an object, this script will kill whatever it was attached to.

Script Parameters:

None

Notes: 

This script should be attached to an object by other scripts during gameplay to kill that object after a time delay.

**********************************************************************************************	

Script Name: "RA_Conyard_Controller"


Description: 

This script is largely a clone of TDA_Conyard_Controller. It has been modified so that it will activate the repair scripts at the beginning of a game, and then disable them when the controller it is attached to dies. This allows the repair script "RA_Conyard_Repair" to function from the preset of an object instead of being attached on a per map basis. This script, however, does still need to be attached on a per map basis so it knows all the IDs it needs to.

Script Parameters:

-Building1_ID: 1st building to enable/disable

-Building2_ID: 2nd building to enable/disable

-Building3_ID: 3rd building to enable/disable

-Building4_ID: 4th building to enable/disable

-Building5_ID: 5th building to enable/disable

-Building6_ID: 6th building to enable/disable

-Building7_ID: 7th building to enable/disable

-Building8_ID: 8th building to enable/disable

-Building9_ID: 9th building to enable/disable

-Building10_ID: 10th building to enable/disable

Notes: 

This script should be attached to a building controller. Multiple copies of this script can be attached to a single controller if more parameters are needed.

**********************************************************************************************	

Script Name: "RA_Conyard_Repair"


Description: 

Primarily a clone of TDA_Conyard_Repair, but designed to work with RA_Conyard Controller so this script can function from a preset.

Script Parameter: 

-Repair_Frequency: How often the buiding receives 1 health point. Time is in seconds. Default is 1.0

Notes: 

This script should be attached to the preset of a building controller. If activated by RA_Conyard_Controller, it will continue to repair the builing until disabled. 

Update:

This script now applies 0 damage to the object it is attached to when the added health crosses the 25%, 50%, and 75% marks. This is to meant to trigger the game engine to switch damage states for building aggregates. 

**********************************************************************************************	

Script Name: "RA_ObjectDeath_OnDeath"


Description:

When the object this script is attached to is killed, this scripts creates and kills an object at that location. The purpose is to avoid having to use the KilledExplosion setting on a soldier, which may not work correctly in multiplayer games.

Script Parameter:

-Preset: The preset name of the object to create and then kill.

Notes: 

The script uses 100 damage and a BlamoKiller warhead to kill the object it creates. With the standard Renegade armor.ini, BlamoKiller magnifies damage a thousand times against all armor types.

**********************************************************************************************	

Script Name: "RA_Demolition_Truck_Improved"


Description:

An improved version of the original Demolition Truck script for Renegade Alert. This script can work entirely from presets, unlike older versions. All script parameters are defaulted to Renegade Alert requirements.

Script Parameters: 

-Explosion: The vehicle/infantry damaging explosion to use when the vehicle detonates. This part of the script code was written before there were fixes for Explosion script commands. This explosion was intended to be invisible, with the KilledExplosion of the vehicle being intended for use as the visual, but harmless effect. It can still be used like this, but is no longer necesary as script generated explosions will be visible to the client. Default is Explosion_Invis_Nuke

-Warhead: The warhead to use when damaging buildings. Default is Nuke

-DamageStrength: The base strength of the explosion, this will be scaled down the further away the target building is. It is recommended that it matches the damage strength of the explosion used in the Explosion parameter. Default is 480.0

-DamageRadius: Maximum distance, in meters, that will be effected by the damage. Default is 120.0

Notes:

This script triggers 'the bomb' by detecting when the driver has damaged the vehicle. When triggered, the script uses 100 damage and a Death warhead to kill the truck. When killed, if the truck has a driver it attaches the script RA_Driver_Death to the driver. 

**********************************************************************************************	

Script Name: "RA_CTF_Zone"


Description:

A copy of Dante's TDA_CTF_Zone, that replaces the ID requirement with new code that can find building controllers on it own. It also disables the capture sounds code, which isn't currently being used for Renegade Alert.

Script Parameters:

-Team_ID: This is what team the Script Zone works for. (0=Soviet, 1=Allied). Default is 0.

-Max_Capture: This is the number of captures that will end the game. Default is 5.

-Flag_Preset_Name: This is the flag preset name.

Notes:

This script is used the same way TDA_CTF_Zone was used.

**********************************************************************************************	

Script Name: "RA_MAD_Tank_Improved"


Description:

An improved version of the original MAD Tank script for Renegade Alert. This script is 'self contained', requiring no other scripts being setup up and can work entirely from presets, unlike older versions. All script parameters are defaulted to Renegade Alert requirements.

Script Parameters:

-Attach_Model: This is the name of the .w3d model that will be attached to the model when deployed. Do not include the .w3d in the name. Default is pistons

-Deployed_Animation: This is the full animation name for the deployed animation. The tank will detonate when this animation completes. Default is V_SO_MADTANK.ANIM_MADTANK

-Sonic_Preset: This is the preset name of the object that will be created and destroyed when the tank detonates. The KilledExplosion of this object should be both the visual effect and the infantry/vehicle damaging part of the explosion. Default is Sonic_Box

-Percentage: This is the percentage of a building's maximum health that the tank should damage with 1.0 being 100% damage. Damage is not scaled down by distance, so even at maximum range the damage is the same as being right next to the blast. Default is 0.34.

-Warhead: This is the warhead to damage buildings with. Default is Sonic.

-DamageRadius: Maximum distance, in meters, that will be effected by the damage. Default is 250.0

-Announcement: A 2D sound preset to play when the tank deploys. Default is MAD_Tank_Deployed

-Bone: Bone in the MAD Tank that will be used when attaching the object specified in Attach_Model to the MAD Tank. Default is ROOTTRANSFORM.

Notes (updated):

This script deploys the tank when it detects the driver has damaged the vehicle. It will kick the driver, and any passengers, out of the tank. The tank's transitions will then be disabled and the tank's team type will be changed to match the driver's instead of staying unteamed like a normal empty vehicle. The deploy sound does not work with a normal installation of Renegade. If both the server and the client have an up to date scripts.dll and bhs.dll installed, it will work.

**********************************************************************************************	

Script Name: "RA_Conyard_Controller_Improved"


Description:

A replacement for RA_Conyard_Controller that can work from presets instead of needing to be setup on a per map basis. It can find all the team's building controller's on its own but require the preset names of 'vehicle type' base defenses, such as AA Guns or Pillboxes. This script works in tandem with RA_Conyard_Repair, by sending messgaess to all building controllers and listed presets on its team. It does not send a message to itself.

Script Parameters:

-Team: The team this script works for. Default is 0

-Preset1: The preset name of an object to repair. Default is blank

-Preset2: The preset name of an object to repair. Default is blank

-Preset3: The preset name of an object to repair. Default is blank

-Preset4: The preset name of an object to repair. Default is blank

-Preset5: The preset name of an object to repair. Default is blank

Notes:

If more than 5 presets are required for extra objects, it is not recommended that another instance of this script be attached to the controller since it would send the message to all building controllers twice, possibly causing issues. It would be better to use the original RA_Conyard_Controller script to fill in the extra object IDs on a per map basis.

**********************************************************************************************	

Script Name: "RA_Building_DeathSound"


Description:

This script plays a 3D sound preset when the object it's attached to reaches certain health percentages. Script parameters are defaulted to Renegade Alert requirements.

Script Parameters:

-HalfSound: Sound preset to play when at 50% health. Default is ExploSound_Bldg_Half

-DeadSound: Sound preset to play when at 0% health. Default is ExploSound_Bldg_Dead

Notes: 

This script won't function in multiplayer with a normal Renegade installation.
If both the server and the client have scripts.dll 1.9/bhs.dll installed however, it will work.

**********************************************************************************************

Script Name: "RA_Base_Defense_Simple"


Description:

A simplified version of a prototype script for a threat assessing Base Defense. It is capable of basic target prioritizing and can adjust its aim when firing at infantry. The script can be set to prioritize infantry, vehicles, or both equally. It will also rate targets based on remaining health, range, and if the target is a player or AI. Players are considered a higher priority than AI. The lower a target's health, the more likely it will be prioritized. The closer a unit is, the higher its priority is.     

Script Parameters:

-MinRange: The minimum range to attack a target. Targets closer than this will be ignored. Default is 0.0.

-MaxRange: The maximum range to attack a target. Targets further than this will be ignored. Default is 100.0.

-AntiAir: Sets if the script targets aircraft. 1 = Yes, 0 = No. Default is 0.

-AntiGround: Sets if the script targets ground units. 1 = Yes, 0 = No. Default is 1.

-AdjustAim: Sets if the script will adjust its aim for infantry. 1 = Yes, 0 = No. Default is 0.

-TargetMode: Sets the script's target priority. 0 = Vehicles before Infantry, 1 = Infantry before Vehicles. Anything else will cause script to ignore infantry/vehicle distinctions and rely solely on range/health/player for prioritizing. Default is 0.

Notes:

Adjusting aim for infantry is useful for base defenses that fire "slow" projectiles, like a cannon. Infantry can typically sidestep these kinds of attacks. The adjusted aim targets the ground the soldier is standing on instead of the soldier. This causes the projectile to explode right next to a soldier instead of traveling another 20 or so meters before impacting the ground. For base defenses that fire "fast" projectiles or non-explosive projectiles, like a machine gun, do not adjust aim for soldiers, as the adjustment would likely cause the projectile to miss completely.

**********************************************************************************************

Script Name: "RA_Base_Defense_Powered"


Description:

A simplified version of a prototype script for a threat assessing Base Defense. It is capable of basic target prioritizing and can adjust its aim when firing at infantry. The script can be set to prioritize infantry, vehicles, or both equally. It will also rate targets based on remaining health, range, and if the target is a player or AI. Players are considered a higher priority than AI. The lower a target's health, the more likely it will be prioritized. The closer a unit is, the higher its priority is.
This one stops attacking if the power goes low.

Script Parameters:

-MinRange: The minimum range to attack a target. Targets closer than this will be ignored. Default is 0.0.

-MaxRange: The maximum range to attack a target. Targets further than this will be ignored. Default is 100.0.

-AntiAir: Sets if the script targets aircraft. 1 = Yes, 0 = No. Default is 0.

-AntiGround: Sets if the script targets ground units. 1 = Yes, 0 = No. Default is 1.

-AdjustAim: Sets if the script will adjust its aim for infantry. 1 = Yes, 0 = No. Default is 0.

-TargetMode: Sets the script's target priority. 0 = Vehicles before Infantry, 1 = Infantry before Vehicles. Anything else will cause script to ignore infantry/vehicle distinctions and rely solely on range/health/player for prioritizing. Default is 0.

Notes:

Adjusting aim for infantry is useful for base defenses that fire "slow" projectiles, like a cannon. Infantry can typically sidestep these kinds of attacks. The adjusted aim targets the ground the soldier is standing on instead of the soldier. This causes the projectile to explode right next to a soldier instead of traveling another 20 or so meters before impacting the ground. For base defenses that fire "fast" projectiles or non-explosive projectiles, like a machine gun, do not adjust aim for soldiers, as the adjustment would likely cause the projectile to miss completely.

**********************************************************************************************

Script Name: "RA_Missile_Silo"


Description: 

Script to control a missile silo capable of animating a launch when a beacon is placed. This script is not designed to be used directly by a level editor. It is attached to the proper building controller by the script RA_Missile_Manager. RA_Missile_Manager will also set the parameters correctly for the building. This script will then create an object called silo_missile at its position and rotate it to match the ZRotation parameter. It will then attach an object to act as a terminal to a bone in the silo_missile model, and attach the script RA_ABomb_Terminal to that object.

It will receive customs from RA_Missile_Manager as to when to create the launching animation.

Script Parameters:

-Team: The team this silo belongs to. 0 = Soviet, 1 = Allied. Default is 0.

-ZRotation: The Z-axis Rotation of the building controller this script is attached to. Default is 0.0.

Notes: 

This script makes extensive use of presets from Renegade Alert. It is recommended that this script only be used in conjunction with Renegade Alert's objects.ddb. It also is based around the needs required prior to some of the more recent engine upgrades provided by either the renalert.dll or bhs.dll, and so may use methods not required any longer. All in all, don't use this script unless you really know what you are doing.

**********************************************************************************************

Script Name: "RA_Missile_Manager" 


Description:

Main controlling script for A-bomb Missile Silos in Renegade Alert. It should be attached to a Dave's Arrow.

Script Parameters:

-AlliedSiloID: Instance ID of the Allied Missile Silo building controller. Default is 0.

-Allied_ZRotation: Z-Axis Rotation of the Allied Missile Silo building controller. Default is 0.0.

-SovietSiloID: Instance ID of the Soviet Missile Silo building controller. Default is 0.

-Soviet_ZRotation: Z-Axis Rotation of the Soviet Missile Silo building controller. Default is 0.0

Notes: 

This script needs to have an instance ID that the script RA_Abomb_Beacon can have listed in its parameters. By default, that ID is 100100. Like with all the Missile scripts, this script is intended to be used with the Renegade Alert objects.ddb and should not be used without a full understanding of its inner workings.

**********************************************************************************************

Script Name: "RA_Silo_Animation"


Description: 

Script used by the script RA_Missile_Silo to control the launching animation. It should not be used independantly by level editors. RA_Missile_Silo will attach it when needed.

Script Parameters:

-ControllerID: The instance ID of the silo building controller.

-Mode: Outdated variable, still used in code. Only responds properly when set to 1.

-Animation: Name of animation to play.

Note:

This script makes use of Renegade Alert presets and is not designed to be used from Level Edit at all.

**********************************************************************************************

Script Name: "RA_ABomb_Terminal"

Description:

Script to control pokable terminal that grants A-Bomb flares. It will allow one flare to be given after a five minute timer and will start another timer only after the flare is taken.

Script Parameters:

Team: ID of team this terminal belongs to. 0 = Soviet, 1 = Allied.

ControllerID: Instance ID of the silo building controller.

Powerup: Preset name for the powerup that gives the flare.

Notes: 

This script is meant only to be used by the script RA_Missle_Silo, which will attach it to the object it creates for the terminal. 

**********************************************************************************************

Script Name: "RA_ABomb_Beacon"

Description:

Script to send custom from a placed beacon. It only sends a custom when it is created.

Script Parameter:

-ManagerID: Instance ID of object with RA_Missile_manager script. Default is 100100

Notes:

This script sends the custom 269269 to the object with the ID listed in its parameters.


**********************************************************************************************

Script Name: "RA_CanyonRiver_Weather"


Description:

Script designed to control the weather in the level RA_CanyonRiver. Script functions end once storm is created.

Script Parameters:

-Message: This is the custom that the script will wait to receive before activating its timers. Default is 754754.

-Storm_Delay: Time in seconds before storm will begin. Default is 900.0.

-Storm_Transition: Time in seconds after storm starts before it reaches full intensity. Default is 300.0.

-Storm_Heading: Equivalent of the 'Heading' parameters in Level Edit weather settings. Default is 60.0.

-Fog_Start: Distance from player that fog begins. Default is 100.0.

-Fog_End: Distance from player that fog becomes too thick to see past. Default is 150.0.

-Rain_Density: Equaivalent to 'Density' setting for rain in Level Edit. Default is 1.0.

-Lightning_Intensity: Equivalent to 'Intensity' setting for Lightning in Level Edit: Default is 1.0.

-Lightning_Start: Equivalent to 'Start' setting for Lightning in Level Edit. Default is 0.2.

-Lightning_End: Equivalent to 'End' setting for Lightning in Level Edit. Default is 0.8.

-Lightning_Distribution: Equivalent to 'Distibution' setting for Lightning in Level Edit. Default is 0.5.

-Wind_Strength: Equivalent to 'Strength' setting for Wind in Level Edit. Default is 10.0.

-Wind_Variability: Equivalent to 'Variability' setting for Wind in Level Edit. Default is 0.9.


Notes: 

This script should be attached to a Dave's Arrow somewhere on the map. Due to script timers running while servers are in a 'gameplay pending' state, this script won't begin its timers until it receives the initial message. Weather effects will tran evenly sition from weather settings present in the level to the settings listed in the script over a period of time as specified by the parameter "Storm_Transition".

**********************************************************************************************

Script Name: "RA_Metro_Weather" 


Description:

Script designed to control the weather in the level RA_Metro. Script continues to change storm intensity over course of gameplay.

Script Parameter:

-Time: Time in seconds it takes to fully change storm intensity. Default is 300.0.

Notes: 

This script should be attached to a Dave's Arrow somewhere in the level. Storm effects are changed based on hard coded numbers. Storm will transition into its effect, and then after 60 seconds the script will change the effect back. Every time the storm changes, the effect will remain in full for only 1 minute before changing again. 


**********************************************************************************************

Script Name: "RA_Underwater_OnCustom"


Description: 

Script designed to change screen color and opacity when it receives specific customs. 

Script Parameters:

Dive_Message: Custom that will trigger underwater effects. Default is 789789.

Surface_Message: Custom that will remove underwater effects. Default is 987987.

Disable_Message: Custom that will disable underwater effects. Default is 852852.

Enable_Message: Custom that will re-enable underwater effects. Default is 258258.

Dive_Red: Determines if red coloring is used in underwater effect. Default is 0.0

Dive_Green: Determines if green coloring will be used in underwater effect. Default is 0.0.

Dive_Blue: Determines if blue coloring will be used in underwater effect. Default is 1.0.

Dive_Opacity: Determines opacity of underwater effect. Variable can range from 0 to 250. Default is 150.0.

Notes:

This script should be attached to all infantry. It is designed to kill a soldier who goes underwater while not in a vehicle. If a player can enter an area that they may touch an underwater zone when they shouldn't be underwater, another script zone needs ot be used to send the 'disable' message to turn off underwater effects, as well as the 'drowning' death triggered by the script.

**********************************************************************************************

Script Name: "RA_Underwater_Zone"


Description: 

Script designed to send a custom when an object enters or exits the script zone this script is attached to.

Script Parameters:

-Enter_Message: Custom to send to an object that enters the zone. Default is 0.

-Exit_Message: Custom to send to an object that exits the zone. Default is 0.

Notes:

This script needs to be attached to a script zone. It can act as both a 'underwater zone' and a 'disable zone' based on the custom listed in its parameters.

**********************************************************************************************

Script Name: "RA_Submarine"


Description:

Script designed to destroy abandoned submarines that sink into the water. It also can create a barrier in front of the muzzle bones on the submarine while underwater. This is so weapons can only be used properly when surfaced.

Script Parameter:

-Disable: Determines if barrier will be setup on muzzle bones when submarine dives. 1 = Yes, 0 = No. Default is 0.

Notes:

The barrier created is an object in the Renegade Alert presets. This setting should not be used outside of Renegade Alert.

**********************************************************************************************

Script Name: "RA_Vehicle"


Description:

Script that goes on vehicles to track when a player is inside the vehicle. It will remember the ID and send customs to the players getting in and out. These customs will be picked up by the script RA_Underwater_OnCustom, so it knows when infantry is inside a vehicle or not.

No Script Parameters

Notes:

This script goes on vehicles. It can remember IDs of up to 6 passengers at once.

**********************************************************************************************

Script Name: "RA_Vehicle_Regen"

Description:

Script designed to slowly regenerate a vehicle's health up to a percentage of maximum health/armor. The script adds the maximum health and maximum armor amounts together to determine 100% and then begins healing if the vehicle's combined health/armor drops below a specificed percentage. Health refills first, then armor if repairing is still needed.

Parameters:

Percentage: Percentage of maximum health/armor to heal. Default is 0.5
Heal: Amount to heal per repair tick. Default is 1.0
Time: Time in seconds between repair ticks. Default is 1.0
TimerID: ID number for the timer. Default is 87654

Notes:

Percentage can range between 0.0 and 1.0 with 0 being 0% and 1 being 100%. Although this script was meant for vehicles, it should work well on infantry too.

**********************************************************************************************

Script Name: "RA_Thief"

Description:

A new(er) script for the allied thief. Upon entering a script zone using RA_Credit_Theft_Zone, this script steals a specified percentage of credits from all players on the team that owned the script zone and redistributes those credits evenly to the thief's entire team.

Parameters:

Enter_Message: Custom to be sent by RA_Credit_Theft_Zone upon zone entry. Default is 357753
Percentage: Percentage of credits to steal. Default is 0.5

Notes:

This script is removed from the thief upon stealing credits, so it can only be used once. To "reload" the thief, there needs to be a script zone somewhere that reattaches this script to the thief if he doesn't already have it.

**********************************************************************************************

Script Name: "RA_Thief_Improved"

Description:

A new(er) script for the allied thief. Upon entering a script zone using RA_Credit_Theft_Zone, this script steals a specified percentage of credits from all players on the team that owned the script zone and redistributes those credits evenly to the thief's entire team. Also, a 2D sound is played for the team of the thief.

Parameters:

Enter_Message: Custom to be sent by RA_Credit_Theft_Zone upon zone entry. Default is 357753
Percentage: Percentage of credits to steal. Default is 0.5
Sound: A 2D sound to play for the team of the thief

Notes:

This script is removed from the thief upon stealing credits, so it can only be used once. To "reload" the thief, there needs to be a script zone somewhere that reattaches this script to the thief if he doesn't already have it.

**********************************************************************************************

Script Name: "RA_Credit_Theft_Zone"

Description:

Script for a script zone. Checks all infantry who enter it and sends a custom to any who have the script RA_Thief.

Parameters:

Enter_Message: Message to send to infantry with RA_Thief. Defaut is 357753
Team: Team who owns the zone. 0 = Soviet, 1 = Allied. Default is 0

Notes:

This script will send a message to any infantry who enters it's zone with the script RA_Thief attached. Should the Thief be from the same team as the zone, the script RA_Thief will ignore the message.

**********************************************************************************************

Script Name: "RA_DestroyNearest_OnDeath"

Description:

This script destroys the nearest instance of a specified preset when the object it is attached to is killed.

Parameters:

Preset_Name: Name of the preset to destroy.

Notes:

This script is meant to remove script zones such as credit stealing zones or Ore Deposit zones when their cooresponding refinery is killed. 

**********************************************************************************************

Script Name: "RA_Ore_Truck"

Description:

Script for Ore Trucks. It calculates the value of each individual scoop of ore it harvests by dividing the field's value by the number of scoops it can hold. The script plays animations for both harvesting and delivering.

Parameters:

Field_Enter: Message sent by ore field when truck enters the zone. Default is 456654
Field_Exit: Message sent by Ore field when truck exits the zone. Default is 75321
Deposit_Enter: Message sent by deposit zone when truck enters. Default is 321654
Max_Capacity: Maximum number of "scoops" this script can hold. Default is 7
Harvest_Anim: Name of animation to play when harvesting.
Deliver_Anim: Name of animation to play when dumping ore.

Notes:

While harvesting, it must complete the harvesting animation to count a 'scoop'. The time it takes to harvest a field will therefore be the length of its harvest animation times the max capacity. When filled to capacity, the script will no longer play harvesting animations until it delivers the load. When delivering ore, it must complete its deliver animation to distribute credits to players. 

**********************************************************************************************

Script Name: "RA_Ore_Field"

Description:

Script for Ore Field script zones. It only sends messages to objects entering/exiting with the script RA_Ore_Truck.

Parameters:

Field_Value: The total credit value a maximum load from this field will produce. Default is 700
Enter: Mesage to send to an entering object. Default is 456654
Exit: Mesage to send to an exiting object. Default is 75321

Notes:

When an ore truck enters the zone this script is attached to, the enter message will also send the field value so the truck can calculate how much money it receives per scoop. A field with a value of 700 will provide a truck with 100 credits per scoop, assuming the truck is using the default 7 scoop capacity.

**********************************************************************************************

Script Name: "RA_Ore_Delivery_Zone"

Description:

Script for Ore Delivery script zones. Ore Trucks enterign the attached zone will be sent a message instructing them to deliver their ore load.

Parameters:

Team: The team this zone works for. Soviets = 0, Allies = 1. Default is 0
Enter: Message to send to Ore Trucks that enter the zone. Default is 321654

Notes:

This script will only send a message to an object using the script RA_Ore_Truck that matches the team specified in its parameters.


**********************************************************************************************

Script Name: "RA_Vehicle_Team_Set"

Description:

Script to make an abandoned vehicle temporarily remain on the team of the exiting driver. This script has no parameters.

Notes:

When a driver exits the vehicle, this script will set the vehicle's team to match the driver's team. Then it will attach the script RA_Vehicle_Timer. If someone enters the vehicle, the script RA_Vehicle_Timer will be removed if it exists.

**********************************************************************************************

Script Name: "RA_Vehicle_Timer"

Description:

Script to make a vehicle switch to unteamed after 20 seconds. This script has no parameters.

Notes:

This script is meant only to be used by the script RA_Vehicle_Team_Set. After the 20 second timer expires, the script will change the attached object's team type to unteamed (-2) and the remove itself from the object.

**********************************************************************************************

Script Name: "RA_Visible_Driver"

Description:

A copy of JFW_Per_Preset_Visible_Person_In_Vechicle that will also remove the driver model if the attached vehicle is stealthed.

Parameters:

BoneName: Bone to attach drievr model to. Default is seat0
Message: Message sent to driver. Default is 471174.

Notes:

The driver model will be removed if the vehicle is capable of being invisible. If the vehicle is presently visible, due to attacking for example, the model will not be replaced. Only when the vehicle completely loses the ability to remain invisible will the driver model reappear.

**********************************************************************************************

Script Name: "RA_Vision_Control"

Description:

Script for infantry that controls underwater and gap fog visual effects. Replaces the script RA_Underwater_OnCustom.

Parameters:

FogController: Preset name of the object with RA_Fog_Level_Settings attached. Default is RA_Game_Manager

Notes:

This script controls visual effects for underwater and fog control, as well as killing infanty who drown. Underwater effects override the gap effects. This script can not properly reset fog effects when the gap generator induced fog is removed, so it must contact an object with the script RA_Fog_Level_Settings in order to get fog settings back to the level's parameters.

**********************************************************************************************

Script Name: "RA_Fog_Level_Settings"

Description:

Script to reset a player's fog settings to a level's standard.

Parameters:

FogEnable: Is fog enabled on this level. 0 = no, 1 = yes. Default is 1.
FogMin: Fog Start distance in background settings. Default is 200.0
FogMax: Fog End distance in background settings. Default is 300.0

Notes:

Script parameters should be set to match the settings used for fog in Level Edit's Background settings.

**********************************************************************************************

Script Name: "RA_Global_Gap_Controller"

Description:

Master control script for tracking all gap generators and calculating who gets effected by what.

Parameters:

Update_Delay: Time in seconds between updates. Default is 2.0
Timer_Number: Id for timer used by this script. Default is 687

Notes:

This script should be attached to a Daves Arrow type object. That object needs to be named the same as the Gap_Controller parameter used on generator scripts. Although Update_Delay defauts to 2 seconds, 0.5 seconds works better. This script tracks every gap generator present on a map. At every update it will check the position of all generators against all players, vehicles and infantry. Anyone within range of a friendly active generator will turn invisible. Anyone in range of an active enemy generator will have a darkened screen with thick fog appear. Anyone outside range of all generators will have both effects deactivated.

**********************************************************************************************

Script Name: "RA_Gap_Generator_Building"

Description:

Script for Gap Generators. This script is meant for building controllers.

Parameters:

Timer_Number: ID for timer this script uses. Default is 876
Stealth_Range: The range of this generator in meters. Default is 50
Gap_Controller: Preset name of the object with RA_Global_Gap_Controller script. Default is RA_Game_Manager
Team: Team this generator works for. 0 = Soviet, 1 = Allies. Default is 1
ShroudPreset: Preset Name for gap cloud preset.

Notes:

If the object this script is attached to dies, the generator will be removed from the list of gap generators. If it loses power, it will become inactive.

**********************************************************************************************

Script Name: "RA_Gap_Generator_Vehicle"

Description:

Script for Gap Generators. This script is meant for vehicles.

Parameters:

Stealth_Range: The range of this generator in meters. Default is 50
Gap_Controller: Preset name of the object with RA_Global_Gap_Controller script. Default is RA_Game_Manager
ShroudPreset: Preset Name for gap cloud preset.

Notes:

Unoccupied vehicles are considered inactive. Vehicles will act in favor of their driver's team, cloaking friendly units and fogging enemy units.

**********************************************************************************************

Script Name: "RA_Gap_Generator_Ignored"

Description:

Script for units that should not be effected by gap generators.

Notes:

This script doesn't do anything on its own. The function controlling gap generator effects is designed to ignore any object with this script attached to it. This script should be used for vehicles that are stealth on their own, such as stealth tanks, so that their stealth ability is not incorrectly removed by gap generators. 

**********************************************************************************************

Script Name: "RA_Missile_Beacon"

Description:

Script for a beacon to send a custom to an object based on the object's preset name

Parameters:

SiloPresetName: Preset name for the object the beacon will send a custom to.

Notes:

This script should be attached to a beacon preset. It will send the custom as soon as the beacon is created. The custom is hardcoded as 269269.

**********************************************************************************************

Script Name: "RA_Beacon_Terminal"

Description:

Script to control a purchase terminal that grants a beacon to a player.

Parameters: 

BeaconPowerup: Preset name of the Powerup with the beacon
BeaconReadySound: Preset name of a 2D sound that will play for all team members when a beacon becomes available
BeaconTakenSound: Preset name of a 2D sound that will play for all team members when a beacon is taken by a player
SiloDisableSound: Preset name of a 2D sound that will play for all team members when this terminal is disabled by a Spy
SpyNotifySound: Preset name of a 2D sound that will play for the team of the Spy who succesfully disables the terminal

Notes:

This script uses the script "RA_Beacon_Timer" to control its 5 minute timer. If you don't want to have the script play one of the specified sounds, putting a 0 for a parameter should be enough to prevent it from being used. It may be safer to create a blank sound preset and use that for the parameter. Either way, do not leave any parameters completely blank. 

The script will reset its timer if poked by an enemy with the script RA_Infantry_Spy. Only one reset is allowed per 'cycle'. After a reset, the terminal will ignore attempted resets until it has completed its countdown.

When notified that its building has been destroyed, this script destroys the object it is attached to.

**********************************************************************************************

Script Name: "RA_Beacon_Timer"

Description:

A script used by "RA_Beacon_Terminal" to control a timer. Timer is hard coded for 5 minutes.

Notes:

This script is intended solely for use by the script RA_Beacon_Terminal and should not be attached to any object manually.

**********************************************************************************************

Script Name: "RA_Missile_Control"

Description:

A Script to receive messages from "RA_Missile_Beacon" and animate a missile launch.

Parameters:

AnimationName: Name of animation to be played on the object this script is attached to.

Notes: 

This script should be attached to the object that needs to be animated in response to a beacon being created. After receiving notice that its building has been destroyed (via the script RA_Missile_Controller), it will destroy any beacon that contacts it instead of playing the animation.

**********************************************************************************************

Script Name: "RA_Missile_Controller"

Description:

Script for the building controller of a missile silo.

Parameters:

SiloPresetName: Preset name for the object with the script RA_Missile_Control
TerminalPresetName" Preset Name for the object with the script RA_Beacon_Terminal

Notes:

This script will send the custom 962962 to the objects specified in its parameters when the building it is attached to is killed.

**********************************************************************************************
Script Name: "RA_Demolition_Truck_Retarded"


Description:

A terrible revision of the improved Demolition Truck script for RA:APB. This script will cause damage to all infantry and vehicles in range, regardless of teams and friendly fire settings.

Script Parameters: 

-Explosion: The vehicle/infantry damaging explosion to use when the vehicle detonates. This explosion will be created twice, and therefore should have half the intended strength of the blast (example, if you want 480 base damage, set the explosion preset to do 240) This part of the script code was written before there were fixes for Explosion script commands. This explosion was intended to be invisible, with the KilledExplosion of the vehicle being intended for use as the visual, but harmless effect. It can still be used like this, but is no longer necesary as script generated explosions will be visible to the client. Default is Explosion_Invis_Nuke

-Warhead: The warhead to use when damaging buildings. Default is Nuke

-DamageStrength: The base strength of the explosion, this will be scaled down the further away the target building is. It is recommended that it matches the damage strength of the explosion used in the Explosion parameter. Default is 480.0

-DamageRadius: Maximum distance, in meters, that will be effected by the damage. Default is 120.0

Notes:

This script triggers 'the bomb' by detecting when the driver has damaged the vehicle. When triggered, the script uses 100 damage and a Death warhead to kill the truck. When killed, if the truck has a driver it attaches the script RA_Driver_Death to the driver. 

This script will create the explosion listed in the parameter 'Explosion' twice. The first will obey the games's friendly fire setting. The second will ignore it and damage all infantry and vehicles in the blast radius. Buildings remain immune to friendly fire, if it has be turned off in the game settings.

**********************************************************************************************

Script Name: "RA_MAD_Tank_Devolved"


Description:

A different version of the MAD Tank script for RA:APB. This script can force friendly fire, even if the option has been turned off for the server. Like its predecessor script, this script only inflicts damage on buildings and vehicles, never infantry.

Script Parameters:

-Attach_Model: This is the name of the .w3d model that will be attached to the model when deployed. Do not include the .w3d in the name. Default is pistons

-Deployed_Animation: This is the full animation name for the deployed animation. The tank will detonate when this animation completes. Default is V_SO_MADTANK.ANIM_MADTANK

-Sonic_Preset: This is the preset name of the object that will be created and destroyed when the tank detonates to cause the visual effect. Default is Sonic_Box.

-EnemyPercentage: This is the percentage of an enemy's maximum health that the tank should damage with 1.0 being 100% damage. Damage is not scaled down by distance, so even at maximum range the damage is the same as being right next to the blast. Default is 0.34.

-FriendPercentage: This is the percentage of a friendly vehicle's maximum health that the tank should damage with 1.0 being 100% damage. Damage is not scaled down by distance, so even at maximum range the damage is the same as being right next to the blast. Default is 0.17.

-ForceFF: This option can override the game's friendly fire setting and force teammates to take damage as well, but converting that damage to neutral. 0 = no forced FF, 1 = forced FF. Default is 1

-Warhead: This is the warhead to damage buildings with. Default is Sonic.

-DamageRadius: Maximum distance, in meters, that will be effected by the damage. Default is 250.0

-Announcement: A 2D sound preset to play when the tank deploys. Default is MAD_Tank_Deployed

-Bone: Bone in the MAD Tank that will be used when attaching the object specified in Attach_Model to the MAD Tank. Default is ROOTTRANSFORM.

Notes:

This script deploys the tank when it detects the driver has damaged the vehicle. It will kick the driver, and any passengers, out of the tank. The tank's transitions will then be disabled and the tank's team type will be changed to match the driver's instead of staying unteamed like a normal empty vehicle. The deploy sound does not work with a normal installation of Renegade. If both the server and the client have an up to date scripts.dll and bhs.dll installed, it will work.

Only the damage caused to friendly units is changed to neutral when ForceFF is set to 1, enemies still take damage from the vehicle's driver and points are still awarded accordingly.

**********************************************************************************************
