/*	Renegade Scripts.dll
	BloomSceneShaderClass
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class BloomSceneShaderClass: public SceneShaderClass
{
protected:
#ifndef SDBEDIT
	bool Validated;
	bool ValidationResult;
	void SetKernals();
	D3DXHANDLE SceneTextureHandle;
	D3DXHANDLE InputTextureHandle;
	D3DXHANDLE BloomScaleHandle;
#endif
public:
	float BloomScale;
	BloomSceneShaderClass();
	~BloomSceneShaderClass();
	void Load(ChunkLoadClass& cload);
	void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	void Initialize();
	bool Validate();
	void Render(SceneShaderRenderInfo *render);
#else
	SceneShaderEditorClass* GetEditor();
#endif
};
#define CHUNK_BLOOMSCENESHADER 1020
#define MC_BLOOMSCENESHADER_BLOOMSCALE 1
