/*	Renegade Scripts.dll
	Base Class for shaders in the Shader Database Editor
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

extern HINSTANCE hInst;

class EditorShaderClass {
public:
	EditorShaderClass()
	{
	}
	virtual ~EditorShaderClass()
	{
	}
	virtual void Load(ChunkLoadClass& cload) = 0;
	virtual void Save(ChunkSaveClass& csave) = 0;
	virtual void Edit(HWND ParentDialog) = 0;
	virtual const char *Get_Name() = 0;
	virtual void Set_Name(const char *name) = 0;
};

class EditorShaderFactory {
public:
	EditorShaderFactory(unsigned int chunkId,const char *name);
	unsigned int Chunk_ID;
	const char *Name;
	virtual EditorShaderClass *Load(ChunkLoadClass& cload) = 0;
	virtual EditorShaderClass *New(const char *name) = 0;
};

template <class T> class EditorShaderRegistrant : public EditorShaderFactory {
	public:
	EditorShaderRegistrant(unsigned int c,const char *name) : EditorShaderFactory(c,name)
	{
	}
	EditorShaderClass *Load(ChunkLoadClass& cload);
	EditorShaderClass *New(const char *name);
};

template <class T> EditorShaderClass *EditorShaderRegistrant<T>::Load(ChunkLoadClass& cload)
{
	EditorShaderClass *shader = new T;
	shader->Load(cload);
	return shader;
}

template <class T> EditorShaderClass *EditorShaderRegistrant<T>::New(const char *name)
{
	EditorShaderClass *shader = new T;
	shader->Set_Name(name);
	return shader;
}
