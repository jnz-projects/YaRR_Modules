/*	Renegade Scripts.dll
	Scripts for vehicles
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class JFW_Aircraft_Fuel : public ScriptImpClass {
	bool entered;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Turret_Spawn_2 : public ScriptImpClass {
	int turID;
	void Created(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Drive_To_Player : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Vechicle_Animation : public ScriptImpClass {
	bool enabled;
	float xpos;
	float ypos;
	float zpos;
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Visible_Person_In_Vechicle : public ScriptImpClass {
	int modelid;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Visible_People_In_Vechicle : public ScriptImpClass {
	int modelid1;
	int modelid2;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Per_Preset_Visible_Person_In_Vechicle : public ScriptImpClass {
	int modelid;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Per_Preset_Visible_People_In_Vechicle : public ScriptImpClass {
	int modelid1;
	int modelid2;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Visible_Person_Settings : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Vechicle_Animation_2 : public ScriptImpClass {
	bool enabled;
	float xpos;
	float ypos;
	float zpos;
	void Created(GameObject *obj);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Vehicle_Model_Team : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Vehicle_Model_Preset : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Vehicle_Model_Weapon : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Vechicle_Animation_Trigger : public ScriptImpClass {
	bool enabled;
	float xpos;
	float ypos;
	float zpos;
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Vehicle_Block_Preset : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Vehicle_Extra : public ScriptImpClass {
	int turID;
	void Created(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Vehicle_Extra_Attach : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
};

class JFW_Vehicle_Extra_2 : public ScriptImpClass {
	int turID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
};

class JFW_Vehicle_Lock : public ScriptImpClass {
	int OwnerID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Vehicle_Effect_Animation : public ScriptImpClass {
	bool enabled;
	int count;
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Created(GameObject *obj);
	void Register_Auto_Save_Variables();
};

class JFW_Vehicle_Regen_2 : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Aircraft_Refill : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Vehicle_Reinforcement : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};
