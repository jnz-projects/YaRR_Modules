/*	Renegade Scripts.dll
	Engine classes and calls for shaders.dll
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#include "engine_common.h"
#include "engine_vector.h"
#include "engine_io.h"
#include "engine_threading.h"
#include "engine_string.h"
#include "engine_math.h"
#include "engine_d3d.h"
#include "engine_def.h"
#include "engine_net.h"
#include "engine_obj.h"
#include "engine_obj2.h"
#include "engine_3dre.h"
#include "engine_3d.h"
#include "engine_tdb.h"
#include "engine_player.h"
#include "engine_weap.h"
#include "engine_game.h"
#include "engine_diagnostics.h"
