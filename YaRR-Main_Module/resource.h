//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by keycfg.rc
//
#define IDI_APP                         5
#define IDD_MAIN                        101
#define IDC_LOGICAL                     1001
#define IDC_PHYSICAL                    1002
#define IDC_ADD                         1003
#define IDC_SAVE                        1004
#define IDC_QUIT                        1005
#define IDC_NEWKEY                      1006
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
