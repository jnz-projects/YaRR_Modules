/*	Renegade Scripts.dll
	Scripts by zunnie - mp-gaming.com/fan-maps.net
	Copyright 2007 Zunnie, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it 
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code 
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "engine.h"
#include "z.h"

void z_Console_Input_Killed::Killed(GameObject *obj,GameObject *shooter)
{
	const char *Input;
	Input = Get_Parameter("Input");
	Console_Input(Get_Parameter("Input"));
}

void z_Console_Input_Killed_2::Killed(GameObject *obj,GameObject *shooter)
{
	const char *Input;
	Input = Get_Parameter("Input");
	Console_Input(Get_Parameter("Input"));
	const char *Input2;
	Input2 = Get_Parameter("Input2");
	Console_Input(Get_Parameter("Input2"));
}

void z_Console_Input_Custom_2::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Console_Input(Get_Parameter("Input"));
		Console_Input(Get_Parameter("Input2"));
	}
}

void z_Play_Three_Cinematics_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	const char *Cinematic1;
	const char *Cinematic2;
	const char *Cinematic3;
	const char *Console_Command;
	Vector3 position1;
	Vector3 position2;
	Vector3 position3;
	GameObject *object1;
	GameObject *object2;
	GameObject *object3;
	float facing1;
	float facing2;
	float facing3;
	msg = Get_Int_Parameter("Message");
	Cinematic1 = Get_Parameter("Cinematic1");
	Cinematic2 = Get_Parameter("Cinematic2");
	Cinematic3 = Get_Parameter("Cinematic3");
	position1 = Get_Vector3_Parameter("Location1");
	position2 = Get_Vector3_Parameter("Location2");
	position3 = Get_Vector3_Parameter("Location3");
	facing1 = Get_Float_Parameter("Facing1");
	facing2 = Get_Float_Parameter("Facing2");
	facing3 = Get_Float_Parameter("Facing3");
	Console_Command = Get_Parameter("Console_Command");
	if (message == msg)
	{
		object1 = Commands->Create_Object("Invisible_Object",position1);
		Commands->Set_Facing(object1,facing1);
		Commands->Attach_Script(object1,"Test_Cinematic",Cinematic1);
		object2 = Commands->Create_Object("Invisible_Object",position2);
		Commands->Set_Facing(object2,facing2);
		Commands->Attach_Script(object2,"Test_Cinematic",Cinematic2);
		object3 = Commands->Create_Object("Invisible_Object",position3);
		Commands->Set_Facing(object3,facing3);
		Commands->Attach_Script(object3,"Test_Cinematic",Cinematic3);
		Console_Input(Get_Parameter("Console_Command"));
	}
}

void z_Play_Cinematic_Console_Input_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	const char *scriptname;
	const char *Console_Command;
	Vector3 position;
	GameObject *object;
	float facing;
	msg = Get_Int_Parameter("Message");
	scriptname = Get_Parameter("Script_Name");
	position = Get_Vector3_Parameter("Location");
	facing = Get_Float_Parameter("Facing");
	Console_Command = Get_Parameter("Console_Command");
	if (message == msg)
	{
		object = Commands->Create_Object("Invisible_Object",position);
		Commands->Set_Facing(object,facing);
		Commands->Attach_Script(object,"Test_Cinematic",scriptname);
		Console_Input(Get_Parameter("Console_Command"));
	}
}

void z_Destroy_Three_Objects_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	int id1;
	int id2;
	int id3;
	GameObject *object;
	msg = Get_Int_Parameter("Message");
	id1 = Get_Int_Parameter("ID1");
	id2 = Get_Int_Parameter("ID2");
	id3 = Get_Int_Parameter("ID3");
	if (msg == message)
	{
		object = Commands->Find_Object(id1);
		Commands->Destroy_Object(object);
		object = Commands->Find_Object(id2);
		Commands->Destroy_Object(object);
		object = Commands->Find_Object(id3);
		Commands->Destroy_Object(object);
	}
}

void z_Enable_Multiple_Spawners_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Commands->Enable_Spawner(Get_Int_Parameter("ID1"),Get_Int_Parameter("Enable"));
		Commands->Enable_Spawner(Get_Int_Parameter("ID2"),Get_Int_Parameter("Enable"));
		Commands->Enable_Spawner(Get_Int_Parameter("ID3"),Get_Int_Parameter("Enable"));
		Commands->Enable_Spawner(Get_Int_Parameter("ID4"),Get_Int_Parameter("Enable"));
		Commands->Enable_Spawner(Get_Int_Parameter("ID5"),Get_Int_Parameter("Enable"));
	}
}

void z_Console_Input_Poke_2::Poked(GameObject *obj,GameObject *poker)
{
	const char *Console_Command1;
	const char *Console_Command2;
	Console_Command1 = Get_Parameter("Console_Command1");
	Console_Command2 = Get_Parameter("Console_Command2");
	{
		Console_Input(Get_Parameter("Console_Command1"));
		Console_Input(Get_Parameter("Console_Command2"));
	}
}


void z_Console_Input_Custom_Delay_1::Custom(GameObject *obj,int message,int timernumber,GameObject *sender)
{
	if (Get_Int_Parameter("Message") == message)
	{
		float timertime,timemin,timemax;
		timemin = Get_Float_Parameter("Time_Min");
		timemax = Get_Float_Parameter("Time_Max");
		timernumber = Get_Int_Parameter("TimerNum");
		timertime = Commands->Get_Random(timemin,timemax);
		if (Get_Float_Parameter("Time_Min"),Get_Float_Parameter("Time_Max") > 0)
			Commands->Start_Timer(obj,this,timertime,timernumber);
	}
}
void z_Console_Input_Custom_Delay_1::Timer_Expired(GameObject *obj,int number)
{
	float timertime,timemin,timemax;
	int timernumber;
	int repeat;
	const char *Input1;
	timemin = Get_Float_Parameter("Time_Min");
	timemax = Get_Float_Parameter("Time_Max");
	timernumber = Get_Int_Parameter("TimerNum");
	repeat = Get_Int_Parameter("Repeat");
	if (number == timernumber)
	{
		Input1 = Get_Parameter("Input1");
		Console_Input(Input1);
		if (repeat == 1)
		{
			timertime = Commands->Get_Random(timemin,timemax);
			Commands->Start_Timer(obj,this,timertime,timernumber);
		}
	}
}

void z_Console_Input_Custom_Delay_2::Custom(GameObject *obj,int message,int timernumber,GameObject *sender)
{
	if (Get_Int_Parameter("Message") == message)
	{
		float timertime,timemin,timemax;
		timemin = Get_Float_Parameter("Time_Min");
		timemax = Get_Float_Parameter("Time_Max");
		timernumber = Get_Int_Parameter("TimerNum");
		timertime = Commands->Get_Random(timemin,timemax);
		if (Get_Float_Parameter("Time_Min"),Get_Float_Parameter("Time_Max") > 0)
			Commands->Start_Timer(obj,this,timertime,timernumber);
	}
}
void z_Console_Input_Custom_Delay_2::Timer_Expired(GameObject *obj,int number)
{
	float timertime,timemin,timemax;
	int timernumber;
	int repeat;
	const char *Input1;
	const char *Input2;
	timemin = Get_Float_Parameter("Time_Min");
	timemax = Get_Float_Parameter("Time_Max");
	timernumber = Get_Int_Parameter("TimerNum");
	repeat = Get_Int_Parameter("Repeat");
	if (number == timernumber)
	{
		Input1 = Get_Parameter("Input1");
		Input2 = Get_Parameter("Input2");
		Console_Input(Input1);
		Console_Input(Input2);
		if (repeat == 1)
		{
			timertime = Commands->Get_Random(timemin,timemax);
			Commands->Start_Timer(obj,this,timertime,timernumber);
		}
	}
}

void z_RemoveWeaponModel::Created(GameObject *obj)
{
	const char *Weapon;
	Weapon = Get_Parameter("Weapon_Model");
	Remove_Weapon(obj,Weapon);
}

void z_rwk::Killed(GameObject *obj, GameObject *shooter)
{
	if (Commands->Is_A_Star(shooter))
	Commands->Create_Object(Commands->Get_Preset_Name(obj),Commands->Get_Position(obj));
}

void z_GameoverCreated::Created(GameObject *obj)
{
	char Msg[100]; char Msg2[100];
	Console_Input("gameover");
	sprintf(Msg,"amsg Mission Accomplished!");
	sprintf(Msg2,"snda m00evag_dsgn0005i1evag_snd.wav");
	Console_Input(Msg);
	Console_Input(Msg2);
}

void z_Created_Send_Custom_Param::Created(GameObject *obj)
{
	Commands->Send_Custom_Event(obj,Commands->Find_Object(Get_Int_Parameter("ID")),Get_Int_Parameter("Message"),Get_Int_Parameter("param"),0);
}

void z_NoDamageMoneyPoints::Damaged(GameObject *obj, GameObject *damager, float damage)
{
	Commands->Set_Health(obj,Commands->Get_Max_Health(obj));
	Commands->Give_Points(damager,(float)(damage*-1.0),false);
	Commands->Give_Money(damager,(float)(damage*-1.0),false);
}

void z_Set_Team::Created(GameObject *obj)
{
	Commands->Set_Player_Type(obj,Get_Int_Parameter("team"));
}

void z_Set_Skin_Created::Created(GameObject *obj)
{
	const char *skin;
	float amm;
	{
		skin = Get_Parameter("SkinType_Created");
		amm = Get_Float_Parameter("Ammount");
		Set_Skin(obj,skin);
		Commands->Set_Health(obj,amm);
	}
}
void z_Set_Skin_Created::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	const char *skin;
	const char *rmvs;
	if (message == Get_Int_Parameter("Message"))
	{
		rmvs = Get_Parameter("Remove_Script");
		skin = Get_Parameter("NewSkinType");
		Set_Skin(obj,skin);
		Commands->Set_Health(obj,Commands->Get_Max_Health(obj));
		Remove_Script(obj,rmvs);
	}
}

void z_Set_Skin::Created(GameObject *obj)
{
	const char *skin;
	float amm;
	{
		skin = Get_Parameter("SkinType");
		amm = Get_Float_Parameter("Ammount");
		Set_Skin(obj,skin);
		Commands->Set_Health(obj,amm);
	}
}

void z_Death_Enable_Spawner::Killed(GameObject *obj,GameObject *shooter) 
{
	Commands->Enable_Spawner(Get_Int_Parameter("ID"),Get_Int_Parameter("Enable"));
}

void z_Death_Enable_3Spawners::Killed(GameObject *obj,GameObject *shooter)
{
	Commands->Enable_Spawner(Get_Int_Parameter("ID1"),Get_Int_Parameter("Enable1"));
	Commands->Enable_Spawner(Get_Int_Parameter("ID2"),Get_Int_Parameter("Enable2"));
	Commands->Enable_Spawner(Get_Int_Parameter("ID3"),Get_Int_Parameter("Enable3"));
}

void z_UnkillableUntilEntered::Damaged(GameObject *obj, GameObject *damager, float damage)
{
	Commands->Set_Health(obj,Commands->Get_Max_Health(obj));
	Commands->Set_Shield_Strength(obj,Commands->Get_Max_Shield_Strength(obj));
}
void z_UnkillableUntilEntered::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == CUSTOM_EVENT_VEHICLE_ENTER)
	{
		Destroy_Script();
	}
}


void z_DestroyVeh259::Created(GameObject *obj)
{ 
	Commands->Start_Timer(obj,this,259,1);
}
void z_DestroyVeh259::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == CUSTOM_EVENT_VEHICLE_ENTER)
	{
		Destroy_Script();
	}
}
void z_DestroyVeh259::Timer_Expired(GameObject *obj, int number)
{ 
	if (number == 1)
	{
		Commands->Apply_Damage(obj,2500,"Death",0);
	}
}

void z_VehExit::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == CUSTOM_EVENT_VEHICLE_EXIT)
	{
		Commands->Attach_Script(obj,"z_DestroyVeh259","");
	}
}

void z_Spawn_When_Killed::Killed(GameObject *obj,GameObject *shooter)//TFX_Spawn_When_Killed, except this script will allow you to set the skintype, Health and Armor ammount instead of a percentage.
{
	float facing = Commands->Get_Facing(obj);
	float Health;
	float Armor;
	const char *skin;
	Vector3 position = Commands->Get_Position(obj);
	GameObject *newobj;
	position.Z += Get_Float_Parameter("DropHeight");
	newobj = Commands->Create_Object(Get_Parameter("PresetName"),position);
	skin = Get_Parameter("SkinType");
	Health = Get_Float_Parameter("Health");
	Armor = Get_Float_Parameter("Armor");
	Set_Skin(newobj,skin);
	Commands->Set_Health(newobj,Health);
	Commands->Set_Shield_Strength(newobj,Armor);
	if (!Get_Int_Parameter("SameFacing")) return;
	Commands->Set_Facing(newobj,facing);
}

void z_Remove_Script_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	const char *script;
	script = Get_Parameter("Script");
	if (message == Get_Int_Parameter("Message"))
	{
		Remove_Script(obj,script);
	}
}

void z_Teleport_Powerup::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 1000000025)
	{
		Vector3 spawn_position;
		spawn_position = Get_Vector3_Parameter("Location");
		int x = Get_Int_Parameter("Object_ID");
		if (x)
		{
			GameObject *gotoObject = Commands->Find_Object(x);
			Vector3 gotoLocation = Commands->Get_Position(gotoObject);
			Commands->Set_Position(sender,gotoLocation);
		}
		else
		{
			Commands->Set_Position(sender,spawn_position);
		}
	}
}

void z_blamo4sec::Created(GameObject *obj)
{
	Commands->Start_Timer(obj,this,4.0f,111);
	{
		Commands->Set_Shield_Type(obj,"blamo");
	}
}
void z_blamo4sec::Timer_Expired(GameObject *obj, int number) 
{ 
	if (number == 111) 
	{
		Commands->Set_Shield_Type(obj,"ShieldKevlar");
		Destroy_Script();
	}
}

void z_ChangeTeamPowerup::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 1000000025)
	{
		char pick[256];
		char cht[200];
		if (Commands->Get_Player_Type(sender) == 1)
		{
			sprintf(cht,"team2 %d 0",Get_Player_ID(sender));
			const char *c = Get_Player_Name(sender);
			sprintf(pick,"msg %s team was changed to NOD by a powerup.",c);
			delete[] c;
		}
		else if (Commands->Get_Player_Type(sender) == 0)
		{
			sprintf(cht,"team2 %d 1",Get_Player_ID(sender));
			const char *c = Get_Player_Name(sender);
			sprintf(pick,"msg %s team was changed to GDI by a powerup.",c);
			delete[] c;
		}
		Console_Input(pick); Console_Input(cht);
	}
}

ScriptRegistrant<z_Console_Input_Poke_2> z_Console_Input_Poke_2_Registrant("z_Console_Input_Poke_2","Console_Command1:string,Console_Command2:string");
ScriptRegistrant<z_Console_Input_Killed> z_Console_Input_Killed_Registrant("z_Console_Input_Killed","Input:string");
ScriptRegistrant<z_Console_Input_Killed_2> z_Console_Input_Killed_2_Registrant("z_Console_Input_Killed_2","Input=msg:string,Input2=snda:string");
ScriptRegistrant<z_Console_Input_Custom_2> z_Console_Input_Custom_2_Registrant("z_Console_Input_Custom_2","Message:int,Input:string,Input2:string");
ScriptRegistrant<z_Enable_Multiple_Spawners_Custom> z_Enable_Multiple_Spawners_Custom_Registrant("z_Enable_Multiple_Spawners_Custom","ID1:int,ID2:int,ID3:int,ID4:int,ID5:int,Enable:int,Message:int");
ScriptRegistrant<z_Play_Three_Cinematics_Custom> z_Play_Three_Cinematics_Custom_Registrant("z_Play_Three_Cinematics_Custom","Message:int,Cinematic1:string,Location1:vector3,Facing1:float,Cinematic2:string,Location2:vector3,Facing2:float,Cinematic3:string,Location3:vector3,Facing3:float,Console_Command:string");
ScriptRegistrant<z_Play_Cinematic_Console_Input_Custom> z_Play_Cinematic_Console_Input_Custom_Registrant("z_Play_Cinematic_Console_Input_Custom","Message:int,Script_Name:string,Location:vector3,Facing:float,Console_Command:string");
ScriptRegistrant<z_Destroy_Three_Objects_Custom> z_Destroy_Three_Objects_Custom_Registrant("z_Destroy_Three_Objects_Custom","Message:int,ID1:int,ID2:int,ID3:int");
ScriptRegistrant<z_RemoveWeaponModel> z_RemoveWeaponModel_Registrant("z_RemoveWeaponModel","Weapon_Model:string");
ScriptRegistrant<z_rwk> z_rwk_Registrant("z_rwk","");
ScriptRegistrant<z_GameoverCreated> z_GameoverCreated_Registrant("z_GameoverCreated","");
ScriptRegistrant<z_Console_Input_Custom_Delay_1> z_Console_Input_Custom_Delay_1_Registrant("z_Console_Input_Custom_Delay_1","Message:int,Input1:string,TimerNum:int,Time_Min:float,Time_Max:float,Repeat:int");
ScriptRegistrant<z_Console_Input_Custom_Delay_2> z_Console_Input_Custom_Delay_2_Registrant("z_Console_Input_Custom_Delay_2","Message:int,Input1:string,Input2:string,TimerNum:int,Time_Min:float,Time_Max:float,Repeat:int");
ScriptRegistrant<z_Created_Send_Custom_Param> z_Created_Send_Custom_Param_Registrant("z_Created_Send_Custom_Param","ID:int,Message:int,param:int");
ScriptRegistrant<z_NoDamageMoneyPoints> z_NoDamageMoneyPoints_Registrant("z_NoDamageMoneyPoints","");
ScriptRegistrant<z_Set_Team> z_Set_Team_Registrant("z_Set_Team","team:int");
ScriptRegistrant<z_Set_Skin_Created> z_Set_Skin_Created_Registrant("z_Set_Skin_Created","SkinType_Created=Blamo:string,Ammount=2500:float,Message=0:int,NewSkinType=CNCStructureHeavy:string,Remove_Script=z_NoDamageMoneyPoints:string");
ScriptRegistrant<z_Set_Skin> z_Set_Skin_Registrant("z_Set_Skin","SkinType=Blamo:string,Ammount=2500:float");
ScriptRegistrant<z_Death_Enable_Spawner> z_Death_Enable_Spawner_Registrant("z_Death_Enable_Spawner","ID:int,Enable:int");
ScriptRegistrant<z_Death_Enable_3Spawners> z_Death_Enable_3Spawners_Registrant("z_Death_Enable_3Spawners","ID1:int,Enable1:int,ID2:int,Enable2:int,ID3:int,Enable3:int");
ScriptRegistrant<z_UnkillableUntilEntered> z_UnkillableUntilEntered_Registrant("z_UnkillableUntilEntered","");
ScriptRegistrant<z_DestroyVeh259> z_DestroyVeh259_Registrant("z_DestroyVeh259","");
ScriptRegistrant<z_VehExit> z_VehExit_Registrant("z_VehExit","");
ScriptRegistrant<z_Spawn_When_Killed> z_Spawn_When_Killed_Registrant("z_Spawn_When_Killed","PresetName:string,DropHeight=0:float,SameFacing=1:int,SkinType=Blamo:string,Health=2500:float,Armor=2500:float");
ScriptRegistrant<z_Remove_Script_Custom> z_Remove_Script_Custom_Registrant("z_Remove_Script_Custom","Script=scriptname:string,Message=100:int");
ScriptRegistrant<z_Teleport_Powerup> z_Teleport_Powerup_Registrant("z_Teleport_Powerup","Location:vector3,Object_ID:int");
ScriptRegistrant<z_blamo4sec> z_blamo4sec_Registrant("z_blamo4sec","");
ScriptRegistrant<z_ChangeTeamPowerup> z_ChangeTeamPowerup_Registrant("z_ChangeTeamPowerup","");
