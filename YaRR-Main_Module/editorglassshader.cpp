/*	Renegade Scripts.dll
	Glass Shader Class for the Shader Database Editor
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <direct.h>
#include "scripts.h"
#include "shadereng.h"
#include <commdlg.h>
#include "shader.h"
#include "editorshader.h"
#include "editorshadermgr.h"
#include "glassshader.h"
#include "editorglassshader.h"
#include "resource1.h"
#pragma warning(disable: 6031)

float GetDlgItemFloat(HWND hDlg, int id);
BOOL SetDlgItemFloat(HWND hDlg, int id, float f);
EditorGlassShaderClass *CurrentShader = 0;

BOOL CALLBACK GlassDlgProc(HWND hwnd,UINT Message,WPARAM wParam,LPARAM lParam)
{
	OPENFILENAME ofn;
	char sfile[MAX_PATH] = "";
	char c[MAX_PATH];
	switch(Message)
	{
	case WM_INITDIALOG:
		if (CurrentShader->shader->Filename)
		{
			SetDlgItemText(hwnd,IDC_SHADERFILE,CurrentShader->shader->Filename);
		}
		if (CurrentShader->shader->EnvironmentMap)
		{
			SetDlgItemText(hwnd,IDC_TEXTURE,CurrentShader->shader->EnvironmentMap);
		}
		SetDlgItemFloat(hwnd,IDC_REFLECT,CurrentShader->shader->ReflectStrength);
		SetDlgItemFloat(hwnd,IDC_REFRACT,CurrentShader->shader->RefractStrength);
		break;
	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			GetDlgItemText(hwnd,IDC_SHADERFILE,sfile,MAX_PATH);
			CurrentShader->shader->Set_Filename(sfile);
			GetDlgItemText(hwnd,IDC_TEXTURE,sfile,MAX_PATH);
			CurrentShader->shader->EnvironmentMap = newstr(sfile);
			CurrentShader->shader->ReflectStrength = GetDlgItemFloat(hwnd,IDC_REFLECT);
			CurrentShader->shader->RefractStrength = GetDlgItemFloat(hwnd,IDC_REFRACT);
			EndDialog(hwnd, 1);
			break;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			break;
		case IDC_OPEN:
			_getcwd(c,MAX_PATH);
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFilter = "Shaders (*.fx)\0*.fx\0All Files (*.*)\0*.*\0";
			ofn.lpstrInitialDir = c;
			ofn.lpstrFile = sfile;
			ofn.lpstrTitle = "Open Shader";
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = ".fx";
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_DONTADDTORECENT | OFN_NOCHANGEDIR;
			if (GetOpenFileName(&ofn))
			{
				strcpy(c,&strrchr(sfile,'\\')[1]);
				SetDlgItemText(hwnd,IDC_SHADERFILE,c);
			}
			break;
		}
	default:
		return FALSE;
	}
	return TRUE;
}

EditorGlassShaderClass::EditorGlassShaderClass()
{
	shader = new GlassShaderClass();
}

EditorGlassShaderClass::~EditorGlassShaderClass()
{
	delete shader;
}

void EditorGlassShaderClass::Load(ChunkLoadClass& cload)
{
	shader->Load(cload);
}

void EditorGlassShaderClass::Save(ChunkSaveClass& csave)
{
	shader->Save(csave);
}

void EditorGlassShaderClass::Edit(HWND ParentDialog)
{
	CurrentShader = this;
	DialogBox(hInst, MAKEINTRESOURCE(IDD_GLASS), ParentDialog,(DLGPROC)GlassDlgProc);
	CurrentShader = 0;
}

const char *EditorGlassShaderClass::Get_Name()
{
	return shader->Get_Name();
}

void EditorGlassShaderClass::Set_Name(const char *name)
{
	shader->Set_Name(name);
}

EditorShaderRegistrant<EditorGlassShaderClass> EditorGlassShaderRegistrant(CHUNK_GLASS_SHADER,"Glass Shader");
