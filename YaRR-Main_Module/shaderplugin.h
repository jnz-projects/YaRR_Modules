/*	Renegade Scripts.dll
	Interface for shaders.dll plugins
	Copyright 2007 Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module 
	that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#define SHADER_INTERFACEVERSION		0x03400000
#define SHADERPLUG_INTERFACEVERSION 0x03400000

#ifndef PURE
#define PURE = 0
#endif

class ShaderPluginClass;
class ShaderStateManager;
class ShaderCommandClass
{
#ifdef SHADERS_EXPORTS
public:
	ShaderCommandClass();
#else
protected:
	ShaderCommandClass();
#endif
public:	
	unsigned long InterfaceVersion;													// Verify against SHADER_INTERFACEVERSION.
																					// HIWORD value must match, LOWORD must be greater or equal to.
	virtual void RegisterPlugin(ShaderPluginClass *plugin);				// Registers the plugin with shaders.dll
	virtual void GetStateManager(ShaderStateManager **statemanager);		// Get the active state manager class
	virtual void SendCustom(unsigned int eventid, int parameter);			// Send a custom to the scripts
	virtual void* AllocateMemory(size_t reportedSize);					// Allocate memory using the active memory manager				
	virtual void DeallocateMemory(void *reportedAddress);					// Deallocate said memory
	virtual void ConsoleOutput(const char *output,...);					// Output to console
	virtual void ConsoleInput(const char *input);							// Input command into console
	virtual void AddCombatMessage(const char *message, unsigned int red, 
							unsigned int green, unsigned int blue);					// Add a colored message to the chatbox
};

class ShaderPluginClass
{
public:
	unsigned long InterfaceVersion;																// Verify against SHADERPLUG_INTERFACEVERSION. 
																								// HIWORD value must match, LOWORD must be greater or equal to.
	virtual HRESULT OnReleaseResources() PURE;										// Called upon device reset
	virtual HRESULT OnReloadResources() PURE;											// Called after device reset
	virtual HRESULT OnDestroyResources() PURE;										// Called upon game close
	virtual HRESULT OnMapLoad() PURE;												// Called upon map load
	virtual HRESULT OnMapUnload() PURE;												// Called upon map unload
	virtual HRESULT OnCustom(unsigned int eventid, float parameter) PURE;				// Called whenever a shader custom is sent (via ShaderSend)
	virtual HRESULT OnCustom(unsigned int eventid, Vector4 parameter) PURE;			// Called whenever a shader custom is sent (via ShaderSend2)
	virtual HRESULT OnFrameStart() PURE;												// Called on frame start
	virtual HRESULT OnFrameEnd() PURE;												// Called on frame end
	virtual HRESULT OnScreenFadeRender() PURE;										// Called whenever a 'Screen Fade' is to be rendered
	virtual HRESULT OnRender(unsigned int primitive_type, unsigned short start_index,	// Called whenever a draw event occurs.
				unsigned short polygon_count, unsigned short min_vertex_index,		
				unsigned short vertex_count) PURE;									
};

typedef void (*_SetShaderCommands) (ShaderCommandClass *commands);
typedef void (*_GetPluginInterface) (ShaderPluginClass **plugin);

#ifdef SHADERS_EXPORTS
void InitShaderPluginSystem();
void DestroyShaderPluginSystem();

class ShaderPluginNotifyClass
{
private:
	SimpleDynVecClass<ShaderPluginClass *> Plugins;
	SimpleDynVecClass<HMODULE> PluginHandles;
public:
	ShaderPluginNotifyClass();
	~ShaderPluginNotifyClass();
	bool RegisterPlugin(ShaderPluginClass *plugin);
	bool LoadPluginFromFile(const char *pluginName);

	void OnReleaseResources();	
	void OnReloadResources();
	void OnDestroyResources();
	void OnMapLoad();
	void OnMapUnload();
	void OnCustom(unsigned int eventid, float parameter);
	void OnCustom(unsigned int eventid, Vector4 parameter);
	void OnFrameStart();
	void OnFrameEnd();
	void OnScreenFadeRender();
	void OnRender(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);								
};
extern ShaderPluginNotifyClass *ShaderPluginNotify;
#endif

extern ShaderCommandClass *ShaderCommands;

// HRESULT Messages
//
// MessageId:		ID_E_INVALIDVERSION
// MessageText:		Incompatible interface version
#define E_INVALIDVERSION   _HRESULT_TYPEDEF_(0x80040001L)
//
// MessageId:		S_CONTINUE
// MessageText:		Continue normal execution of function
#define S_CONTINUE   _HRESULT_TYPEDEF_(0x00040002L)