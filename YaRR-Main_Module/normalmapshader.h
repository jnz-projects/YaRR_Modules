/*	Renegade Scripts.dll
	Normal Map Shader Class
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class NormalMapShaderClass : public ProgrammableShaderClass {
private:
#ifndef SDBEDIT
	TextureClass *texture_NormalMap;
	void ApplyRenderState();
	void ApplyTransform();
	void ApplyVertexBuffer();
	void ApplyIndexBuffer();
	bool ResetMapVariables;
	bool EnableFillHandles;
	void FillHandles();
	D3DXHANDLE ColorTextureHandle;
	D3DXHANDLE NormalTextureHandle;
	
	D3DXHANDLE AmbientLightColorHandle;
	D3DXHANDLE SurfaceColorHandle;

	D3DXHANDLE Light1DiffuseColorHandle;
	D3DXHANDLE Light1SpecularColorHandle;
	D3DXHANDLE Light1SpecularPowerHandle;
	D3DXHANDLE Light1DirectionHandle;

	D3DXHANDLE WorldMatrixHandle;
	D3DXHANDLE WorldInverseTransposeMatrixHandle;
	D3DXHANDLE ViewInverseMatrixHandle;
	D3DXHANDLE WorldViewProjectionMatrixHandle;

	D3DXHANDLE NormalMapTechniqueHandle;
	D3DXHANDLE NormalMapSpecularTechniqueHandle;
	D3DXHANDLE NormalMapTerrainTechniqueHandle;
 	D3DXHANDLE NormalMapTerrainSpecularTechniqueHandle;

	D3DXHANDLE FogModeHandle;
	D3DXHANDLE FogStartHandle;
	D3DXHANDLE FogEndHandle;
	D3DXHANDLE FogDensityHandle;
	D3DXHANDLE FogColorHandle;

	Vector4 LightDirectionCache;
#endif
public:
	char *NormalMapFilename;
	Vector4 LightColor;
	Vector4 AmbientColor;
	Vector4 SurfaceColor;
	bool SpecularEnabled;
	Vector4 SpecularColor;
	float SpecularPower;
	NormalMapShaderClass();
	~NormalMapShaderClass();
	void Load(ChunkLoadClass& cload);
	void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	void Reset_Cache();
	void Release_Resources();
	void Reload_Resources();
	void Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
	bool Can_Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
#endif
};

#define SHADER_NORMALMAP 300
#define MC_NORMALMAP_MATERIALNAME 1
#define MC_NORMALMAP_FXFILENAME 2
#define MC_NORMALMAP_NORMALMAPFILENAME 3
#define MC_NORMALMAP_LIGHTCOLOR 4
#define MC_NORMALMAP_AMBIENTCOLOR 5
#define MC_NORMALMAP_SURFACECOLOR 6
#define MC_NORMALMAP_SPECULARENABLED 7
#define MC_NORMALMAP_SPECULARCOLOR 8
#define MC_NORMALMAP_SPECULARPOWER 9
