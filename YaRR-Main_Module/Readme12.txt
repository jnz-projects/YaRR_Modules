; ************************* Scripts by Daniel King - aka Dan *************************

 - Dan.h
 - Dan.cpp

====================================================================================================

; ************************* [Script Name] DAN_CnC_Crate

====================================================================================================

 [Description]

 - Attach this script to "CnC_Spawner_Crate" under Objects -> Spawner -> Object Spawners -> CnC Spawners and enable the available powerups from the crate by selecting the powerup from the drop down menu, and adding a number greater than 0 to enable it, or 0 to disable it.

 [Parameters]

 - Death_Crate (Death on pickup)

 - Stealth_Suit (Grants player stealth)

 - DeHealth (Depletes Health)

 - DeArmour (Depletes Armor)

 - Full_Health (Grants Full Health and Full Armour)

 - Money (Grants or removes the specified amount of money)

 - Spy_Crate

 - Random_Weapon (Chooses from the list of available powerups enabled in "CnC_Spawner_Crate")

 - Points (Adds or subtracts the specified amount of Points)

 - Theif (Steals all the player's money)

 ***NOT LISTED*** - Refill (Replenishes Ammo)

 [Notes]

 - More shall be added in future versions, as well as any bug fixes or performance changes. I have tested each script, and fixed any bugs that I have come across.

====================================================================================================

; ************************* [Script Name] DAN_Drop_Wreckage_On_Death

====================================================================================================

 [Description]

 - Attach this script to a tank that you want to drop a wreckage when it is destroyed.

 [Parameters]

 - Wreckage_Preset (The wreckage preset to drop)

 [Notes]

 - This script works along side DAN_Wreckage_Rebuildable.

====================================================================================================

; ************************* [Script Name] DAN_Wreckage_Rebuildable

====================================================================================================

 [Description]

 - Attach this script to a tank wreckage, and repair it to rebuild the tank. In the parameters, state the preset name of the tank that you want to spawn when the wreckage is fully repaired.

 [Parameters]

 - Vehicle_Preset (The vehicle preset to create)

 [Notes]

 - This script works alongside DAN_Drop_Wreckage_On_Death.