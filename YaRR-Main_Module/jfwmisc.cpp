/*	Renegade Scripts.dll
	Miscellanious scripts
	Copyright 2007 Vloktboky, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <fstream>
#include <time.h>
#include "scripts.h"
#include "engine.h"
#include "jfwmisc.h"

void JFW_User_Settable_Parameters::Created(GameObject *obj)
{
	const char *filename;
	const char *scriptname;
	char params[100];
	filename = Get_Parameter("File_Name");
	scriptname = Get_Parameter("Script_Name");
	std::ifstream f(filename);
	f.getline(params,100);
	Commands->Attach_Script(obj,scriptname,params);
}

void JFW_Debug_Text_File::Created(GameObject *obj)
{
	int id;
	t = time(0);
	filename = Get_Parameter("Log_File");
	description = Get_Parameter("Description");
	f = fopen(filename,"wt");
	id = Commands->Get_ID(obj);
	fprintf(f,"%s [ID %d] created.\n",description,id);
	fflush(f);
}

void JFW_Debug_Text_File::Destroyed(GameObject *obj)
{
	int id;
	id = Commands->Get_ID(obj);
	fprintf(f,"%s [ID %d] shutdown.\n",description,id);
	fflush(f);
	fclose(f);
}

void JFW_Debug_Text_File::Killed(GameObject *obj,GameObject *shooter)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(shooter);
	fprintf(f,"%s [ID %d] killed by object %d. %f sec.\n",description,id,id2,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Damaged(GameObject *obj,GameObject *damager,float damage)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(damager);
	fprintf(f,"%s [ID %d] damaged by object %d. Damage was %f %f sec.\n",description,id,id2,damage,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(sender);
	fprintf(f,"%s [ID %d] received custom event of type %d and param %d. Sender was object %d. %f sec.\n",description,id,message,param,id2,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Sound_Heard(GameObject *obj,const CombatSound & sound)
{
	float timediff;
	int id;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	fprintf(f,"%s [ID %d] heard sound %d at position %f %f %f object %d. %f sec.\n",description,id,sound.sound,sound.position.X,sound.position.Y,sound.position.Z,Commands->Get_ID(sound.obj),timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(seen);
	fprintf(f,"%s [ID %d] saw enemy: object %d. %f sec.\n",description,id,id2,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	float timediff;
	int id;
	const char *reasonstr;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	switch (reason)
	{
	case ACTION_COMPLETE_ATTACK_OUT_OF_RANGE:
		reasonstr = "ACTION_COMPLETE_ATTACK_OUT_OF_RANGE";
		break;
	case ACTION_COMPLETE_MOVE_NO_PROGRESS_MADE:
		reasonstr = "ACTION_COMPLETE_MOVE_NO_PROGRESS_MADE";
		break;
	case ACTION_COMPLETE_PATH_BAD_DEST:
		reasonstr = "ACTION_COMPLETE_PATH_BAD_DEST";
		break;
	case ACTION_COMPLETE_PATH_BAD_START:
		reasonstr = "ACTION_COMPLETE_PATH_BAD_START";
		break;
	case ACTION_COMPLETE_LOW_PRIORITY:
		reasonstr = "ACTION_COMPLETE_LOW_PRIORITY";
		break;
	case ACTION_COMPLETE_NORMAL:
		reasonstr = "ACTION_COMPLETE_NORMAL";
		break;
	default:
		reasonstr = "ACTION_COMPLETE_NORMAL";
	}
	fprintf(f,"Action %d complete on %s [ID %d] -- Reason: %s. %f sec.\n",action,description,id,reasonstr,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Timer_Expired(GameObject *obj,int number)
{
	float timediff;
	int id;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	fprintf(f,"timer %d on %s [ID %d] expired. %f sec.\n",number,description,id,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Animation_Complete(GameObject *obj,const char *anim)
{
	float timediff;
	int id;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	fprintf(f,"animation %s on %s [ID %d] complete. %f sec.\n",anim,description,id,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Poked(GameObject *obj,GameObject *poker)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(poker);
	fprintf(f,"%s [ID %d] was poked by: object %d. %f sec.\n",description,id,id2,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Entered(GameObject *obj,GameObject *enter)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(enter);
	fprintf(f,"%s [ID %d] was entered by: object %d. %f sec.\n",description,id,id2,timediff);
	fflush(f);
}

void JFW_Debug_Text_File::Exited(GameObject *obj,GameObject *exit)
{
	float timediff;
	int id,id2;
	t2 = time(0);
	timediff = (float)difftime(t2,t);
	id = Commands->Get_ID(obj);
	id2 = Commands->Get_ID(exit);
	fprintf(f,"%s [ID %d] was exited by: object %d. %f sec.\n",description,id,id2,timediff);
	fflush(f);
}

void JFW_Power_Off::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg,msg2;
	msg = Get_Int_Parameter("Message_Off");
	msg2 = Get_Int_Parameter("Message_On");
	if (msg == message)
	{
		Commands->Set_Building_Power(obj,false);
	}
	if (msg2 == message)
	{
		Commands->Set_Building_Power(obj,true);
	}
}

void JFW_Follow_Waypath::Created(GameObject *obj)
{
	int waypathid;
	float speed;
	waypathid = Get_Int_Parameter("Waypathid");
	speed = Get_Float_Parameter("Speed");
	ActionParamsStruct params;
	params.Set_Move_Arrive_Distance(100);
	params.Set_Basic(this,100,777);
	params.Set_Goto(0,speed,10);
	params.Set_Goto_Waypath(waypathid,true,false,false);
	Commands->Action_Goto(obj,params);
}

void JFW_Follow_Waypath_Zone::Entered(GameObject *obj,GameObject *enter)
{
	if (!_stricmp(Commands->Get_Preset_Name(enter),Get_Parameter("Preset")))
	{
		int waypathid;
		float speed;
		waypathid = Get_Int_Parameter("Waypathid");
		speed = Get_Float_Parameter("Speed");
		ActionParamsStruct params;
		params.Set_Move_Arrive_Distance(100);
		params.Set_Basic(this,100,777);
		params.Set_Goto(0,speed,10);
		params.Set_Goto_Waypath(waypathid,true,false,false);
		Commands->Action_Goto(enter,params);
	}
}

void JFW_Object_Draw_In_Order::Created(GameObject *obj)
{
	currentmodelid = 0;
	currentmodelnumber = 1;
}

void JFW_Object_Draw_In_Order::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	Vector3 location;
	const char *basename;
	char name[20];
	int count;
	float facing;
	GameObject *object;
	msg = Get_Int_Parameter("Custom");
	if (message == msg)
	{
		if (currentmodelid)
		{
			Commands->Destroy_Object(Commands->Find_Object(currentmodelid));
		}
		location = Get_Vector3_Parameter("location");
		basename = Get_Parameter("BaseName");
		count = Get_Int_Parameter("Count");
		facing = Get_Float_Parameter("Facing");
		sprintf(name,"%s%i",basename,currentmodelnumber);
		object = Commands->Create_Object("Generic_Cinematic",location);
		Commands->Enable_Hibernation(object,false);
		Commands->Add_To_Dirty_Cull_List(object);
		Commands->Set_Model(object,name);
		Commands->Set_Facing(object,facing);
		currentmodelid = Commands->Get_ID(object);
		currentmodelnumber++;
		if (currentmodelnumber > count)
		{
			currentmodelnumber = 1;
		}
	}
}

void JFW_Object_Draw_In_Order::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&currentmodelnumber);
	Auto_Save_Variable(2,4,&currentmodelid);
}

void JFW_Object_Draw_In_Order_2::Created(GameObject *obj)
{
	currentmodelid = 0;
	currentmodelnumber = Get_Int_Parameter("Start_Number");
}

void JFW_Object_Draw_In_Order_2::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	Vector3 location;
	const char *basename;
	char name[20];
	int count;
	float facing;
	GameObject *object;
	msg = Get_Int_Parameter("Custom");
	if (message == msg)
	{
		if (currentmodelid)
		{
			Commands->Destroy_Object(Commands->Find_Object(currentmodelid));
		}
		location = Get_Vector3_Parameter("location");
		basename = Get_Parameter("BaseName");
		count = Get_Int_Parameter("Count");
		facing = Get_Float_Parameter("Facing");
		sprintf(name,"%s%i",basename,currentmodelnumber);
		object = Commands->Create_Object("Generic_Cinematic",location);
		Commands->Enable_Hibernation(object,false);
		Commands->Add_To_Dirty_Cull_List(object);
		Commands->Set_Model(object,name);
		Commands->Set_Facing(object,facing);
		currentmodelid = Commands->Get_ID(object);
		currentmodelnumber++;
		if (currentmodelnumber > count)
		{
			currentmodelnumber = Get_Int_Parameter("Start_Number");
		}
	}
}

void JFW_Object_Draw_In_Order_2::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&currentmodelnumber);
	Auto_Save_Variable(2,4,&currentmodelid);
}

void JFW_Object_Draw_Random::Created(GameObject *obj)
{
	currentmodelid = 0;
}

void JFW_Object_Draw_Random::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	int msg;
	Vector3 location;
	const char *basename;
	char name[20];
	int count;
	int currentmodelnumber;
	float facing;
	GameObject *object;
	msg = Get_Int_Parameter("Custom");
	if (message == msg)
	{
		if (currentmodelid)
		{
			Commands->Destroy_Object(Commands->Find_Object(currentmodelid));
		}
		Commands->Destroy_Object(Commands->Find_Object(currentmodelid));
		location = Get_Vector3_Parameter("location");
		basename = Get_Parameter("BaseName");
		count = Get_Int_Parameter("Count");
		facing = Get_Float_Parameter("Facing");
		currentmodelnumber = Commands->Get_Random_Int(1,count);
		sprintf(name,"%s%i",basename,currentmodelnumber);
		object = Commands->Create_Object("Generic_Cinematic",location);
		Commands->Enable_Hibernation(object,false);
		Commands->Add_To_Dirty_Cull_List(object);
		Commands->Set_Model(object,name);
		Commands->Set_Facing(object,facing);
		currentmodelid = Commands->Get_ID(object);
	}
}

void JFW_Object_Draw_Random::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&currentmodelid);
}

void JFW_Play_Animation_Destroy_Object::Created(GameObject *obj)
{
	const char *subobject = Get_Parameter("Subobject");
	if (!_stricmp(subobject,"0"))
	{
		subobject = 0;
	}
	float firstframe = Get_Float_Parameter("FirstFrame");
	if (firstframe == -1)
	{
		firstframe = Get_Animation_Frame(obj);
	}
	Commands->Set_Animation(obj,Get_Parameter("Animation"),false,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
}

void JFW_Play_Animation_Destroy_Object::Animation_Complete(GameObject *obj,const char *anim)
{
	Commands->Destroy_Object(obj);
}

void JFW_Animated_Effect::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Vector3 pos = Get_Vector3_Parameter("Location");
		GameObject *object = Commands->Create_Object("Generic_Cinematic",pos);
		Commands->Set_Model(object,Get_Parameter("Model"));
		const char *subobject = Get_Parameter("Subobject");
		if (!_stricmp(subobject,"0"))
		{
			subobject = 0;
		}
		float firstframe = Get_Float_Parameter("FirstFrame");
		Commands->Set_Animation(object,Get_Parameter("Animation"),false,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
	}
}

void JFW_Animated_Effect_2::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
		Vector3 pos = Commands->Get_Position(obj);
		GameObject *object = Commands->Create_Object("Generic_Cinematic",pos);
		Commands->Set_Model(object,Get_Parameter("Model"));
		const char *subobject = Get_Parameter("Subobject");
		if (!_stricmp(subobject,"0"))
		{
			subobject = 0;
		}
		float firstframe = Get_Float_Parameter("FirstFrame");
		Commands->Set_Animation(object,Get_Parameter("Animation"),false,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
}

void JFW_Random_Animated_Effect::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Vector3 pos = Get_Vector3_Parameter("Location");
		Vector3 offset = Get_Vector3_Parameter("Offset");
		pos.X += Commands->Get_Random(-offset.X,offset.X);
		pos.Y += Commands->Get_Random(-offset.Y,offset.Y);
		pos.Z += Commands->Get_Random(-offset.Z,offset.Z);
		GameObject *object = Commands->Create_Object("Generic_Cinematic",pos);
		Commands->Set_Model(object,Get_Parameter("Model"));
		const char *subobject = Get_Parameter("Subobject");
		if (!_stricmp(subobject,"0"))
		{
			subobject = 0;
		}
		float firstframe = Get_Float_Parameter("FirstFrame");
		Commands->Set_Animation(object,Get_Parameter("Animation"),false,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
	}
}

void JFW_Random_Animated_Effect_2::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
		Vector3 pos = Commands->Get_Position(obj);
		Vector3 offset = Get_Vector3_Parameter("Offset");
		pos.X += Commands->Get_Random(-offset.X,offset.X);
		pos.Y += Commands->Get_Random(-offset.Y,offset.Y);
		pos.Z += Commands->Get_Random(-offset.Z,offset.Z);
		GameObject *object = Commands->Create_Object("Generic_Cinematic",pos);
		Commands->Set_Model(object,Get_Parameter("Model"));
		const char *subobject = Get_Parameter("Subobject");
		if (!_stricmp(subobject,"0"))
		{
			subobject = 0;
		}
		float firstframe = Get_Float_Parameter("FirstFrame");
		Commands->Set_Animation(object,Get_Parameter("Animation"),false,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
}

void JFW_Animated_Effect::Animation_Complete(GameObject *obj,const char *anim)
{
	Commands->Destroy_Object(obj);
}

void JFW_Animated_Effect_2::Animation_Complete(GameObject *obj,const char *anim)
{
	Commands->Destroy_Object(obj);
}

void JFW_Random_Animated_Effect_2::Animation_Complete(GameObject *obj,const char *anim)
{
	Commands->Destroy_Object(obj);
}

void JFW_Random_Animated_Effect::Animation_Complete(GameObject *obj,const char *anim)
{
	Commands->Destroy_Object(obj);
}

void JFW_Fog_Create::Created(GameObject *obj)
{
	if (Get_Int_Parameter("OnCreate") > 0)
	{
		if (Get_Float_Parameter("Delay") > 0)
		{
			Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),1);
		}
		else
		{
			bool fogenable = Get_Int_Parameter("Fog_Enable");
			Commands->Set_Fog_Enable(fogenable);
			if (fogenable)
			{
				Commands->Set_Fog_Range(Get_Float_Parameter("Fog_Start_Distance"),Get_Float_Parameter("Fog_End_Distance"),1.0f);
			}
		}
	}
}

void JFW_Fog_Create::Destroyed(GameObject *obj)
{
	if (Get_Int_Parameter("OnDestroy") > 0)
	{
		if (Get_Float_Parameter("Delay") > 0)
		{
			Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),1);
		}
		else
		{
			bool fogenable = Get_Int_Parameter("Fog_Enable");
			Commands->Set_Fog_Enable(fogenable);
			if (fogenable)
			{
				Commands->Set_Fog_Range(Get_Float_Parameter("Fog_Start_Distance"),Get_Float_Parameter("Fog_End_Distance"),1.0f);
			}
		}
	}
}

void JFW_Fog_Create::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		bool fogenable = Get_Int_Parameter("Fog_Enable");
		Commands->Set_Fog_Enable(fogenable);
		if (fogenable)
		{
			Commands->Set_Fog_Range(Get_Float_Parameter("Fog_Start_Distance"),Get_Float_Parameter("Fog_End_Distance"),1.0f);
		}
	}
}

void JFW_War_Blitz_Create::Created(GameObject *obj)
{
	if (Get_Int_Parameter("OnCreate") > 0)
	{
		if (Get_Float_Parameter("Delay") > 0)
			Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),1);
		else
			Commands->Set_War_Blitz(Get_Float_Parameter("War_Blitz_Intensity"),Get_Float_Parameter("Start_Distance"),Get_Float_Parameter("End_Distance"),Get_Float_Parameter("War_Blitz_Heading"),Get_Float_Parameter("War_Blitz_Distribution"),1.0f);
	}
}

void JFW_War_Blitz_Create::Destroyed(GameObject *obj)
{
	if (Get_Int_Parameter("OnDestroy") > 0)
	{
		if (Get_Float_Parameter("Delay") > 0)
			Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),1);
		else 
			Commands->Set_War_Blitz(Get_Float_Parameter("War_Blitz_Intensity"),Get_Float_Parameter("Start_Distance"),Get_Float_Parameter("End_Distance"),Get_Float_Parameter("War_Blitz_Heading"),Get_Float_Parameter("War_Blitz_Distribution"),1.0f);
	}
}

void JFW_War_Blitz_Create::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
		Commands->Set_War_Blitz(Get_Float_Parameter("War_Blitz_Intensity"),Get_Float_Parameter("Start_Distance"),Get_Float_Parameter("End_Distance"),Get_Float_Parameter("War_Blitz_Heading"),Get_Float_Parameter("War_Blitz_Distribution"),1.0f);
}

void JFW_Goto_Object_On_Startup::Created(GameObject *obj)
{
	ActionParamsStruct params;
	params.Set_Basic(this,100,2);
	params.Set_Goto(Commands->Find_Object(Get_Int_Parameter("ID")),Get_Float_Parameter("Speed"),Get_Float_Parameter("Arrivedistance"));
	Commands->Action_Goto(obj,params);
}

void JFW_Scope::Created(GameObject *obj)
{
	Set_Scope(obj,Get_Int_Parameter("Scope"));
}

void JFW_HUD::Created(GameObject *obj)
{
	Set_HUD_Texture(obj,Get_Parameter("HUDTexture"));
}

void JFW_Reticle::Created(GameObject *obj)
{
	Set_Reticle_Texture1(obj,Get_Parameter("ReticleTexture1"));
	Set_Reticle_Texture2(obj,Get_Parameter("ReticleTexture2"));
}

void JFW_HUD_INI::Created(GameObject *obj)
{
	Load_New_HUD_INI(obj,Get_Parameter("HUDINI"));
}

void JFW_Screen_Fade_On_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Set_Screen_Fade_Opacity_Player(obj,Get_Float_Parameter("Opacity"),0.0f);
		Set_Screen_Fade_Color_Player(obj,Get_Float_Parameter("Red"),Get_Float_Parameter("Green"),Get_Float_Parameter("Blue"),0.0f);
	}
}

void JFW_Screen_Fade_On_Enter::Entered(GameObject *obj,GameObject *enter)
{
	Set_Screen_Fade_Opacity_Player(enter,Get_Float_Parameter("Opacity"),0.0f);
	Set_Screen_Fade_Color_Player(enter,Get_Float_Parameter("Red"),Get_Float_Parameter("Green"),Get_Float_Parameter("Blue"),0.0f);
}

void JFW_Screen_Fade_On_Exit::Exited(GameObject *obj,GameObject *exit)
{
	Set_Screen_Fade_Opacity_Player(exit,Get_Float_Parameter("Opacity"),0.0f);
	Set_Screen_Fade_Color_Player(exit,Get_Float_Parameter("Red"),Get_Float_Parameter("Green"),Get_Float_Parameter("Blue"),0.0f);
}

void JFW_BHS_DLL::Created(GameObject *obj)
{
	Console_Output("BHS.DLL is required for this map");
}

void JFW_Screen_Fade_Custom_Timer::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		int TimerNum = Get_Int_Parameter("TimerNum");
		float Time = Get_Float_Parameter("Time");
		Commands->Start_Timer(obj,this,Time,TimerNum);
		Commands->Set_Screen_Fade_Color(Get_Float_Parameter("Red"),Get_Float_Parameter("Green"),Get_Float_Parameter("Blue"),0);
		Commands->Set_Screen_Fade_Opacity(Get_Float_Parameter("Opacity"),0);
	}
}

void JFW_Screen_Fade_Custom_Timer::Timer_Expired(GameObject *obj,int number)
{
	int TimerNum = Get_Int_Parameter("TimerNum");
	if (TimerNum == number)
	{
		Commands->Set_Screen_Fade_Color(0.0,0.0,0.0,0.0);
		Commands->Set_Screen_Fade_Opacity(0.0,0.0);
	}
}

void JFW_Stealthable_Object::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Commands->Enable_Stealth(obj,1);
		Commands->Start_Timer(obj,this,(float)2.80,1);
		Commands->Start_Timer(obj,this,(float)3.20,2);
		enabled = true;
	}
}

void JFW_Stealthable_Object::Timer_Expired(GameObject *obj,int number) 
{
	if (number == 1)
	{
		enabled = false;
	}
	if (number == 2 && !enabled)
	{
		Commands->Enable_Stealth(obj,0);
	}
}

void JFW_Stealthable_Object::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&enabled);
}

void JFW_Object_Counter::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("IncrementMessage"))
	{
		count++;
	}
	if (message == Get_Int_Parameter("DecrementMessage"))
	{
		count--;
	}
	if (count >= Get_Int_Parameter("Count"))
	{
		Commands->Destroy_Object(sender);
	}
}

void JFW_Object_Counter::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&count);
}

void JFW_Change_Spawn_Character::Created(GameObject *obj)
{
	Change_Spawn_Char(Get_Int_Parameter("Player_Type"),Get_Parameter("Character"));
}

void JFW_Show_Info_Texture::Created(GameObject *obj)
{
	Set_Info_Texture(obj,Get_Parameter("Texture"));
	int TimerNum = Get_Int_Parameter("TimerNum");
	float Time = Get_Float_Parameter("Time");
	Commands->Start_Timer(obj,this,Time,TimerNum);
}

void JFW_Show_Info_Texture::Timer_Expired(GameObject *obj,int number)
{
	Clear_Info_Texture(obj);
	Destroy_Script();
}

void JFW_Wireframe_Mode::Created(GameObject *obj)
{
	Set_Wireframe_Mode(Get_Int_Parameter("Mode"));
}

void JFW_PT_Disable::Created(GameObject *obj)
{
	int type = Get_Int_Parameter("Type");
	if (type == 5)
	{
		Set_Enlisted(Get_Int_Parameter("Player_Type"),Get_Int_Parameter("Position"),0,0,"");
	}
	else if (type == 6)
	{
		Set_Beacon(Get_Int_Parameter("Player_Type"),0,0,0,"");
	}
	else
	{
		Set_Preset(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),0,0,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),0,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),1,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),2,0,"");
	}
	Update_PT_Data();
}

void JFW_PT_Disable_Death::Killed(GameObject *obj,GameObject *shooter)
{
	int type = Get_Int_Parameter("Type");
	if (type == 5)
	{
		Set_Enlisted(Get_Int_Parameter("Player_Type"),Get_Int_Parameter("Position"),0,0,"");
	}
	else if (type == 6)
	{
		Set_Beacon(Get_Int_Parameter("Player_Type"),0,0,0,"");
	}
	else
	{
		Set_Preset(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),0,0,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),0,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),1,0,"");
		Set_Alternate(Get_Int_Parameter("Player_Type"),type,Get_Int_Parameter("Position"),2,0,"");
	}
	Update_PT_Data();
}

void JFW_PT_Hide::Created(GameObject *obj)
{
	Hide_Preset_By_Name(Get_Int_Parameter("Player_Type"),Get_Parameter("Preset"));
	Update_PT_Data();
}

void JFW_PT_Hide_Death::Killed(GameObject *obj,GameObject *shooter)
{
	Hide_Preset_By_Name(Get_Int_Parameter("Player_Type"),Get_Parameter("Preset"));
	Update_PT_Data();
}

void JFW_PT_Hide_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		Hide_Preset_By_Name(Get_Int_Parameter("Player_Type"),Get_Parameter("Preset"));
		Update_PT_Data();
	}
}

void JFW_Change_Radar_Map::Created(GameObject *obj)
{
	Change_Radar_Map(Get_Float_Parameter("Scale"),Get_Float_Parameter("OffsetX"),Get_Float_Parameter("OffsetY"),Get_Parameter("Texture"));
}

void JFW_Goto_Player_Timer::Created(GameObject *obj)
{
	int TimerNum = Get_Int_Parameter("TimerNum");
	float Time = Get_Float_Parameter("Time");
	Commands->Start_Timer(obj,this,Time,TimerNum);
}

void JFW_Goto_Player_Timer::Timer_Expired(GameObject *obj,int number)
{
	int TimerNum = Get_Int_Parameter("TimerNum");
	float Time = Get_Float_Parameter("Time");
	if (number == TimerNum)
	{
		Commands->Action_Reset(obj,100);
		GameObject *object = Commands->Get_A_Star(Commands->Get_Position(obj));
		ActionParamsStruct params;
		params.Set_Basic(this,100,2);
		params.Set_Goto(object,Get_Float_Parameter("Speed"),Get_Float_Parameter("Arrivedistance"));
		Commands->Action_Goto(obj,params);
		Commands->Start_Timer(obj,this,Time,TimerNum);
	}
}

void JFW_Goto_Player_Timer::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	Commands->Action_Reset(obj,100);
}

void JFW_Credit_Grant::Created(GameObject *obj)
{
	float delay;
	delay = Get_Float_Parameter("Delay");
	Commands->Start_Timer(obj,this,delay,667);
}

void JFW_Credit_Grant::Timer_Expired(GameObject *obj,int number)
{
	if (number == 667)
	{
		Commands->Give_Money(Find_Smart_Object_By_Team(0),(float)Get_Int_Parameter("Credits"),true);
		Commands->Give_Money(Find_Smart_Object_By_Team(1),(float)Get_Int_Parameter("Credits"),true);
		float delay;
		delay = Get_Float_Parameter("Delay");
		Commands->Start_Timer(obj,this,delay,667);
	}
}

ScriptRegistrant<JFW_Stealthable_Object> JFW_Stealthable_Object_Registrant("JFW_Stealthable_Object","Message:int");
ScriptRegistrant<JFW_Object_Counter> JFW_Object_Counter_Registrant("JFW_Object_Counter","Count:int,IncrementMessage:int,DecrementMessage:int");
ScriptRegistrant<JFW_Fog_Create> JFW_Fog_Create_Registrant("JFW_Fog_Create","Fog_Enable:int,Fog_Start_Distance=0.000:float,Fog_End_Distance=0.000:float,Delay=0.000:float,OnCreate=0:int,OnDestroy=0:int");
ScriptRegistrant<JFW_War_Blitz_Create> JFW_War_Blitz_Create_Registrant("JFW_War_Blitz_Create","War_Blitz_Intensity=0.000:float,Start_Distance=0.000:float,End_Distance=1.000:float,War_Blitz_Heading=0.000:float,War_Blitz_Distribution=1.000:float,Delay=0.000:float,OnCreate=0:int,OnDestroy=0:int");
ScriptRegistrant<JFW_Goto_Object_On_Startup> JFW_Goto_Object_On_Startup_Registrant("JFW_Goto_Object_On_Startup","ID:int,Speed:float,ArriveDistance:float");
ScriptRegistrant<JFW_Animated_Effect> JFW_Animated_Effect_Registrant("JFW_Animated_Effect","Animation:string,Subobject:string,FirstFrame:float,LastFrame:float,Blended:int,Model:string,Message:int,Location:vector3");
ScriptRegistrant<JFW_Animated_Effect_2> JFW_Animated_Effect_2_Registrant("JFW_Animated_Effect_2","Animation:string,Subobject:string,FirstFrame:float,LastFrame:float,Blended:int,Model:string,Message:int");
ScriptRegistrant<JFW_Random_Animated_Effect> JFW_Random_Animated_Effect_Registrant("JFW_Random_Animated_Effect","Animation:string,Subobject:string,FirstFrame:float,LastFrame:float,Blended:int,Model:string,Message:int,Location:vector3,Offset:vector3");
ScriptRegistrant<JFW_Random_Animated_Effect_2> JFW_Random_Animated_Effect_2_Registrant("JFW_Random_Animated_Effect_2","Animation:string,Subobject:string,FirstFrame:float,LastFrame:float,Blended:int,Model:string,Message:int,Offset:vector3");
ScriptRegistrant<JFW_Object_Draw_In_Order> JFW_Object_Draw_In_Order_Registrant("JFW_Object_Draw_In_Order"," Location:vector3,Custom:int,BaseName:string,Count:int,Facing:float");
ScriptRegistrant<JFW_Object_Draw_In_Order_2> JFW_Object_Draw_In_Order_2_Registrant("JFW_Object_Draw_In_Order_2"," Location:vector3,Custom:int,BaseName:string,Count:int,Facing:float,Start_Number:int");
ScriptRegistrant<JFW_Object_Draw_Random> JFW_Object_Draw_Random_Registrant("JFW_Object_Draw_Random"," Location:vector3,Custom:int,BaseName:string,Count:int,Facing:float");
ScriptRegistrant<JFW_Play_Animation_Destroy_Object> JFW_Play_Animation_Destroy_Object_Registrant("JFW_Play_Animation_Destroy_Object","Animation:string,Subobject:string,FirstFrame:float,LastFrame:float,Blended:int");
ScriptRegistrant<JFW_Debug_Text_File> JFW_Debug_Text_File_Registrant("JFW_Debug_Text_File","Log_File:string,Description:string");
ScriptRegistrant<JFW_Power_Off> JFW_Power_Off_Registrant("JFW_Power_Off","Message_Off:int,Message_On:int");
ScriptRegistrant<JFW_Follow_Waypath> JFW_Follow_Waypath_Registrant("JFW_Follow_Waypath","Waypathid:int,Speed:float");
ScriptRegistrant<JFW_User_Settable_Parameters> JFW_User_Settable_Parameters_Registrant("JFW_User_Settable_Parameters","File_Name:string,Script_Name:string");
ScriptRegistrant<JFW_Change_Spawn_Character> JFW_Change_Spawn_Character_Registrant("JFW_Change_Spawn_Characher","Player_Type:int,Character:string");
ScriptRegistrant<JFW_HUD> JFW_HUD_Registrant("JFW_HUD","HUDTexture:string");
ScriptRegistrant<JFW_Reticle> JFW_Reticle_Registrant("JFW_Reticle","ReticleTexture1:string,ReticleTexture2:string");
ScriptRegistrant<JFW_HUD_INI> JFW_HUD_INI_Registrant("JFW_HUD_INI","HUDINI:string");
ScriptRegistrant<JFW_Scope> JFW_Scope_Registrant("JFW_Scope","Scope:int");
ScriptRegistrant<JFW_Screen_Fade_On_Enter> JFW_Screen_Fade_On_Enter_Registrant("JFW_Screen_Fade_On_Enter","Red:float,Green:float,Blue:float,Opacity:float");
ScriptRegistrant<JFW_Screen_Fade_On_Exit> JFW_Screen_Fade_On_Exit_Registrant("JFW_Screen_Fade_On_Exit","Red:float,Green:float,Blue:float,Opacity:float");
ScriptRegistrant<JFW_Screen_Fade_On_Custom> JFW_Screen_Fade_On_Custom_Registrant("JFW_Screen_Fade_On_Custom","Message:int,Red:float,Green:float,Blue:float,Opacity:float");
ScriptRegistrant<JFW_Screen_Fade_Custom_Timer> JFW_Screen_Fade_Custom_Timer("JFW_Screen_Fade_Custom_Timer","Message:int,Red:float,Blue:float,Green:float,Opacity:float,Time:float,TimerNum:int");
ScriptRegistrant<JFW_BHS_DLL> JFW_BHS_DLL_Registrant("JFW_BHS_DLL","");
ScriptRegistrant<JFW_Show_Info_Texture> JFW_Show_Info_Texture_Registrant("JFW_Show_Info_Texture","Time:float,TimerNum:int,Texture:string");
ScriptRegistrant<JFW_Wireframe_Mode> JFW_Wireframe_Mode_Registrant("JFW_Wireframe_Mode","Mode:int");
ScriptRegistrant<JFW_PT_Disable> JFW_PT_Disable_Registrant("JFW_PT_Disable","Player_Type:int,Type:int,Pos:int");
ScriptRegistrant<JFW_PT_Disable_Death> JFW_PT_Disable_Death_Registrant("JFW_PT_Disable_Death","Player_Type:int,Type:int,Pos:int");
ScriptRegistrant<JFW_Change_Radar_Map> JFW_Change_Radar_Map_Registrant("JFW_Change_Radar_Map","Scale:float,OffsetX:float,OffsetY:float,Texture:string");
ScriptRegistrant<JFW_Goto_Player_Timer> JFW_Goto_Player_Timer_Registrant("JFW_Goto_Player_Timer","Time:float,TimerNum:int,Speed:float,ArriveDistance:float");
ScriptRegistrant<JFW_Credit_Grant> JFW_Credit_Grant_Registrant("JFW_Credit_Grant","Credits:int,Delay:float");
ScriptRegistrant<JFW_PT_Hide> JFW_PT_Hide_Registrant("JFW_PT_Hide","Player_Type:int,Preset:string");
ScriptRegistrant<JFW_PT_Hide_Death> JFW_PT_Hide_Death_Registrant("JFW_PT_Hide_Death","Player_Type:int,Preset:string");
ScriptRegistrant<JFW_PT_Hide_Custom> JFW_PT_Hide_Custom_Registrant("JFW_PT_Hide_Custom","Player_Type:int,Preset:string,Message:int");
ScriptRegistrant<JFW_Follow_Waypath_Zone> JFW_Follow_Waypath_Zone_Registrant("JFW_Follow_Waypath_Zone","Waypathid:int,Speed:float,Preset:string");
