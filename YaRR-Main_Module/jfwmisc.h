/*	Renegade Scripts.dll
	Miscellanious scripts
	Copyright 2007 Vloktboky, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class JFW_User_Settable_Parameters : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Debug_Text_File : public ScriptImpClass {
	const char *filename;
	const char *description;
	FILE *f;
	time_t t2;
	time_t t;
	void Created(GameObject *obj);
	void Destroyed(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Sound_Heard(GameObject *obj,const CombatSound & sound);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Poked(GameObject *obj,GameObject *poker);
	void Entered(GameObject *obj,GameObject *enter);
	void Exited(GameObject *obj,GameObject *exit);
};

class JFW_Power_Off : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Follow_Waypath : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Object_Draw_In_Order : public ScriptImpClass {
	int currentmodelnumber;
	int currentmodelid;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Object_Draw_In_Order_2 : public ScriptImpClass {
	int currentmodelnumber;
	int currentmodelid;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Object_Draw_Random : public ScriptImpClass {
	int currentmodelid;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Play_Animation_Destroy_Object : public ScriptImpClass {
	void Created(GameObject *obj);
	void Animation_Complete(GameObject *obj,const char *anim);
};

class JFW_Animated_Effect : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Animation_Complete(GameObject *obj,const char *anim);
};

class JFW_Animated_Effect_2 : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Animation_Complete(GameObject *obj,const char *anim);
};

class JFW_Random_Animated_Effect : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Animation_Complete(GameObject *obj,const char *anim);
};

class JFW_Random_Animated_Effect_2 : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Animation_Complete(GameObject *obj,const char *anim);
};

class JFW_Fog_Create : public ScriptImpClass {
	void Created(GameObject *obj);
	void Destroyed(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_War_Blitz_Create : public ScriptImpClass {
	void Created(GameObject *obj);
	void Destroyed(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Goto_Object_On_Startup : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_BHS_DLL : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Scope : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_HUD : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Reticle : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_HUD_INI : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Screen_Fade_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Screen_Fade_On_Enter : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};

class JFW_Screen_Fade_On_Exit : public ScriptImpClass {
	void Exited(GameObject *obj,GameObject *exit);
};

class JFW_Screen_Fade_Custom_Timer : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Stealthable_Object : public ScriptImpClass {
	bool enabled;
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Object_Counter : public ScriptImpClass {
	int count;
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Change_Spawn_Character : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_Show_Info_Texture : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Wireframe_Mode : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_PT_Disable : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_PT_Disable_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Change_Radar_Map : public ScriptImpClass {
	void Created(GameObject *obj);
};
class JFW_Goto_Player_Timer : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
};

class JFW_Credit_Grant : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_PT_Hide_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_PT_Hide : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JFW_PT_Hide_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Follow_Waypath_Zone : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};
