/*	Renegade Scripts.dll
	GlowShaderClass
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

class GlowShaderClass : public ProgrammableShaderClass {
public:
	Vector3 LightDir;
	Vector4 GlowColor;
	Vector4 GlowAmbient;
	float GlowThickness;
	GlowShaderClass();
	void Load(ChunkLoadClass& cload);
	void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	void ApplyTexture(unsigned int stage);
	void ApplyTextureNull(unsigned int stage);
	void ApplyRenderState();
	void Release_Resources();
	void Reload_Resources();
	void Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
#endif
};

#define CHUNK_GLOW_SHADER 200
#define CHUNK_GLOW_SHADER_VARIABLES 210
#define MICROCHUNK_GLOW_SHADER_VARIABLES_LIGHTDIRECTION 1
#define MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWCOLOR 2
#define MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWAMBIENT 3
#define MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWTHICKNESS 4
