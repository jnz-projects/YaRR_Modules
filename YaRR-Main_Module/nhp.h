/*	Renegade Scripts.dll
	More scripts by NameHunter
	Copyright 2007 NameHunter, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class NHP_Smart_Targetting : public ScriptImpClass {
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Created(GameObject *obj);
};

class NHP_Guard_Bot : public ScriptImpClass {
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
};

class NHP_Construction_AI : public ScriptImpClass {
	int unitlim1;
	int unitlim2;
	int unitlim3;
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class NHP_Sensor_Array_Zone : public ScriptImpClass {
	bool SensorIsAlive;
	int count;
	int soundId;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Entered(GameObject *obj,GameObject *enter);
	void Exited(GameObject *obj,GameObject *exit);
	void Register_Auto_Save_Variables();
};

class NHP_Sensorable_Object : public ScriptImpClass {
	bool insideSensorZone;
	int sensorZone;
	void Created(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};
