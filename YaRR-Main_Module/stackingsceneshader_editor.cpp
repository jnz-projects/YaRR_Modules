/*	Renegade Scripts.dll
	StackingSceneShaderEditorClass
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <direct.h>
#include "scripts.h"
#include "shadereng.h"
#include <commdlg.h>
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader_scene.h"
#include "scene_shader_editor.h"
#include "stackingsceneshader.h"
#include "stackingsceneshader_editor.h"
#include "resource1.h"

StackingSceneShaderClass* StackingSceneShaderEditor::CurrentShader;

SceneShaderEditorClass* StackingSceneShaderClass::GetEditor()
{
	return new StackingSceneShaderEditor(this);
}

BOOL CALLBACK StackingSceneShaderDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	unsigned int x;
	int sel;
	char c[100];
	switch(Message)
	{
	case WM_INITDIALOG:
		SetDlgItemInt(hwnd,IDC_UID,StackingSceneShaderEditor::CurrentShader->UID,false);
		for (int i = 0; i < SceneShaderRegistrar->FactoryCount; i++)
		{
			SceneShaderFactory *factory = SceneShaderRegistrar->GetFactoryByIndex(i);
			if (factory->Chunk_ID == CHUNK_STACKINGSCENESHADER) // Sorry, we cant contain ourselves
			{
				continue;	
			}
			if (factory->Editable == true)
			{
				LRESULT res = SendDlgItemMessage(hwnd,IDC_PPSHADERTYPES,LB_ADDSTRING,0,(LPARAM)factory->Editor_Name);
				if ((res != LB_ERR) && (res != LB_ERRSPACE)) 
				{
					SendDlgItemMessage(hwnd,IDC_PPSHADERTYPES,LB_SETITEMDATA,(WPARAM)res,(LPARAM)i);
				}
			}
		}
		x = StackingSceneShaderEditor::CurrentShader->Shaders->Count();
		for (unsigned int i = 0;i < x;i++)
		{
			SendDlgItemMessage(hwnd,IDC_PPSHADERS,LB_ADDSTRING,0,(LPARAM)(*StackingSceneShaderEditor::CurrentShader->Shaders)[i]->Name);
		}
		break;
	case WM_CLOSE:
		EndDialog(hwnd, 1);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_NEWPPSHADER:
			sel = SendDlgItemMessage(hwnd,IDC_PPSHADERTYPES,LB_GETCURSEL,0,0);
			if (sel != -1)
			{
				GetDlgItemText(hwnd,IDC_PPSHADERNAME,c,100);
				if (c[0])
				{
					LRESULT registrarNum = SendDlgItemMessage(hwnd,IDC_PPSHADERTYPES,LB_GETITEMDATA,(WPARAM)sel,0);
					SceneShaderClass* shader = SceneShaderRegistrar->GetFactoryByIndex(registrarNum)->CreateNew();
					shader->PrivateData = shader->GetEditor();
					shader->Name = newstr(c);
					StackingSceneShaderEditor::CurrentShader->Shaders->Add(shader);
					SendDlgItemMessage(hwnd,IDC_PPSHADERS,LB_ADDSTRING,0,(LPARAM)c);
					((SceneShaderEditorClass*)shader->PrivateData)->Edit(hwnd);
				}
			}
			break;
		case IDC_EDITPPSHADER:
			sel = SendDlgItemMessage(hwnd,IDC_PPSHADERS,LB_GETCURSEL,0,0);
			if (sel != -1)
			{
				(*StackingSceneShaderEditor::CurrentShader->Shaders)[sel]->GetEditor()->Edit(hwnd);
			}
			break;
		case IDC_DELPPSHADER:
			sel = SendDlgItemMessage(hwnd,IDC_PPSHADERS,LB_GETCURSEL,0,0);
			if (sel != -1)
			{
				if((*StackingSceneShaderEditor::CurrentShader->Shaders)[sel])
				{
					delete (*StackingSceneShaderEditor::CurrentShader->Shaders)[sel];
					StackingSceneShaderEditor::CurrentShader->Shaders->Delete(sel);
				}
			}
			break;
		case IDOK:
			StackingSceneShaderEditor::CurrentShader->UID = GetDlgItemInt(hwnd,IDC_UID,NULL,false);
			EndDialog(hwnd, 1);
			break;
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

void StackingSceneShaderEditor::Edit(HWND ParentDialog)
{
	StackingSceneShaderEditor::CurrentShader = (StackingSceneShaderClass *)this->Shader;
	DialogBox(hInst, MAKEINTRESOURCE(IDD_GENERICPPS), ParentDialog,StackingSceneShaderDlgProc);
	StackingSceneShaderEditor::CurrentShader = 0;
}
