void YaRR_Load();
void YaRR_Unload();
void Load_Script_Commands(int *x);

extern "C" __declspec(dllexport) void Request_Load(const char *);
extern "C" __declspec(dllexport) void Request_Unload(HMODULE);
extern "C" __declspec(dllexport) void Request_Reload(HMODULE);

typedef void (*_Request_Load)(const char *);
typedef void (*_Request_Unload)(HMODULE);
typedef void (*_Request_Reload)(HMODULE);


DWORD GetModulePath(HINSTANCE hInst, LPTSTR pszBuffer, DWORD dwSize);

typedef void (* _Set_Script_Commands)(const int *, ScriptCommands *, _Request_Load, _Request_Unload, _Request_Reload);