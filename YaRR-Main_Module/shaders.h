/*	Renegade Scripts.dll
	Shaders.dll header file
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
extern "C"
{
void __declspec(dllimport) Release_Resources();
void __declspec(dllimport) Destroy_Resources();
void __declspec(dllimport) Reload_Resources();
void __declspec(dllimport) Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
void __declspec(dllimport) MapLoaded();
void __declspec(dllimport) MapUnloaded();
void __declspec(dllimport) FrameStart();
void __declspec(dllimport) FrameEnd();
void __declspec(dllimport) ScopeTrigger(bool enabled);
void __declspec(dllimport) ScopeChange(int scope);
void __declspec(dllimport) ShaderSet(int value,float value2);
void __declspec(dllimport) ShaderSet2(int value,Vector4 value2);
void __declspec(dllimport) DrawSkin(char *fvfcc);
void __declspec(dllimport) DrawRigid(char *fvfcc,char *MeshClass);
void __declspec(dllimport) Cleanup_HUD2();
void __declspec(dllimport) ReadHUDBits(INIClass *hudini);
void __declspec(dllimport) ReadRuntimeINI(const char *ini);
void __declspec(dllimport) UpdateHUD2();
void __declspec(dllimport) Update_Radar_Map(float scale,float offsetx,float offsety,const char *texture);
void __declspec(dllimport) Send_HUD_Number(int number);
void __declspec(dllimport) SetRenderState(D3DRENDERSTATETYPE State,DWORD Value);
void __declspec(dllimport) SetTextureStageState(DWORD Stage,D3DTEXTURESTAGESTATETYPE Type,DWORD Value);
void __declspec(dllimport) ScreenFadeRender();
void __declspec(dllimport) DynamicVBReset(bool b);
void __declspec(dllimport) SetViewport(CONST D3DVIEWPORT8* pViewport);
void __declspec(dllimport) SetRenderTarget(myIDirect3DSurface8* pRenderTarget,myIDirect3DSurface8* pNewZStencil);
void __declspec(dllimport) SetTransform(D3DTRANSFORMSTATETYPE State,const Matrix4 &matrix);
void __declspec(dllimport) CopySurface(myIDirect3DSurface8 *source,myIDirect3DSurface8 *dest);
SurfaceClass __declspec(dllexport) *DoFont(Font3DDataClass *Font,SurfaceClass *surface);
void __declspec(dllimport) SetScriptNotify(ScriptNotify notify);
}
