/*	Renegade Scripts.dll
	Base Class for shaders in the Shader Database Editor
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "shader.h"
#include "editorshader.h"
#include "editorshadermgr.h"

float GetDlgItemFloat(HWND hDlg, int id)
{
	char *pEnd;
	char szItemText[20];
	GetDlgItemText(hDlg, id, szItemText, 20);
	return (float)strtod(szItemText, &pEnd);
}

BOOL SetDlgItemFloat(HWND hDlg, int id, float f)
{
	char szItem[20];
	sprintf(szItem, "%f", f);
	return SetDlgItemText(hDlg, id, szItem);
}

EditorShaderFactory::EditorShaderFactory(unsigned int chunkId,const char *name)
{
	Chunk_ID = chunkId;
	Name = name;
	if (!TheEditorShaderManager)
	{
		TheEditorShaderManager = new EditorShaderManagerClass();
	}
	TheEditorShaderManager->Register_Editor_Shader_Factory(this);
}
