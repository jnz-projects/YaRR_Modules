
#ifdef WIN32
#define SCRIPTSAPI __declspec(dllexport)
#else
#define SCRIPTSAPI
#endif
typedef void (*srdf) (rdf);
typedef void (*ds) (ScriptClass *scr);
typedef ScriptClass *(*cs) (const char *);
typedef int (*gsc) ();
typedef char *(*gsn) (int);
typedef char *(*gspd) (int);
typedef bool (*ssc) (ScriptCommandsClass *);
#ifdef WIN32
HINSTANCE wwscripts;
#endif