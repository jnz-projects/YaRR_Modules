/*	Renegade Scripts.dll
	Base Class for shaders
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader.h"
#ifndef SDBEDIT
#include "shadermgr.h"
#endif

ProgrammableShaderClass::ProgrammableShaderClass()
{
	Name = 0;
	CRC32 = 0;
	Effect = 0;
	PixelShaderVersion = 0.0f;
	VertexShaderVersion = 0.0f;
	Filename = 0;
	Loaded = false;
}

ProgrammableShaderClass::~ProgrammableShaderClass()
{
	if (Effect)
	{
		Effect->Release_Ref();
		Effect = 0;
	}
	SAFE_DELETE_ARRAY(Name);
	SAFE_DELETE_ARRAY(Filename);
}

void ProgrammableShaderClass::Set_Filename(char *filename)
{
	Filename = newstr(filename);
}

const char *ProgrammableShaderClass::Get_Name()
{
	return Name;
}

unsigned long ProgrammableShaderClass::Get_Hash()
{
	return CRC32;
}

void ProgrammableShaderClass::Set_Name(const char *name)
{
	if (Name)
	{
		delete[] Name;
	}
	Name = newstr(name);
	CRC32 = CRC_Memory((unsigned char *)_strlwr(Name),strlen(Name),0);
}

void ProgrammableShaderClass::Load(ChunkLoadClass& cload)
{
	while (cload.Open_Micro_Chunk())
	{
		switch(cload.Cur_Micro_Chunk_ID())
		{
			case 1:
				Name = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(Name,cload.Cur_Micro_Chunk_Length());
				CRC32 = CRC_Memory((unsigned char *)_strlwr(Name),strlen(Name),0);
				break;
			case 3:
				Filename = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(Filename,cload.Cur_Micro_Chunk_Length());
				break;
		}
		cload.Close_Micro_Chunk();
	}
}

void ProgrammableShaderClass::Save(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_SHADER);
	csave.Begin_Micro_Chunk(1);
	csave.Write(Name,strlen(Name)+1);
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(3);
	csave.Write(Filename,strlen(Filename)+1);
	csave.End_Micro_Chunk();
	csave.End_Chunk();
}

#ifndef SDBEDIT
void ProgrammableShaderClass::Reset_Cache()
{

}

void ProgrammableShaderClass::Release_Resources()
{

}

void ProgrammableShaderClass::Reload_Resources()
{

}

bool ProgrammableShaderClass::Can_Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count)
{
	if (Loaded == false)
	{
		return false;
	}
	if (Effect->Initialized == false)
	{
		return false;
	}
	if (CRC32 == TheShaderManager->Get_Current_CRC32())
	{
		if ((render_state->material) && (render_state->material->Name[0]))
		{
			StateManager->SetTextureStageState(0,D3DTSS_TEXCOORDINDEX,0);
			StateManager->SetTextureStageState(1,D3DTSS_TEXCOORDINDEX,1);
			StateManager->SetTextureStageState(0,D3DTSS_TEXTURETRANSFORMFLAGS,0);
			StateManager->SetTextureStageState(1,D3DTSS_TEXTURETRANSFORMFLAGS,0);
			Render(primitive_type,start_index,polygon_count,min_vertex_index,vertex_count);
			return true;
		}
	}
	return false;
}

ShaderFactory::ShaderFactory(unsigned int chunkId)
{
	Chunk_ID = chunkId;
	if (!TheShaderManager)
	{
		TheShaderManager = new ShaderManagerClass();
	}
	TheShaderManager->Register_Shader_Factory(this);
}
#endif
