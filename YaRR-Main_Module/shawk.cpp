/*	Renegade Scripts.dll
	Scripts by Mark "Saberhawk" Sararu
	Copyright 2007 Mark "Saberhawk" Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <fstream>
#include "scripts.h"
#include "engine.h"
#include "shawk.h"

void SH_PCT_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		if (!Commands->Get_Player_Type(sender))
		{
			Display_NOD_Player_Terminal_Player(sender);
		}
		else
		{
			Display_GDI_Player_Terminal_Player(sender);
		}
	}
}

void SH_PCT_Powerup::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == CUSTOM_EVENT_POWERUP)
	{
		if (!Commands->Get_Player_Type(sender))
		{
			Display_NOD_Player_Terminal_Player(sender);
		}
		else
		{
			Display_GDI_Player_Terminal_Player(sender);
		}
	}
}

void SH_ConsoleCommand::Created(GameObject *obj) 
{
	Commands->Start_Timer(obj,this,0.1f,1);
}

void SH_ConsoleCommand::Timer_Expired(GameObject *obj,int number)
{
	char CommandRead[300];
	std::ifstream cc ("ConsoleCommand.txt");
	if (cc.is_open())
	{
		while (!cc.eof())
		{
			cc.getline(CommandRead,300);
			if (CommandRead[0] != '\0')
			{
				Console_Input(CommandRead);
			}
		}
		cc.close();
		if(!remove("ConsoleCommand.txt"))
		{
			Commands->Start_Timer(obj,this,0.1f,0);	
		}
	}

}

void SH_Spawn_Difficulty::Created(GameObject *obj)
{
	int level = Commands->Get_Difficulty_Level();
	if (level == 1)
	{
		if (Get_Int_Parameter("ObjectEasyEnabled") == 1)
		{
			GameObject *newObj = Commands->Create_Object(Get_Parameter("ObjectEasy"),Commands->Get_Position(obj));
			Commands->Set_Facing(newObj,Commands->Get_Facing(obj));
		}
	}
	else if (level == 2) 
	{
		if (Get_Int_Parameter("ObjectMediumEnabled") == 1)
		{
			GameObject *newObj = Commands->Create_Object(Get_Parameter("ObjectMedium"),Commands->Get_Position(obj));
			Commands->Set_Facing(newObj,Commands->Get_Facing(obj));
		}
	}
	else if (level == 3)
	{
		if (Get_Int_Parameter("ObjectHardEnabled") == 1)
		{
			GameObject *newObj = Commands->Create_Object(Get_Parameter("ObjectHard"),Commands->Get_Position(obj));
			Commands->Set_Facing(newObj,Commands->Get_Facing(obj));
		}
	}
}

FileVerificationController *VerifyController = 0;

FileVerificationController::FileVerificationController(const char *filename)
{
	FileINI = Get_INI(filename);
	if (!FileINI)
	{
		UseVerification = false;
		return;
	}
	FileCount = FileINI->Get_Int("VerifyFiles", "FileCount", 0);
	Files = new SimpleDynVecClass<unsigned long>(FileCount);
	Players = new SimpleDynVecClass<PlayerInfoStruct *>(The_Game()->MaxPlayers + 1);
	Files->ClearAll();
	Players->ClearAll();
	AddLoadLevelHook(FileVerificationController::MapLoadHook);
	AddGameOverHook(FileVerificationController::MapUnloadHook);
	AddPlayerJoinHook(FileVerificationController::PlayerJoinedHook);
	AddPlayerLeaveHook(FileVerificationController::PlayerLeftHook);
	/*
	//Reserved for possible future hook code update
	MapLoadHookNumber		= AddLoadLevelHook(FileVerificationController::MapLoadHook);
	MapUnloadHookNumber	= AddGameOverHook(FileVerificationController::MapUnloadHook);
	PlayerJoinedHookNumber	= AddPlayerJoinHook(FileVerificationController::PlayerJoinedHook);
	PlayerLeftHookNumber	= AddPlayerLeaveHook(FileVerificatonController::PlayerLeftHook);
	*/
}

FileVerificationController::~FileVerificationController()
{
	for (int i = 0; i < Players->Count(); ++i)
	{
		SAFE_DELETE((*Players)[i]);
	}
	Release_INI(FileINI);
	delete Files;
	delete Players;
	/*
	//Reserved for possible future hook code update
	RemoveLoadLevelHook(MapLoadHookNumber);
	RemoveGameOverHook(MapUnloadHookNumber);
	RemovePlayerJoinHook(PlayerJoinedHookNumber);
	RemovePlayerLeaveHook(PlayerLeftHookNumber);
	*/
}

FileVerificationController::PlayerInfoStruct::PlayerInfoStruct(int playerid, int filecount)
{
	FilesVerificationStatus = new SimpleDynVecClass<bool>(filecount);
	CallbackHooks = new SimpleDynVecClass<int>(filecount);
	FilesVerificationStatus->ClearAll();
	for (int i = 0; i < filecount; ++i)
	{
		NotifyCallbackData *callbackData = new NotifyCallbackData();
		callbackData->FileID = i;
		callbackData->PlayerID = playerid;

		ShaderNotifyStruct *notifystruct = new ShaderNotifyStruct();
		notifystruct->PlayerID = playerid;
		notifystruct->data = callbackData;
		notifystruct->ID = i + FILEIDBIAS;
		notifystruct->hook = &FileVerificationController::NotifyHook;
		(*CallbackHooks)[i] = AddShaderNotify(notifystruct);
	}
	ShaderNotifyStruct *notifystruct = new ShaderNotifyStruct();
	notifystruct->PlayerID = playerid;
	notifystruct->data = (void *)playerid;
	notifystruct->ID = SCRIPTEVENT_VERIFYDONE;
	notifystruct->hook = &FileVerificationController::VerifyDoneNotifyHook;
	int notifystructnum = AddShaderNotify(notifystruct);
	CallbackHooks->Add(notifystructnum);
}

FileVerificationController::PlayerInfoStruct::~PlayerInfoStruct()
{
	for (int i = 0; i < CallbackHooks->Count(); ++i)
	{
		RemoveShaderNotify((*CallbackHooks)[i]);
	}
	delete CallbackHooks;
	delete FilesVerificationStatus;
}

#pragma warning(disable: 6011)
void FileVerificationController::MapLoadHook()
{
	if (!VerifyController)
	{
		CreateFileVerifyController();
	}
	VerifyController->MapLoaded();
}
#pragma warning(default: 6011)
void FileVerificationController::MapUnloadHook()
{
	VerifyController->MapUnloaded();
}
void FileVerificationController::PlayerJoinedHook(int PlayerID, const char *)
{
	VerifyController->PlayerJoined(PlayerID);
}
void FileVerificationController::PlayerLeftHook(int PlayerID)
{
	VerifyController->PlayerLeft(PlayerID);
}
void FileVerificationController::NotifyHook(void *data, int parameter)
{
	NotifyCallbackData *notifydata = (NotifyCallbackData *) data;
	VerifyController->ProcessFile(notifydata->PlayerID, notifydata->FileID, parameter);
}
void FileVerificationController::VerifyDoneNotifyHook(void *data, int parameter)
{
	int player = (int) data;
	VerifyController->PlayerVerifyTimeout(player);
}

unsigned long FileVerificationController::GetFileCRC(int file)
{
	if (!FileINI)
	{
		return 0;
	}
	if (file > FileCount)
	{
		return 0;
	}
	if ((*Files)[file])
	{
		return (*Files)[file];
	}
	char buffer[256];
	char numBuffer[10];
	sprintf(numBuffer,"%d",file+1);
	FileINI->Get_String("VerifyFiles", numBuffer, "dummy", buffer, 256);
	FileClass *f = Get_Data_File(buffer);
	if ((f) && (f->Open(1)))
	{
		int fileData_size = f->Size();
		char *fileData = new char[fileData_size];
		f->Read(fileData,fileData_size);
		f->Close();
		(*Files)[file] = CRC_Memory((unsigned char *)fileData, fileData_size,0);
		Close_Data_File(f);
		delete[] fileData;
	}
	return (*Files)[file];
}

void FileVerificationController::MapLoaded()
{
	GameObject *obj = Find_Object_With_Script("SH_FileVerificationController");
	if (obj) 
	{
		UseVerification = true;
	}
	else
	{	
		UseVerification = false;
		return;
	}
	for (GenericSLNode* PlayerIter = PlayerList->HeadNode; (PlayerIter != NULL); PlayerIter = PlayerIter->NodeNext)
	{
		cPlayer *p = (cPlayer *)PlayerIter->NodeData;
		int id = p->PlayerId;
		(*Players)[id] = new PlayerInfoStruct(id,FileCount);
		(*Players)[id]->CRCSalt = Commands->Get_Random_Int(0, 65535);
		Commands->Send_Custom_Event(obj,obj,CUSTOM_PLAYERLOAD,id,0.0f);
		Commands->Send_Custom_Event(obj,obj,CUSTOM_STARTTIMEOUT,id,0.0f);
	}
}

void FileVerificationController::MapUnloaded()
{
	for (int i = 0; i < Players->Count(); ++i)
	{
		SAFE_DELETE((*Players)[i]);
	}
	Players->ClearAll();
}

void FileVerificationController::PlayerJoined(int PlayerID)
{
	if (!UseVerification)
	{
		return;
	}
	if ((*Players)[PlayerID])
	{
		delete (*Players)[PlayerID];
	}
	PlayerInfoStruct *temp = new PlayerInfoStruct(PlayerID, FileCount);
	temp->CRCSalt = Commands->Get_Random_Int(0,65535);
	(*Players)[PlayerID] = temp;
	GameObject *obj = Find_Object_With_Script("SH_FileVerificationController");
	if (obj)
	{
		Commands->Send_Custom_Event(obj,obj,CUSTOM_PLAYERLOAD,PlayerID,0.0f);
		Commands->Send_Custom_Event(obj,obj,CUSTOM_STARTTIMEOUT,PlayerID,0.0f);
	}
}

void FileVerificationController::PlayerLeft(int PlayerID)
{
	if (!UseVerification)
	{
		return;
	}
	if ((*Players)[PlayerID])
	{
		delete (*Players)[PlayerID];
	}
	(*Players)[PlayerID] = 0;
}

void FileVerificationController::PlayerVerifyTimeout(int PlayerID)
{
	PlayerInfoStruct *player = (*Players)[PlayerID];
	if (!player)
	{
		return;
	}
	int invalidFiles = FileCount;
	for (int i = 0; i < player->FilesVerificationStatus->Count(); ++i)
	{	
		if ((*(player->FilesVerificationStatus))[i] == true)
		{
			--invalidFiles;
		}
	}
	if (invalidFiles)
	{
		//They modified files, KILL THEM!
		char message[512];
		char command[512];
		sprintf(message,"%s was found having one or more invalid files and has been disconnected.",Get_Player_Name_By_ID(PlayerID));
		sprintf(command,"kick %d",PlayerID);
		Send_Message(106,0,106,message);
		Console_Output("%s was suspected of cheating\n",Get_Player_Name_By_ID(PlayerID));
		Console_Input(command);
	}
}

void FileVerificationController::VerifyPlayer(int PlayerID)
{
	PlayerInfoStruct *player = (*Players)[PlayerID];
	if (!player)
	{
		return;
	}
	GameObject *obj = Get_GameObj(PlayerID);
	player->CRCSalt = Commands->Get_Random_Int(0, 65535);
	Set_Shader_Number(obj, SHADEREVENT_SETVERIFYSALT, (float)player->CRCSalt);
	Set_Shader_Number(obj, SHADEREVENT_REQUESTVERIFY, 0);
}

void FileVerificationController::VerifyAllPlayers()
{
	for (int i = 0; i < Players->Count(); i++)
	{
		if ((*Players)[i])
		{
			GameObject *obj = Get_GameObj(i);
			(*Players)[i]->CRCSalt = Commands->Get_Random_Int(0, 65535);
			Set_Shader_Number(obj, SHADEREVENT_SETVERIFYSALT, (float)(*Players)[i]->CRCSalt);
			Set_Shader_Number(obj, SHADEREVENT_REQUESTVERIFY, 0);
		}
	}
}

void FileVerificationController::ProcessFile(int PlayerID, int file, unsigned long crc)
{
	PlayerInfoStruct *player = (*Players)[PlayerID];
	if (!player)
	{
		return;
	}
	unsigned long filecrc = GetFileCRC(file) + player->CRCSalt;
	if (crc != filecrc)
	{
		// File didn't match, OUT HE GOES.
		char message[512];
		char command[512];
		sprintf(message,"%s was found having one or more invalid files and has been disconnected.",Get_Player_Name_By_ID(PlayerID));
		sprintf(command,"kick %d",PlayerID);
		Send_Message(106,0,106,message);
		Console_Output("%s was suspected of cheating\n",Get_Player_Name_By_ID(PlayerID));
		Console_Input(command);
	} 
	else
	{
		(*(player->FilesVerificationStatus))[file] = true;	
	}
}

void CreateFileVerifyController()
{
	VerifyController = new FileVerificationController("verify.ini");
}

void DestroyVerifyFileController()
{
	SAFE_DELETE(VerifyController);
}

void SH_FileVerificationControllerScript::Created(GameObject *obj)
{
	if (!VerifyController)
	{
		CreateFileVerifyController();
	}
}
void SH_FileVerificationControllerScript::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == CUSTOM_MAPLOAD)
	{
		Commands->Start_Timer(obj,this,5.0f,TIMER_MAPLOAD);
	}
	else if (message == CUSTOM_PLAYERLOAD)
	{
		Commands->Start_Timer(obj,this,5.0f,(TIMER_PLAYERLOAD | param));
	}
	else if (message == CUSTOM_STARTTIMEOUT)
	{
		Commands->Start_Timer(obj,this,90.0f,(TIMER_PLAYERTIMEOUT | param));
	}
}

void SH_FileVerificationControllerScript::Timer_Expired(GameObject *sender, int number)
{
	int timer = (number & 0xFFFF0000)>>16; // Unpack the data from the number
	int parameter = (number & 0x0000FFFF);
	if (timer == (TIMER_MAPLOAD & 0xFFFF0000) >> 16)
	{
		VerifyController->VerifyAllPlayers();
	}
	else if (timer == (TIMER_PLAYERLOAD & 0xFFFF0000) >> 16)
	{
		VerifyController->VerifyPlayer(parameter);
	}
	else if (timer == (TIMER_PLAYERTIMEOUT & 0xFFFF0000) >> 16)
	{
		VerifyController->PlayerVerifyTimeout(parameter);
	}
}

ScriptRegistrant<SH_ConsoleCommand> SH_ConsoleCommand_Registrant("SH_ConsoleCommand","");
ScriptRegistrant<SH_PCT_Powerup> SH_PCT_Powerup_Registrant("SH_PCT_Powerup","");
ScriptRegistrant<SH_PCT_Custom> SH_PCT_On_Custom_Registrant("SH_PCT_On_Custom","");
ScriptRegistrant<SH_Spawn_Difficulty> SH_Spawn_Difficulty_Created("SH_Spawn_Difficulty","ObjectEasy:string,ObjectEasyEnabled:int,ObjectMedium:string,ObjectMediumEnabled:int,ObjectHard:string,ObjectHardEnabled:int");
ScriptRegistrant<SH_FileVerificationControllerScript> SH_FileVerificationControllerScript_Registrant("SH_FileVerificationController","INI:string");
