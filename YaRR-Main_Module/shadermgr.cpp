/*	Renegade Scripts.dll
	Shader Manager Class
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "shader.h"
#include "shadermgr.h"

ShaderManagerClass *TheShaderManager;

ShaderManagerClass::ShaderManagerClass()
{
	ShaderFactories = new SimpleDynVecClass<ShaderFactory *>(0);
	Shaders = 0;
	CurrentCRC32 = 0;
	MapShaderDatabaseOffset = 0;
	MapShaderDatabaseLoaded = false;
}

ShaderManagerClass::~ShaderManagerClass()
{
	if (Shaders)
	{
		Unload_Database();
	}
	SAFE_DELETE(ShaderFactories);
}


ProgrammableShaderClass *ShaderManagerClass::Load(ChunkLoadClass& cload)
{
	int x = ShaderFactories->Count();
	for (int i = 0;i < x;i++)
	{
		if ((*ShaderFactories)[i]->Chunk_ID == cload.Cur_Chunk_ID())
		{
			return (*ShaderFactories)[i]->Load(cload);
		}
	}
	char *c = new char[cload.Cur_Chunk_Length()];
	cload.Read(c,cload.Cur_Chunk_Length());
	delete[] c;
	return 0;
}

void ShaderManagerClass::Load_Database(FileClass *file)
{
	if (!Shaders) 
	{
		Shaders = new SimpleDynVecClass<ProgrammableShaderClass *>(0);
	}
	ChunkLoadClass cload(file);
	while (cload.Open_Chunk())
	{
		ProgrammableShaderClass *shader = Load(cload);
		if (shader)
		{
			Shaders->Add(shader);
		}
		cload.Close_Chunk();
	}
}

void ShaderManagerClass::Load_Map_Database(FileClass *file)
{
	MapShaderDatabaseOffset = Shaders->Count();
	Load_Database(file);
	MapShaderDatabaseLoaded = true;
}
void ShaderManagerClass::Unload_Database()
{
	if (!Shaders)
	{
		return;
	}
	int x = Shaders->Count();
	for (int i = 0;i < x;i++)
	{
		SAFE_DELETE((*Shaders)[i]);
	}
	SAFE_DELETE(Shaders);
}

void ShaderManagerClass::Unload_Map_Database()
{
	if ((!MapShaderDatabaseLoaded) || (!Shaders))
	{
		return;
	}
	int x = Shaders->Count();

	for (int i = x - 1;i > MapShaderDatabaseOffset - 1;i--)
	{
		SAFE_DELETE((*Shaders)[i]);
		Shaders->Delete(i);
	}
	MapShaderDatabaseOffset = 0;
	MapShaderDatabaseLoaded = false;
}

void ShaderManagerClass::Reset_Cache()
{
	if (!Shaders)
	{
		return;
	}
	int x = Shaders->Count();
	for (int i = 0;i < x;i++)
	{
		if ((*Shaders)[i])
		{
			(*Shaders)[i]->Reset_Cache();
		}
	}
}
void ShaderManagerClass::Release_Resources()
{
	if (!Shaders)
	{
		return;
	}
	int x = Shaders->Count();
	for (int i = 0;i < x;i++)
	{
		if ((*Shaders)[i])
		{
			(*Shaders)[i]->Release_Resources();
		}
	}
}

void ShaderManagerClass::Reload_Resources()
{
	if (!Shaders)
	{
		return;
	}
	int x = Shaders->Count();
	for (int i = 0;i < x;i++)
	{
		(*Shaders)[i]->Reload_Resources();
	}
}

bool ShaderManagerClass::Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count)
{
	if (!Shaders)
	{
		return false;
	}
	int x = Shaders->Count();
	for (int i = x - 1;i > -1;i--)
	{
		if ((*Shaders)[i])
		{
			if ((*Shaders)[i]->Can_Render(primitive_type,start_index,polygon_count,min_vertex_index,vertex_count))
			{
				return true;
			}
		}
	}
	return false;
}

void ShaderManagerClass::Register_Shader_Factory(ShaderFactory *loader)
{
	ShaderFactories->Add(loader);
}

unsigned long ShaderManagerClass::Get_Current_CRC32()
{
	return CurrentCRC32;
}

void ShaderManagerClass::Set_Current_Name(const char *name)
{
	CurrentCRC32 = CRC_Memory((unsigned const char *)_strlwr((char*)name),strlen(name),0);
}
