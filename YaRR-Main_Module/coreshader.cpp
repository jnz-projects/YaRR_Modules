/*	Renegade Scripts.dll
	Default shader class thats used when no other shader handles an object
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "shader.h"
#include "coreshader.h"
#include "shaderstatemanager.h"

CoreShaderClass::CoreShaderClass() : ProgrammableShaderClass()
{
	Set_Name("coreshader");
}

CoreShaderClass::~CoreShaderClass()
{
}

void CoreShaderClass::Load(ChunkLoadClass &cload)
{
	return;
}

void CoreShaderClass::Save(ChunkSaveClass &csave)
{
	return;
}

bool CoreShaderClass::Can_Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count)
{
	return true;
}

void CoreShaderClass::Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count)
{
	ApplyRenderState();
	Draw(primitive_type,start_index,polygon_count,min_vertex_index,vertex_count);
}

void CoreShaderClass::ApplyRenderState()
{
	StateManager->SetPixelShader(NULL);
	StateManager->SetVertexShader(NULL);
	if (*render_state_changed & WORLD_CHANGED)
	{
		Direct3DDevice->SetTransform(D3DTS_WORLD,(D3DMATRIX *)&render_state->world);
		*render_state_changed &= ~WORLD_CHANGED;
	}
	if (*render_state_changed & VIEW_CHANGED)
	{
		Direct3DDevice->SetTransform(D3DTS_VIEW,(D3DMATRIX *)&render_state->view);
		*render_state_changed &= ~VIEW_CHANGED;
	}
	if (*render_state_changed & LIGHT0_CHANGED)
	{
		ApplyLight(0);
		*render_state_changed &= ~LIGHT0_CHANGED;
	}
	if (*render_state_changed & LIGHT1_CHANGED)
	{
		ApplyLight(1);
		*render_state_changed &= ~LIGHT1_CHANGED;
	}
	if (*render_state_changed & LIGHT2_CHANGED)
	{
		ApplyLight(2);
		*render_state_changed &= ~LIGHT2_CHANGED;
	}
	if (*render_state_changed & LIGHT3_CHANGED)
	{
		ApplyLight(3);
		*render_state_changed &= ~LIGHT3_CHANGED;
	}
	if (*render_state_changed & MATERIAL_CHANGED)
	{
		ApplyMaterial();
		*render_state_changed &= ~MATERIAL_CHANGED;
	}
	if (*render_state_changed & VERTEX_BUFFER_CHANGED)
	{
		ApplyVertexBuffer();
		*render_state_changed &= ~VERTEX_BUFFER_CHANGED;
	}
	if (*render_state_changed & INDEX_BUFFER_CHANGED)
	{
		ApplyIndexBuffer();
		*render_state_changed &= ~INDEX_BUFFER_CHANGED;
	}
	if (*render_state_changed & SHADER_CHANGED)
	{
		ApplyShader();
		*render_state_changed &= ~SHADER_CHANGED;
	}
	ApplyTexture(0);
	ApplyTexture(1);
	*render_state_changed &= ~TEXTURE0_CHANGED & ~TEXTURE1_CHANGED;
}
void CoreShaderClass::ApplyTexture(unsigned int stage)
{
	if (!render_state->Textures[stage])
	{
		ApplyNullTexture(stage);
		return;
	}
	DebugEventStart(DEBUG_COLOR1,L"Texture[%u]<%S>::Apply",stage,render_state->Textures[stage]->Name);
	TextureClass *t = render_state->Textures[stage];
	if (!t->Initialized)
	{
		t->Init();
	}
	t->LastAccessed = *SyncTime;
	if (*TexturingEnabled)
	{
		StateManager->SetTexture(stage,t->D3DTexture->texture9);
	}
	else
	{
		ApplyNullTexture(stage);
		DebugEventEnd();
		return;
	}
	StateManager->SetSamplerState(stage,D3DSAMP_MINFILTER,MinTextureFilters[t->TextureMinFilter + stage * 4]);
	StateManager->SetSamplerState(stage,D3DSAMP_MAGFILTER,MagTextureFilters[t->TextureMagFilter + stage * 4]);
	StateManager->SetSamplerState(stage,D3DSAMP_MIPFILTER,MipMapFilters[t->MipMapFilter + stage * 4]);
	if (!t->UAddressMode)
	{
		StateManager->SetSamplerState(stage,D3DSAMP_ADDRESSU,D3DTADDRESS_WRAP);
	}
	else if (t->UAddressMode == 1)
	{
		StateManager->SetSamplerState(stage,D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
	}
	if (!t->VAddressMode)
	{
		StateManager->SetSamplerState(stage,D3DSAMP_ADDRESSV,D3DTADDRESS_WRAP);
	}
	else if (t->VAddressMode == 1)
	{
		StateManager->SetSamplerState(stage,D3DSAMP_ADDRESSV,D3DTADDRESS_CLAMP);
	}
	DebugEventEnd();
}
void CoreShaderClass::ApplyNullTexture(unsigned int stage)
{
	DebugEventStart(DEBUG_COLOR1,L"Texture[%u]<null>::Apply",stage);
	StateManager->SetTexture(stage,0);
	DebugEventEnd();
}
void CoreShaderClass::ApplyMaterial()
{
	if (!render_state->material)
	{
		ApplyDefaultMaterial();
		return;
	}
	DebugEventStart(DEBUG_COLOR1,L"Material<%S>::Apply",render_state->material->Name);
	VertexMaterialClass *m = render_state->material;
	StateManager->SetMaterial(m->Material);
	StateManager->SetRenderState(D3DRS_LIGHTING,m->UseLighting);
	StateManager->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE,m->AmbientColorSource);
	StateManager->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE,m->DiffuseColorSource);
	StateManager->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE,m->EmissiveColorSource);
	if (m->Mapper[0])
	{
		DebugEventStart(DEBUG_COLOR2,L"Mapper[0]<%i>::Apply",m->Mapper[0]->Mapper_ID());
		m->Mapper[0]->Apply(m->UVSource[0]);
		DebugEventEnd();
	}
	else
	{
		StateManager->SetTextureStageState(0,D3DTSS_TEXCOORDINDEX,m->UVSource[0]);
		StateManager->SetTextureStageState(0,D3DTSS_TEXTURETRANSFORMFLAGS,0);
	}
	if (m->Mapper[1])
	{
		DebugEventStart(DEBUG_COLOR2,L"Mapper[1]<%i>::Apply",m->Mapper[1]->Mapper_ID());
		m->Mapper[1]->Apply(m->UVSource[1]);
		DebugEventEnd();
	}
	else
	{
		StateManager->SetTextureStageState(1,D3DTSS_TEXCOORDINDEX,m->UVSource[1]);
		StateManager->SetTextureStageState(1,D3DTSS_TEXTURETRANSFORMFLAGS,0);
	}
	DebugEventEnd();
}
void CoreShaderClass::ApplyDefaultMaterial()
{
	DebugEventStart(DEBUG_COLOR1,L"Material<default>::Apply");
	StateManager->SetMaterial(DefaultMaterial);
	StateManager->SetRenderState(D3DRS_LIGHTING,0);
	StateManager->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE,0);
	StateManager->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE,0);
	StateManager->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE,0);
	StateManager->SetTextureStageState(0,D3DTSS_TEXCOORDINDEX,0);
	StateManager->SetTextureStageState(1,D3DTSS_TEXCOORDINDEX,1);
	StateManager->SetTextureStageState(0,D3DTSS_TEXTURETRANSFORMFLAGS,0);
	StateManager->SetTextureStageState(1,D3DTSS_TEXTURETRANSFORMFLAGS,0);
	DebugEventEnd();
}
void CoreShaderClass::ApplyShader()
{
	DebugEventStart(DEBUG_COLOR1,L"ShaderClass<%d>::Apply",render_state->shader.ShaderBits);
	ShaderClass *s = &(render_state->shader);
	s->Apply();
	DebugEventEnd();
}
void CoreShaderClass::ApplyLight(unsigned int light)
{
	if (render_state->LightEnable[light])
	{
		Set_Light(light,&render_state->Lights[light]);
	}
	else if (CurrentDX8LightEnables[light])
	{
		CurrentDX8LightEnables[light] = false;
		Direct3DDevice->LightEnable(light,false);
	}
}
void CoreShaderClass::ApplyVertexBuffer()
{
	if (render_state->vertex_buffer)
	{
		if ((!render_state->vertex_buffer_type) || (render_state->vertex_buffer_type == 2))
		{
			if (render_state->vertex_buffer->fvf_info)
			{
				Direct3DDevice->SetStreamSource(0,((DX8VertexBufferClass *)render_state->vertex_buffer)->Get_DX8_Vertex_Buffer(),0,render_state->vertex_buffer->FVF_Info().Get_FVF_Size());
				Direct3DDevice->SetFVF(render_state->vertex_buffer->FVF_Info().Get_FVF());
			}
			else
			{
				Direct3DDevice->SetStreamSource(0,((DX8VertexBufferClass *)render_state->vertex_buffer)->Get_DX8_Vertex_Buffer(),0,((DeclarationVertexBufferClass *)render_state->vertex_buffer)->DeclarationSize);
				Direct3DDevice->SetVertexDeclaration(((DeclarationVertexBufferClass *)render_state->vertex_buffer)->VertexDecl);
			}
		}
	}
	else
	{
		Direct3DDevice->SetStreamSource(0,0,0,0);
	}
}
void CoreShaderClass::ApplyIndexBuffer()
{
	if (render_state->index_buffer)
	{
		if ((!render_state->index_buffer_type) || (render_state->index_buffer_type == 2))
		{
			Direct3DDevice->SetIndices(((DX8IndexBufferClass *)render_state->index_buffer)->Get_DX8_Index_Buffer());
		}
	}
	else
	{
		Direct3DDevice->SetIndices(0);
	}
}
