/*	Renegade Scripts.dll
	Glass Shader Class
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

class GlassShaderClass : public ProgrammableShaderClass {
private:
#ifndef SDBEDIT
	IDirect3DTexture9 *texture_Fresnel;
	CubeTextureClass *texture_Environment;
	void ApplyShaderConstants();
	void LoadTextures();
	void ReleaseTextures();
	void ApplyRenderState();
	void ApplyTransform();
#endif
public:
	GlassShaderClass();
	~GlassShaderClass();
	float ReflectStrength; // Range: 0 to 2
	float RefractStrength; // Range: 0 to 2
	char *EnvironmentMap; // Name of the environment map 
	void Load(ChunkLoadClass& cload);
	void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	void Release_Resources();
	void Reload_Resources();
	void Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
#endif
};

#define CHUNK_GLASS_SHADER 260
#define MC_GLASS_MATERIALNAME 1
#define MC_GLASS_FXFILENAME 2
#define MC_GLASS_REFLECTSTRENGTH 3
#define MC_GLASS_REFRACTSTRENGTH 4
#define MC_GLASS_ENVIRONMENTMAP 5
