/*	Renegade Scripts.dll
	StackingSceneShaderClass
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class StackingSceneShaderClass: public SceneShaderClass
{
protected:
#ifndef SDBEDIT
	bool Validated;
	bool ValidationResult;
#endif
	void LoadVariables(ChunkLoadClass& cload);
	void LoadShaders(ChunkLoadClass& cload);
	void SaveVariables(ChunkSaveClass& csave);
	void SaveShaders(ChunkSaveClass& csave);
public:
	SimpleDynVecClass<SceneShaderClass *> *Shaders;
	StackingSceneShaderClass();
	~StackingSceneShaderClass();
	void Load(ChunkLoadClass& cload);
	void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	void Initialize();
	bool Validate();
	void OnDeviceLost();
	void OnDeviceReset();
	void OnCustom(unsigned int eventid, float parameter);
	void Render(SceneShaderRenderInfo *render);
#else
	SceneShaderEditorClass* GetEditor();
#endif
};
#define CHUNK_STACKINGSCENESHADER 1050
#define CHUNK_STACKINGSCENESHADER_VARIABLES 1051
#define CHUNK_STACKINGSCENESHADER_SHADERS 1052
