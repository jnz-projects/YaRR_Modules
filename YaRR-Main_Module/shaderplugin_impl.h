/*	Renegade Scripts.dll
	Default implementation of shaderplugin.h to be used when no plugin is loaded
	Copyright 2007 Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module 
	that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/


class DefaultShaderPluginClass: public ShaderPluginClass
{
public:
	DefaultShaderPluginClass();
	HRESULT OnReleaseResources();	
	HRESULT OnReloadResources();
	HRESULT OnDestroyResources();
	HRESULT OnMapLoad();
	HRESULT OnMapUnload();
	HRESULT OnCustom(unsigned int eventid, float parameter);
	HRESULT OnCustom(unsigned int eventid, Vector4 parameter);
	HRESULT OnFrameStart();
	HRESULT OnFrameEnd();
	HRESULT OnScreenFadeRender();
	HRESULT OnRender(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);								
};