/*	Renegade Scripts.dll
	D3DXEffect Wrapper and related loaders
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

enum EffectType {
	EFFECTTYPE_NONE = 0x0,
	EFFECTTYPE_VERTEX = 0x1,
	EFFECTTYPE_PIXEL = 0x2,
	EFFECTTYPE_BOTH = 0x3,
};
class EffectLoadTaskClass;
class EffectClass: public RefCountClass
{
	friend class EffectLoadTaskClass;
protected:
	ID3DXEffect *D3DEffect;
public:
	EffectClass();
	EffectClass(char *name);
	~EffectClass();
	char *Name;
	bool Initialized;
	EffectLoadTaskClass* EffectLoadTask;
	void Delete_This();
	HRESULT Init();
	HRESULT ForceReset();
	HRESULT Begin(unsigned int *passes, DWORD flags);
	HRESULT BeginPass(unsigned int pass);
	HRESULT CommitChanges();
	HRESULT EndPass();
	HRESULT End();
	HRESULT OnDeviceLost();
	HRESULT OnDeviceReset();
	HRESULT ValidateTechnique(D3DXHANDLE technique);
	HRESULT SetArrayRange(D3DXHANDLE parameter,unsigned int start,unsigned int stop);
	HRESULT SetBool(D3DXHANDLE parameter, BOOL value);
	HRESULT SetBoolArray(D3DXHANDLE parameter, const BOOL* values, unsigned int count);
	HRESULT SetFloat(D3DXHANDLE parameter, float value);
	HRESULT SetFloatArray(D3DXHANDLE parameter, const float* values, unsigned int count);
	HRESULT SetInt(D3DXHANDLE parameter, int value);
	HRESULT SetIntArray(D3DXHANDLE parameter, const int* values, unsigned int count);
	HRESULT SetMatrix(D3DXHANDLE parameter, const Matrix4* value);
	HRESULT SetMatrixArray(D3DXHANDLE parameter, const Matrix4* values, unsigned int count);
	HRESULT SetMatrixPointerArray (D3DXHANDLE parameter, const Matrix4** values, unsigned int count);
	HRESULT SetMatrixTranspose(D3DXHANDLE parameter, const Matrix4* value);
	HRESULT SetMatrixTransposeArray(D3DXHANDLE parameter, const Matrix4* values, unsigned int count);
	HRESULT SetMatrixTransposePointerArray (D3DXHANDLE parameter, const Matrix4** values, unsigned int count);
	HRESULT SetString(D3DXHANDLE parameter, const char* value);
	HRESULT SetTechnique(D3DXHANDLE hTechnique);
	HRESULT SetTexture(D3DXHANDLE parameter, IDirect3DBaseTexture9* value);
	HRESULT SetValue(D3DXHANDLE parameter, const void* data, unsigned int bytes);
	HRESULT SetVector(D3DXHANDLE parameter, const Vector4* value);
	HRESULT SetVectorArray(D3DXHANDLE parameter, const Vector4* values, unsigned int count);
	D3DXHANDLE GetAnnotation(D3DXHANDLE object, unsigned int index);
	D3DXHANDLE GetAnnotationByName(D3DXHANDLE object, const char* name);
	HRESULT GetBool(D3DXHANDLE parameter, BOOL* value);
	HRESULT GetBoolArray(D3DXHANDLE parameter, BOOL* values, unsigned int count);
	HRESULT GetDesc(D3DXEFFECT_DESC* desc);
	HRESULT GetFloat(D3DXHANDLE parameter, float* value);
	HRESULT GetFloatArray(D3DXHANDLE parameter, float* values, unsigned int count);
	D3DXHANDLE GetFunction(unsigned int index);
	D3DXHANDLE GetFunctionByName(const char* pName);
	HRESULT GetFunctionDesc(D3DXHANDLE function, D3DXFUNCTION_DESC* desc);
	HRESULT GetInt(D3DXHANDLE parameter, int* value);
	HRESULT GetIntArray(D3DXHANDLE parameter, int* values, unsigned int count);
	HRESULT GetMatrix(D3DXHANDLE parameter, Matrix4* value);
	HRESULT GetMatrixArray(D3DXHANDLE parameter, Matrix4* values, unsigned int count);
	HRESULT GetMatrixPointerArray(D3DXHANDLE parameter, Matrix4** values, unsigned int count);
	HRESULT GetMatrixTransposeArray(D3DXHANDLE parameter, Matrix4* values, unsigned int count);
	HRESULT GetMatrixTransposePointerArray(D3DXHANDLE parameter, Matrix4** values, unsigned int count);
	D3DXHANDLE GetParameter(D3DXHANDLE parameter, unsigned int index);
	D3DXHANDLE GetParameterByName(D3DXHANDLE parameter, const char *name);
	D3DXHANDLE GetParameterBySemantic(D3DXHANDLE parameter, const char* semantic);
	HRESULT GetParameterDesc(D3DXHANDLE parameter, D3DXPARAMETER_DESC* desc);
	D3DXHANDLE GetParameterElement(D3DXHANDLE parameter, unsigned int index);
	D3DXHANDLE GetPass(D3DXHANDLE technique, unsigned int index);
	D3DXHANDLE GetPassByName(D3DXHANDLE technique, const char* name);
	HRESULT GetString(D3DXHANDLE parameter, const char** string);
	D3DXHANDLE GetTechnique(unsigned int index);
	D3DXHANDLE GetTechniqueByName(const char* name);
	HRESULT GetTechniqueDesc(D3DXHANDLE technique, D3DXTECHNIQUE_DESC* desc);
	HRESULT GetTexture(D3DXHANDLE parameter, IDirect3DBaseTexture9** value);
	HRESULT GetValue(D3DXHANDLE parameter, void* data, unsigned int bytes);
	HRESULT GetVector(D3DXHANDLE parameter, Vector4* value);
	HRESULT GetVectorArray(D3DXHANDLE parameter, Vector4* values, unsigned int count);
};
class EffectLoadTaskClass: public ResourceLoadTask
{
protected:
	void LoadResource();
public:	
	EffectLoadTaskClass(EffectClass* effect)
	{
		Data = effect;
	}
};
class EffectIncludeFileClass : public ID3DXInclude
{
private:
	FileClass *File;
public:
	HRESULT __stdcall Open(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID * ppData, UINT * pBytes);
	HRESULT __stdcall Close(LPCVOID pData);
};
