; ************************* Scripts by Francois-G. Auclair aka TheKGBspy *************************

 - Boris_the_invincible@hotmail.com
 - TheKGBspy@timeofwar.com

====================================================================================================

; ************************* [Script Name] Ra2Ven_DemoTruck

====================================================================================================

 [Description]

 - Script that will make a functional demo truck even if friendly fire is off.

 - To make the vehicle work, make sure you don't put an Explosion_Preset on the vehicle. That means the Explosion_Preset setting in Level Edit, should be left blank.

 - The Destroy/Explosion animation on vehicle death should be set within the script. The primary weapon should be the demo charge.

 - This work same way as the SSM, but the script looks at when the vehicle is being damaged. If it's the driver, the vehicle grants the damage set in the script using the warhead set in the script and doesn't do the Demo_Explosion.

 - The Demo_Explosion is used when the vehicle is killed by someone else who is not the driver.

 [Parameters]

 - Dammage (Damage given to destroy the vehicle)

 - Warhead (Warhead used to destroy the vehicle)

 - Demo_Explosion (Explosion to create when the vehicle is killed by somebody other than the driver)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] Ra2Ven_Deploy_Animation

====================================================================================================

 [Description]

 - Attach this script to an object that will act as the deploy animation.

 [Parameters]

 - Deployed_Tank_Preset (Preset name of the deployed tank)

 - oldTnk_Warhead (Warhead used to destroy the animation)

 - oldTnk_Dammage (Damage used to destroy the animation)

 - Animation_Time (Time before destroying the animation in order to spawn the deployed tank)

 - TimerNumber (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] Ra2Ven_Deployable_Vehicle

====================================================================================================

 [Description]

 - Script to make a deployable vehicle.

 [Parameters]

 - Animation_Preset (Preset name of the animation. Like a standard vehicle, but with an animation and the script Ra2Ven_Deploy_Animation attached)

 - oldTnk_Warhead (Warhead used to destroy the animation)

 - oldTnk_Dammage (Damage used to destroy the animation)

 - Explosion_preset (This is used as the destroy animation)

 [NOTES]

 - The Explosion_preset is used as a destruction animation. It is set in the script instead of Level Edit because when it is being destroyed for a deploy animation, the tank might have an explosion animation, which may look odd because you are deploying and not destroying the tank. Setting the Explosion_preset allows you to use the destruction animation once the tank is destroyed, not while being deployed. The deploy logic that uses self attack, works the same way as the Reborn one.

 - For best results, all your deploy / deployed / undeploy animations should have their origin positioned at same place within the model, and make sure that each worldbox has the same size. If you deploy any ground unit to another ground unit, make sure that the origin of the deployable object has the same Z value from the ground, and that all models have the same origin height from the ground. Otherwise, you could run into some bugs.

 - To undeploy, use the deploy script again to deploy your deployed vehicle back into its undeployed state (Use two deploy scripts, one to deploy and another to undeploy). Also, what's new to this script than the Reborn one? You don't have to create a new warhead or armor in the armor.ini to deploy a tank. This version also fixes an issue while on uneven terrain, where the vehicle could get stuck during deployment. It also fixes a minor issue while transfering the heatlh and the shield values. Instead, now it sends the ratio. Say that the normal vehicle has a maximum shield strength of 300, but it currently has 150. The deployed Vehicle could have a maximum shield strength of 500. In that case, the shield value would be 250. 150/300 is a ratio of 1:2, therefore the deployed vehicle would be 1:2, or 250/500. (150/300) X 500 = 250.

====================================================================================================

; ************************* [Script Name] Ra2Ven_Dplbl_Vhcls_Cursor

====================================================================================================

 [Description]

 - This is the best deploy script to date. The player won't die anymore, and it allows for an action cursor. However, the player will still need to exit the vehicle.

 [Parameters]

 - Cursor_preset (Object name for the cursor object, the deploy icon)

 - CursorBoneName (Name of the bone to spawn the cursor object at)

 - CursorExplosion (Explosion used to kill the cursor object)

 - Animation_Preset (Preset name of the animation. Like a standard vehicle, but with an animation and the script Ra2Ven_Deploy_Animation attached)

 - oldTnk_Warhead (Warhead used to destroy the animation)

 - oldTnk_Dammage (Damage used to destroy the animation)

 - Explosion_preset (This is used as the destroy animation)

 [NOTES]

 - It works almost the same as the standard deploy script Ra2Ven_Deployable_Vehicle except it includes a cursor and some bug fixes. It still uses Ra2Ven_Deploy_Animation like in Ra2Ven_Deployable_Vehicle. Player will not get killed on deploy.

 - Make the cursor in a separate 3D model, then export as w3dcursor.w3d with no collision flags. Your vehicle can use the origin bone or another bone if you did your own vehicles. In Level Edit, created a new object that will be your cursor preset. It can be a preset like that of a turret (DecorationPhys). Unteamed, no radar blip, not targetable, and animation as the name of your W3d file. Do not forget to set the armor to a special armor so that the cursor isn't destroyed by any other explosion than your CursorExplosion setting. Have fun with this one!

====================================================================================================

; ************************* [Script Name] Ra2Ven_FireAnimation

====================================================================================================

 [Description]

 - This scripts allows a firing animation to play while shooting from a vehicle.

 [Parameters]

 - ParentBone (Name of the parent bone of the muzzle bone to check if the player has fired. Barrel bone if it's a tank, turret bone if the weapon doesn't use barrels)

 - MuzzleBone (Name of the muzzle bone (Either MuzzleA0,MuzzleA1,MuzzleB0,MuzzleB1 depend on how the modeled has been boned)

 - Animation (Name of the main fire or reload animation - see additional notes)

 - FirstFrame (First frame of the fire/reload animation)

 - LastFrame (Last frame of the fire/reload animation)

 - AfAnimation (Name of the after fire animation)

 - AfFirstFrame (First frame of after fire animation)

 - AfLastFrame (Last frame of after fire animation)

 - Time (I strongly suggest 0.100)

 - TimerNumber (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - Not an easy script depending on what you want. The script works in 2 ways: Reload only mode, and continious fire mode (plus reload/end-fire animation).

 - Reload Only Mode: To use this mode, leave the AfAnimation to "none." This mode is useful for ONE rocket type vehicle (such as V2 or V3), or any "one shot, reload, then I can shoot" weapon type on a vehicle. The script detects if the user has fired by looking at the muzzle bone and seeing if it has moved due to the recoil effect. Settings should reflect upon the following:

 - Recoil time must be a smaller value than the reload time.
 - For good results, recoil scale should be set to 1.
 - An animation time frame less than the reload time.

 - "Here is an example I did on my V3. My V3 reload animation is 5 seconds, and my weapon reload time is 6 seconds. My recoil time is set to 3, and my recoil scale set to 2. My weapon can only hold 1 bullet at time. That's all!"

 - Continious Fire (plus reload/end-fire animation) Mode: This mode works almost the same way as reload only mode (using the recoil method to detect if the player fired or not). However, it requires a bit more effort to make it work just right. It's useful for gattling type vehicles, or vehicles that carry a lot of ammunition. It plays an animation each time the player shoots, stops shooting, or is currently reloading (a reload or end fire animation). First, you need to set up a weapon for your vehicle, similar to reload only mode. Except the reload time doesn't need to be bigger than the end fire animation time length. You may decide how you want it, but it's highly suggested to make it the exact time length. Make the recoil time a bit slower than the timer value and set the recoil scale to 1.

 - The weapon ammunition also needs to be edited in order to make the animation look good. Edit the rate of fire so that the time is near to or exactly the same as the fire animation.

 - Here are some examples of the settings used on my Kirov:

 - Reload time is 1.0 seconds
 - Recoil time is 0.2 seconds
 - Recoil scale is  1.0
 - The weapon's ammunition rate of fire is 0.620 (remember, the higher the number is, the faster your bullets fire)
 - My fire animation has 46 frames
 - My after fire animation has 11 frames

 - With these settings you should be able to set things up correctly. Experiment with some of the settings, and you will achieve a nice effect.

 - Take care with this script, and be sure to note the following:

 - If you have other animations attached to the vehicle you're working with (such as a walking animation), this script will stop the current animation and play the fire animation. This means that only one animation at a time can play on a vehicle. The other wheel or rotor bones, won't have any problems.

 - If you are going to have a dual barreled gattling vehicle, set up your animation to include both guns spinning at the same time. A second script is not required.

====================================================================================================

; ************************* [Script Name] Ra2Ven_FireAnimation2

====================================================================================================

 [Description]

 - This scripts allows a firing animation to play while shooting from a vehicle. Similar to Ra2Ven_FireAnimation, but instead it alternates between 2 different firing animations.

 [Parameters]

 - ParentBone (Name of the parent bone of the muzzle bone to check if the player has fired. Barrel bone if it's a tank, turret bone if the weapon doesn't use barrels)

 - MuzzleBone (Name of the muzzle bone (Either MuzzleA0,MuzzleA1,MuzzleB0,MuzzleB1 depend on how the modeled has been boned)

 - Animation1 (First animation, name of the main fire or reload animation - see additional notes)

 - FirstFrame1 (First animation, first frame of the fire/reload animation)

 - LastFrame1 (First animation, last frame of the fire/reload animation)

 - Animation2 (Second animation, name of the main fire or reload animation - see additional notes)

 - FirstFrame2 (Second animation, first frame of the fire/reload animation)

 - LastFrame2 (Second animation, last frame of the fire/reload animation)

 - AfAnimation (Name of the after fire animation)

 - AfFirstFrame (First frame of after fire animation)

 - AfLastFrame (Last frame of after fire animation)

 - Time (I strongly suggest 0.100)

 - TimerNumber (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - Read the documentation for Ra2Ven_FireAnimation2 above.

 - For the version of this script with 2 alternating animations, twiddle around with the parameters to get it right. The logic of this script also requires a reload animation even if it does nothing.

====================================================================================================

; ************************* [Script Name] Ra2Ven_MirageTank

====================================================================================================

 [Description]

 - While stopped, it turns the attached vehicle invisible and replaces it with 1 of 4 random tree models. The effect wears off during movement.

 [Parameters]

 - Tree1_Preset (First tree object)

 - Tree2_Preset (Second tree object)

 - Tree3_Preset (Third tree object)

 - Tree4_Preset (Fourth tree object)

 - Explosion_Preset (Explosion used to destroy the tree object after moving)

 - Time (The transition delay before turning invisible)

 - TimerNumber (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - You must have at least 1 tree object defined, or else it will crash.

====================================================================================================

; ************************* [Script Name] Ra2Ven_OccupentWeapon

====================================================================================================

 [Description]

 - Attach that script to a vehicle that will change his weapon when a certain unit get in.

 [Parameters]

 - Character1 (Character preset that will use PowerUp1 and Weapon1)

 - PowerUp1 (Powerup given to the tank that has Weapon1)

 - Weapon1 (Weapon name that is in Powerup1. Used for the selection of the weapon)

 - Character2 (Character preset that will use PowerUp2 and Weapon2)

 - PowerUp2 (Powerup given to the tank that has Weapon2)

 - Weapon2 (Weapon name that is in Powerup2. Used for the selection of the weapon)

 - Character3 (Character preset that will use PowerUp3 and Weapon3)

 - PowerUp3 (Powerup given to the tank that has Weapon3)

 - Weapon3 (Weapon name that is in Powerup3. Used for the selection of the weapon)

 - Character4 (Character preset that will use PowerUp4 and Weapon4)

 - PowerUp4 (Powerup given to the tank that has Weapon4)

 - Weapon4 (Weapon name that is in Powerup4. Used for the selection of the weapon)

 - DefPowerUp (Default powerup given when the character name is not listed above)

 - DefWeapon (Default weapon name that is in DefPowerUp)

 [NOTES]

 - When using this script, it is essential that both the primary and secondary ammo fields are filled in. If you don't want secondary fire, set both to the same ammo. If someone activates secondary fire and you haven't done this, Renegade will crash.

 - Also, sound issues may arrise if the fire sound for the weapon you are using has a loop setting of anything other than 1.

====================================================================================================

; ************************* [Script Name] Ra2Ven_RandomTree

====================================================================================================

 [Description]

 - Want to add a spicy feature to your map? Then this script is for you! Attach this script to a daves arrow and set up 4 different object as tree models. Every time the map loads, there will be a different tree, or no tree. You even use more than just trees, thing like rocks, destroyed vehicles, etc.

 [Parameters]

 - Tree1_Preset (First tree object)

 - Tree2_Preset (Second tree object)

 - Tree3_Preset (Third tree object)

 - Tree4_Preset (Fourth tree object)

 [NOTES]

 - The tree object is spawned at the origin of the object that is used as the random tree generator.

====================================================================================================

; ************************* [Script Name] Ra2Ven_TurretSound

====================================================================================================

 [Description]

 - Attach this script to a vehicle with a turret. It will play a sound upon turret rotation.

 [Parameters]

 - TurretSoundObj (Preset name of the invisible object with Ra2Ven_TurretSoundObj)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] Ra2Ven_TurretSoundObj

====================================================================================================

 [Description]

 - Attach this script to an invisible object that will be used to detect when the turret rotates. You can use a daves arrows, but uncheck IsEditorObject and check IsInvisible. Set both armor and skin type to Blamo. The explosion is used to destroy the object, so use the Blamo Killer warhead for your explosion.

 [Parameters]

 - RotationAngleLimit (The maximum angle rotation)

 - Rotate_3dSound (Name of the 3D sound object to play)

 - Explosion_Preset (Explosion used to destroy the 3D sound object)

 - Time (I suggest to set to half of the Wav_Length value for best results)

 - TimerNumber1 (Set to an unique number if the object has more than one script using a timer)

 - Wav_Length (Set in float, the length of the wav file used to make the turret rotation sound)

 - TimerNumber2 (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] Ra2Ven_VehicleFalling

====================================================================================================

 [Description]

 - Gives damage to falling vehicles after a certain Z value.

 [Parameters]

 - ZValueBeforeStartFallingMode (The distance on the Z axis that is needed to put the vehicle in falling mode)

 - ImpactExplosion_Preset (The explosion to display on vehicle impact, as well as any hit sounds)

 - Warhead (Warhead used to damage the vehicle)

 - DammageMultiplier (Used as a damage multiplier)

 - Time (How accurate is the script on a time scale, if X<1 then that would make it more accurate)

 - TimerNumber (Set to an unique number if the object has more than one script using a timer)

 [NOTES]

 - The equation used for falling damage is: Value = (fabs(position.Z-zpos)/2.5)*DammageMultiplier).

 - The script will look to see if the vehicle is falling on the Z axis for a long distance (if lastZvalue - currentZ >= ZValueBeforeStartFallingMode). Once the vehicle is in falling mode it keeps track of the Z coordinate decent. Then it applies damage until the Z movement is suddenly stopped. When that happens, it will also apply the warhead damage.

 - The difference between ImpactExplosion_Preset and Warhead along with the DammageMultiplier is the damage value on ImpactExplosion_Preset has a fixed value defined by the user. The combination of Warhead with DammageMultiplier allows to compute the damage given on impact: The more the vehicle fell, the more damage that is applied.