; ************************* Scripts by TimeFX  *************************

 - TimeFX@HyO2.net
 - #TimeFX on irc.quakenet.org

====================================================================================================

; ************************* [Script Name] TFX_Replace_When_Repaired

====================================================================================================

 [Description]

 - Creates an object at the position where another object has been repaired to 100% health & armor, and destroys the repaired object. This is useful if you want to drop a player vehicle where a destroyed one has been repaired.

 [Parameters]

 - PresetName (The preset that will be created)

 - DropHeight (The height above the repaired object where the new one will be created)

 - SameFacing (If "1" the dropped object will have the same facing as the repaired one)

 - HealthPercent (The dropped object will have the given percentage of its maximum health)

 - ArmorPercent (The dropped object will have the given percentage of its maximum armor)

 [NOTES]

 - NONE

====================================================================================================

; ************************* [Script Name] TFX_Spawn_When_Killed

====================================================================================================

 [Description]

 - Creates an object at the position where another object has been killed. This is useful if you want to drop a destroyed vehicle where another one has been killed.

 [Parameters]

 - PresetName (The preset that will be created)

 - DropHeight (The height above the destroyed object where the new one will be created)

 - SameFacing (If "1" the dropped object will have the same facing as the destroyed one)

 - HealthPercent (The dropped object will have the given percentage of its maximum health)

 - ArmorPercent (The dropped object will have the given percentage of its maximum armor)

 [NOTES]

 - NONE