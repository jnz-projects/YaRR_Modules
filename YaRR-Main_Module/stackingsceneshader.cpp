/*	Renegade Scripts.dll
	StackingSceneShaderClass
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader_scene.h"
#include "stackingsceneshader.h"

StackingSceneShaderClass::StackingSceneShaderClass()
{
	Shaders = new SimpleDynVecClass<SceneShaderClass *>(0);
#ifndef SDBEDIT	
	Validated = false;
	ValidationResult = false;
#endif
	SceneShaderClass::SceneShaderClass();
}
StackingSceneShaderClass::~StackingSceneShaderClass()
{
	for (int i = 0; i < Shaders->Count(); i++)
	{
		delete (*Shaders)[i]; 
		(*Shaders)[i] = 0;
	}
	delete Shaders;
	Shaders = 0;
	SceneShaderClass::~SceneShaderClass();
}
void StackingSceneShaderClass::Load(ChunkLoadClass& cload)
{
	while (cload.Open_Chunk())
	{
		switch(cload.Cur_Chunk_ID())
		{
			case CHUNK_STACKINGSCENESHADER_VARIABLES:
				LoadVariables(cload);
				break;
			case CHUNK_STACKINGSCENESHADER_SHADERS:
				LoadShaders(cload);
				break;
		}
		cload.Close_Chunk();
	}	
}
void StackingSceneShaderClass::LoadVariables(ChunkLoadClass& cload)
{
 	while (cload.Open_Micro_Chunk())
	{
		switch(cload.Cur_Micro_Chunk_ID())
		{
			case MC_SCENESHADER_UID:
				cload.Read(&UID,sizeof(unsigned int));
				break;
			case MC_SCENESHADER_NAME:
				Name = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(Name,cload.Cur_Micro_Chunk_Length());
				break;
		}
		cload.Close_Micro_Chunk();
	}
}
void StackingSceneShaderClass::LoadShaders(ChunkLoadClass& cload)
{
	while (cload.Open_Chunk())
	{
		SceneShaderFactory *factory = SceneShaderRegistrar->GetFactory(cload.Cur_Chunk_ID());
		if (factory)
		{
			SceneShaderClass *shader = factory->Load(cload);
			if (shader)
			{
				Shaders->Add(shader);
			}
		}
		else 
		{
			char *c = new char[cload.Cur_Chunk_Length()];
			cload.Read(c,cload.Cur_Chunk_Length());
			delete[] c;
		}
		cload.Close_Chunk();
	}
}
void StackingSceneShaderClass::Save(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_STACKINGSCENESHADER);
	SaveVariables(csave);
	SaveShaders(csave);
	csave.End_Chunk();
}
void StackingSceneShaderClass::SaveVariables(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_STACKINGSCENESHADER_VARIABLES);
	csave.Begin_Micro_Chunk(MC_SCENESHADER_UID);
	csave.Write(&UID,sizeof(unsigned int));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MC_SCENESHADER_NAME);
	csave.Write(Name,strlen(Name) + 1);
	csave.End_Micro_Chunk();
	csave.End_Chunk();
}
void StackingSceneShaderClass::SaveShaders(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_STACKINGSCENESHADER_SHADERS);
	for (int i = 0; i < Shaders->Count(); i++)
	{
		(*Shaders)[i]->Save(csave); 
	}
	csave.End_Chunk();
}
#ifndef SDBEDIT
void StackingSceneShaderClass::Initialize()
{
	for (int i = 0; i < Shaders->Count(); i++)
	{
		(*Shaders)[i]->Initialize(); 
	}
}
bool StackingSceneShaderClass::Validate()
{
	if (Validated)
	{
		return ValidationResult;
	}
	if (Shaders->Count() == 0) 
	{
		Validated = true;
		ValidationResult = false;
		return false;
	}
	for (int i = 0; i < Shaders->Count(); i++)
	{
		bool valid = (*Shaders)[i]->Validate();
		if (!valid)	{
			Validated = true;
			ValidationResult = false;
			return false;	
		}
	}
	Validated = true;
	ValidationResult = true;
	return true;
}
void StackingSceneShaderClass::OnDeviceLost()
{
	for (int i = 0; i < Shaders->Count(); i++)
	{
		(*Shaders)[i]->OnDeviceLost(); 
	}
}
void StackingSceneShaderClass::OnDeviceReset()
{
	for (int i = 0; i < Shaders->Count(); i++)
	{
		(*Shaders)[i]->OnDeviceReset(); 
	}
}
void StackingSceneShaderClass::OnCustom(unsigned int eventid, float parameter)
{
	for (int i = 0; i < Shaders->Count(); i++)
	{
		(*Shaders)[i]->OnCustom(eventid,parameter); 
	}
}
void StackingSceneShaderClass::Render(SceneShaderRenderInfo *render)
{
 	for (int pass = 0; pass < Shaders->Count(); pass++)
	{
		if (pass > 0) 
		{
			SceneShaderController->RequestNewBuffer(&render);
		}
		(*Shaders)[pass]->Render(render); 
	}
}
#endif

SceneShaderRegistrant<StackingSceneShaderClass> StackingSceneShaderRegistrant(CHUNK_STACKINGSCENESHADER,"Stacking Scene Shader");
