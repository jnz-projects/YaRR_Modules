/*	Renegade Scripts.dll
	Scripts by Mark "Saberhawk" Sararu
	Copyright 2007 Mark "Saberhawk" Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class SH_ConsoleCommand : public ScriptImpClass {
	void Created(GameObject *sender);
	void Timer_Expired(GameObject *sender,int number);
};

class SH_PCT_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class SH_PCT_Powerup : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class SH_Spawn_Difficulty : public ScriptImpClass {
	void Created(GameObject *obj);
};


class FileVerificationController
{
protected:
	struct PlayerInfoStruct
	{
		PlayerInfoStruct(int playerID, int filecount);
		~PlayerInfoStruct();
		unsigned long CRCSalt;
		SimpleDynVecClass<bool>	*FilesVerificationStatus;
		SimpleDynVecClass<int> *CallbackHooks;
	};
	struct NotifyCallbackData
	{
		int FileID;
		int PlayerID;
	};
	bool UseVerification;
	INIClass *FileINI;
	int FileCount;
	SimpleDynVecClass<unsigned long> *Files;
	SimpleDynVecClass<PlayerInfoStruct *> *Players;
	static void MapLoadHook();
	static void MapUnloadHook();
	static void PlayerJoinedHook(int PlayerID, const char *PlayerName);
	static void PlayerLeftHook(int PlayerID);
	static void NotifyHook(void *data, int parameter);
	static void VerifyDoneNotifyHook(void *data, int parameter);
	unsigned long GetFileCRC(int file);
	/*
	Reserved for possible future hook code update
	int MapLoadHookNumber;
	int MapUnloadHookNumber;
	int PlayerJoinedHookNumber;
	int PlayerLeftHookNumber;
	*/
public:
	FileVerificationController(const char *filename);
	~FileVerificationController();
	void MapLoaded();
	void MapUnloaded();
	void PlayerJoined(int PlayerID);
	void PlayerLeft(int PlayerID);
	void PlayerVerifyTimeout(int PlayerID);
	void VerifyPlayer(int PlayerID);
	void VerifyAllPlayers();
	void ProcessFile(int PlayerID,int file, unsigned long crc);
};

class SH_FileVerificationControllerScript: public ScriptImpClass
{
public:
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Timer_Expired(GameObject *sender, int number);
};

void CreateFileVerifyController();
void DestroyFileVerifyController();

#define CUSTOM_MAPLOAD				0x85010000
#define TIMER_MAPLOAD				0x85020000
#define CUSTOM_PLAYERLOAD			0x85030000
#define TIMER_PLAYERLOAD			0x85040000
#define CUSTOM_STARTTIMEOUT			0x85050000
#define TIMER_PLAYERTIMEOUT			0x85060000
#define SHADEREVENT_SETVERIFYSALT	8001
#define SHADEREVENT_REQUESTVERIFY	8002
#define SCRIPTEVENT_VERIFYDONE		8003
#define FILEIDBIAS					9000
